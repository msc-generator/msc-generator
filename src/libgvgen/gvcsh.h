/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2024 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file gvcsh.h The declaration for the GraphCsh class.
* @ingroup libgvgen_files */

#ifndef GVCSH_H
#define GVCSH_H

#include "csh.h"
#include "gvstyle.h"

namespace graph {

/** Coloring for graphs. */
class GraphCsh : public Csh
{
protected:
    virtual bool DoHintLocated(EHintSourceType hsource, std::string_view a_name) override;
public:
    std::string layout; ///<Stores the layout that will be used. (used to select attribute availability for hints)
    explicit GraphCsh(Csh::FileListProc proc, const LanguageCollection* languages);
    ~GraphCsh() override = default;
    std::unique_ptr<Csh> Clone() const override { return std::make_unique<GraphCsh>(*this); }

    //static override
    ///** Push the context stack with a fresh 'plain' design.*/
    //Leave original of duplicating the existing one: void PushContext() { Contexts.emplace_back(FullDesigns["plain"]); }
    // Inherited via Csh
    void FillNamesHints() override;
    void ParseText(std::string&& input, CharByteIndex first_char, CharByteIndex cursor_p, bool pedantic) override;
    /** Emphasize the beginning of a subgraph name if it starts with 'cluster'*/
    void AddCSH_Subgraphname(const CshPos &pos, std::string_view name);
    /** Color the compass point if a valid one. Else emit error.*/
    void AddCSH_Compass(const CshPos &pos, std::string_view name);
    virtual void AddCSH_AttrName(const CshPos&, std::string_view name, EColorSyntaxType) override;
    /** Just color the text to COLOR_ATTRVALUE*/
    void AddCSH_AttrValue(const CshPos &pos, std::string_view aname, std::string_view avalue, EColorSyntaxType t);
    /** Color a partial line begin keyword outside graphs */
    void AddCSH_LineBeginOutsideGraph(const CshPos &pos, std::string_view text);

    /** Add the available attributes of a style to Hints.*/
    void AttributeNames(GraphStyle::EGraphElementType element_type) { GraphStyle::DotAttributeNames(element_type, layout, *this); }
    /** Add the value of an attributes of a style to Hints.*/
    void AttributeValues(GraphStyle::EGraphElementType element_type) { GraphStyle::DotAttributeValues(element_type, hintAttrName, layout, *this); }

    /** Add to hints what may come at the beginning of a line inside a design definition.*/
    void AddLineBeginToHintsInsideDesignDef();
    /** Add to hints what may come at the beginning of a line inside a graph definition.*/
    void AddLineBeginToHintsInsideGraph();
    /** Add to hints what keywords we may have outside graphs. No options added.*/
    void AddKeywordsOutsideGraph(bool add_strict, bool add_graph_digraph);
    /** Add to hints what may come at the beginning of a line outside a graph definition. Options (only if add strict is true) and keywords.*/
    void AddLineBeginToHintsOutsideGraph(bool add_strict, bool add_graph_digraph);
    /** Add Chart options to hints.*/
    void AddOptionsToHints() override;
    /** Add the value of chart options to hints.*/
    bool AddOptionsValuesToHints();
};

} //namespace graph

#endif