/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2024 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This file is included by parsers both for color syntax and language compilation */

#ifndef GVPARSERHELPER_H
#define GVPARSERHELPER_H

#include <string>
#include "gvgraphs.h"
#include "parser_tools.h"

namespace graph {

struct gv_parser_helper_compile : procedure_parse_helper {
    bool directed = true;       ///<what kind of graph do we parse
    bool html_may_come = false; ///<true after an '=' until the first non whitespace token
};

struct gv_parser_helper_csh {
    bool   directed = true;       ///<what kind of graph do we parse
    bool   html_may_come = false; ///<true after an '=' until the first non whitespace token
    CshPos last_top_level_command_pos; ///<The end position of the last graph, defdesign, defstyle, etc. command. Used to see if we need to hint a 'strict' before graph/digraph
};

template <bool CSH>
using gv_parser_helper = std::conditional<CSH, gv_parser_helper_csh, gv_parser_helper_compile>::type;

}

#endif