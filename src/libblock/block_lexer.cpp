#include <type_traits>
#include <iostream>
#include "maphoon-lexer/includes.h"
#include "block_lexer.h"
#include "block_parser_compile.h"

int main() {
    std::ostringstream out;
    lexing::printcode<char, block_compile_tokentype>( "char", "int", {"block"},
        block::build_classifier<block_compile_tokentype>(), out,
        [](std::ostream& out, char ch) {
             unsigned char c = ch;
             if (c=='\\' || c=='\'') out << "\'\\" << c << '\'';
             else if (32 <= c && c <= 127) out << '\'' << c << '\'';
             else out << (int)c; },
        [](std::ostream& out, block_compile_tokentype S) { out << int(S) ; }
    );
    //Remove Q00 (and the following colon) if present only once
    //to avoid "unused label" warnings
    std::string ret = std::move(out).str();
    if (const size_t pos = ret.find("Q00")
        ; pos != ret.npos && ret.substr(pos, 4) == "Q00:" && ret.find("Q00", pos + 1) == ret.npos)
        ret.erase(pos, 4);

    std::ofstream("block_tokenizer.h") << ret;
}