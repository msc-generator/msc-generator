#ifndef MSCGEN_CLIPBOARD_H
#define MSCGEN_CLIPBOARD_H

#include <string>
#include <string_view>
#include <tuple>

/** The formats we are interested in on the clipboard. */
enum class ClipboardFormat {
    Art_GVML,    ///<Zipped XML containing something copied from Office
    Escaped_Text,///<Plain text starting with "Msc-generator~|"
    Else         ///<Some other format
};
/** Returns the content of the clipboard (if interesting for us).
 * (2 formats are interesting, see 'ClipboardFormat'.) Any value
 * is only returned once, that is, after we have returned the content,
 * subsequent calls to this function will return the empty optional
 * unless new data is copied to the clipboard.*/
std::optional<std::pair<ClipboardFormat, std::string>> GetClipboard();
/** Place this to the clipboard.
 * @params [in] format What is the format of 'content' we shall use when placing to clipboard.
 * @params [in] content The content to place to the clipboard.
 * @params [in] type For Art_GVML, this contains the human readable chart type. Ignored for Escaped_Text.
 * @returns true if the operation failed and re-trying it makes sense. (E.g., clipboard was locked, etc.) */
bool CopyToClipboard(ClipboardFormat format, const std::string& content, std::string_view type = {});

#endif //MSCGEN_CLIPBOARD_H