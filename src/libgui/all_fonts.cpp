#include "fonts/Cousine/Cousine-Regular.cpp"
#include "fonts/Cousine/Cousine-Italic.cpp"
#include "fonts/Cousine/Cousine-Bold.cpp"
#include "fonts/Cousine/Cousine-BoldItalic.cpp"
#include "fonts/OpenSans/OpenSans-Regular.cpp"
#include "fonts/OpenSans/OpenSans-Italic.cpp"
#include "fonts/OpenSans/OpenSans-Bold.cpp"
#include "fonts/OpenSans/OpenSans-BoldItalic.cpp"
#include "cousine_licence.c"
#include "opensans_licence.c"

#include "gui.h"

void LoadFonts() {
    auto &io = ImGui::GetIO();
    //io.Fonts->AddFontDefault(); //The first font added will be the default
    ImFontConfig config;
    config.OversampleH = 5;
    config.OversampleV = 5;
    // The list is tightly coupled with GetFont() so don't mess with the ordering here
    MscGenFonts[0] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(Cousine_Regular_compressed_data_base85, MscGenEditorFontSize, &config);
    MscGenFonts[1] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(Cousine_Italic_compressed_data_base85, MscGenEditorFontSize, &config);
    MscGenFonts[2] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(Cousine_Bold_compressed_data_base85, MscGenEditorFontSize, &config);
    MscGenFonts[3] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(Cousine_BoldItalic_compressed_data_base85, MscGenEditorFontSize, &config);
    MscGenFonts[4] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(OpenSans_Regular_compressed_data_base85, MscGenWindowFontSize, &config);
    MscGenFonts[5] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(OpenSans_Italic_compressed_data_base85, MscGenWindowFontSize, &config);
    MscGenFonts[6] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(OpenSans_Bold_compressed_data_base85, MscGenWindowFontSize, &config);
    MscGenFonts[7] = io.Fonts->AddFontFromMemoryCompressedBase85TTF(OpenSans_BoldItalic_compressed_data_base85, MscGenWindowFontSize, &config);
}

std::map<std::string, std::string> FontLicenses() {
    return {{"Font Cousine", cousine_licence}, {"Font OpenSans", opensans_licence}};
}
