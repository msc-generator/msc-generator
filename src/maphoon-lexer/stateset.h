
//  Copyright <2022> <Hans de Nivelle>
//
//  This software is released with the 3-clause BSD license, which 
//  is given below: 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions are met:

//  Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer:

//  Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//  Neither the name of the copyright holder nor the names of its contributors 
//  may be used to endorse or promote products derived from this software 
//  without specific prior written permission.

//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
//  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
//  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



// Written by Dina Muktubayeva, March/April 2021.

#ifndef MAPH_LEXING_STATESET_
#define MAPH_LEXING_STATESET_  1

#include <iostream>
#include <set>

#include "state.h"

namespace lexing
{

   class stateset 
   {
      std::set< state > states; 

   public:
      stateset( ) = default;  

      struct hash
      {
         size_t operator( ) ( const stateset& set ) const;
      };

      struct equal_to
      {
         bool operator( ) ( const stateset& s1, const stateset& s2 ) const;
      };

      void print( std::ostream& out ) const; 

      bool insert( state s ) { return states. insert(s). second; } 
      bool erase( state s ) { return states. erase(s); }

      bool contains( state s ) const 
         { return states. find(s) != states. end( ); } 

      using const_iterator = std::set< state > :: const_iterator;
      const_iterator begin( ) const { return states. begin( ); }
      const_iterator end( ) const { return states. end( ); } 


      size_t size( ) const { return states. size( ); }  

      void clear( ) { states. clear( ); } 
   };

   
   inline bool subset( const stateset& set1, const stateset& set2 )
   {
      if( set1. size( ) <= set2. size( ))
      {
         for( state s : set1 )
         {
            if( !set2. contains(s))
               return false;
         }
         return true; 
      }
      else
         return false; 
   }

   inline std::ostream& operator<< ( std::ostream& out, const stateset& set ) 
   {
      set. print( out );
      return out;
   }

}

#endif



