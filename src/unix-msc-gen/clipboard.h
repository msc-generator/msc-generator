#pragma once

//This file contains the functions needed by the clipboard manipulation functions

/** The formats we are interested in on the clipboard. */
enum MacClipboardFormat {
    Art_GVML,    ///<Zipped XML containing something copied from Office
    Escaped_Text,///<Plain text starting with "Msc-generator~|"
    Else,        ///<Some other format
    No_Change    ///<The clipboard has not changed
};

/** Returns the content of the clipboard (if interesting for us).
 * (2 formats are interesting, see 'MacClipboardFormat'.) Any value
 * is only returned once, that is, after we have returned the content,
 * subsequent calls to this function will return No_Change.*/
extern "C" char* MacGetClipboard(MacClipboardFormat *format, unsigned *len);
/** Place this to the clipboard.
 * @params [in] format What is the format of 'content' we shall use when placing to clipboard.
 * @params [in] content The content to place to the clipboard. If it is text, it must be null-terminated.
 * @params [in] content_len the length of the content (not counting terminating zero for text)
 * @params [in] type For Art_GVML, this contains the human readable chart type null-terminated. Ignored for Escaped_Text.
 * @returns non-null if the operation failed and re-trying it makes sense. (E.g., clipboard was locked, etc.) */
extern "C" int MacCopyToClipboard(MacClipboardFormat format, const char *content, unsigned content_len, const char *type);

