/*takes one argument and generates designs.c from an incoming original_designlinb.c*/
#include <stdio.h>

int main(int argc, char* argv[])
{
    if (argc<1) return 1;
    printf("char %s[]=\n\"", argv[1]);
    int c;
    while ((c = getchar()) != EOF) {
        switch (c) {
        case '\n': printf("\\n\"\n\"");
        case '\r': break;  /*so DOS files are handled too*/
        case '\\':
        case '\'': /*escape " and ' and \ */
        case '\"': putchar('\\');
        default: putchar(c);
        }
    }
    printf("\";\n");
    return 0;
}
