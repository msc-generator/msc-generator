/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2024 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file flowcsh.cpp The definition for the FlowCsh class and coloring for the "flow" language.
* @ingroup libflow_files */


#include <cstring>
#include "flowcsh.h"
#include "flowstyle.h"
#include "steps.h"
#include "flow_parser_csh.h"

using namespace flow;


FlowCsh::FlowCsh(Csh::FileListProc proc, const LanguageCollection* languages) :
    Csh(FlowContext(true, EContextParse::NORMAL, EContextCreate::PLAIN, FileLineCol()), proc, languages)
{
    FillNamesHints();
}

void FlowCsh::FillNamesHints()
{
    //Uncomment if you have options
    //AddOptionsToHints();
    //MoveHintsToOptionNames();

    //Uncomment if you have keywords
    //AddKeywordToHints();
    //MoveHintsToKeywordNames();

    //Uncomment if you do attributes
    Arrow::AttributeNames(*this);
    Step::AttributeNames(*this);
    MoveHintsToAttrNames();
}

/** Parse chart text for color syntax and hint info
 *
 * @param [in] input The chart text
 * @param [in] first_char The (1-based) index of the first character of 'input' in the file we parse.
 *             If we parse a whole file, it shall be 1.
 * @param [in] cursor_p The current position of the cursor.
 * @param [in] pedantic The initial value of the pedantic chart option.
   (We ignore it in CSH parsing.)*/
void FlowCsh::ParseText(std::string&& input, CharByteIndex first_char, CharByteIndex cursor_p, bool /*pedantic*/)
{
    //initialize data struct
    BeforeYaccParse(std::move(input), first_char, cursor_p);
    //call parsing
    sv_reader<true> reader(input_text, first_char);
    flow_csh_parse(*this, reader); //return value ignored
    //Tidy up afterwards
    AfterYaccParse();
}
