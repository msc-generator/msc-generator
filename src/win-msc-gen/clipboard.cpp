/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2024 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#undef min
#undef max
#include "../libgui/clipboard.h"
#include "utf8utils.h"


UINT CF_ART_GVML_CLIPFORMAT = RegisterClipboardFormatA("Art::GVML ClipFormat");
UINT CF_OBJECTDESCRIPTOR = RegisterClipboardFormatA("Object Descriptor");

static DWORD clipboard_seq = 0;

std::optional<std::pair<ClipboardFormat, std::string>> GetClipboard() {
    std::optional<std::pair<ClipboardFormat, std::string>> ret;
    if (DWORD S = GetClipboardSequenceNumber(); S == clipboard_seq) return ret;
    else clipboard_seq = S;
    if (OpenClipboard(NULL)) {
        ret.emplace(ClipboardFormat::Else, "");
        if (HGLOBAL hglb = GetClipboardData(CF_ART_GVML_CLIPFORMAT)) {
            if (LPVOID p = GlobalLock(hglb)) {
                ret->second.assign(static_cast<char*>(p), GlobalSize(hglb));
                ret->first = ClipboardFormat::Art_GVML;
                GlobalUnlock(hglb);
            }
        } else if (HGLOBAL hglb = GetClipboardData(CF_TEXT)) {
            if (LPVOID p = GlobalLock(hglb)) {
                std::string_view text(static_cast<char*>(p), GlobalSize(hglb));
                if (size_t l = text.find('\0'); l != std::string::npos)
                    text.remove_suffix(text.size() - l);
                if (text.starts_with("Msc-generator~|")) {
                    ret->second.assign(text);
                    ret->first = ClipboardFormat::Escaped_Text;
                }
                GlobalUnlock(hglb);
            }
        }
        CloseClipboard();
    }
    return ret;
}

//returns false to try again
bool CopyToClipboard(ClipboardFormat format, const std::string& content, std::string_view type) {
    if (!OpenClipboard(NULL)) return false;
    const size_t size = content.size() + (format == ClipboardFormat::Escaped_Text ? 1 : 0); //for text include the terminating zero
    HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, size);
    if (hglbCopy == NULL) {
        CloseClipboard();
        return true; //do not try again
    }
    EmptyClipboard();
    void* lptstrCopy = GlobalLock(hglbCopy);
    if (!lptstrCopy) {
        GlobalUnlock(hglbCopy);
        goto fail;
    }
    memcpy(lptstrCopy, content.data(), size);
    GlobalUnlock(hglbCopy);
    switch (format) {
    case ClipboardFormat::Escaped_Text:
        SetClipboardData(CF_TEXT, hglbCopy);
        break;
    case ClipboardFormat::Art_GVML:
        SetClipboardData(CF_ART_GVML_CLIPFORMAT, hglbCopy);
        //Attach an object descriptor. https://docs.microsoft.com/hu-hu/windows/win32/api/oleidl/ns-oleidl-objectdescriptor
        static constexpr wchar_t source[] = L"Msc-generator";
        const std::wstring type_name = ConvertFromUTF8_to_UTF16(type);
        const size_t type_name_len = (type_name.length() + 1) * sizeof(wchar_t); //null terminated in bytes
        OBJECTDESCRIPTOR OD;
        OD.cbSize = sizeof(OD) + type_name_len + sizeof(source); //source is null terminated, and size includes the terminating zero
        OD.clsid = CLSID_NULL;
        OD.dwDrawAspect = DVASPECT_CONTENT;
        OD.sizel = {0,0}; //TODO: In himetric (one hundredth of a millimeter)
        OD.pointl = {0,0};
        OD.dwStatus = 0;
        OD.dwFullUserTypeName = sizeof(OD);
        OD.dwSrcOfCopy = sizeof(OD) + type_name_len; //offset from beginning
        hglbCopy = GlobalAlloc(GMEM_MOVEABLE, OD.cbSize);
        if (hglbCopy == NULL) goto fail;
        lptstrCopy = GlobalLock(hglbCopy);
        if (!lptstrCopy) {
            GlobalUnlock(hglbCopy);
            goto fail;
        }
        std::memcpy(lptstrCopy, &OD, sizeof(OD));
        std::memcpy(static_cast<char*>(lptstrCopy) + sizeof(OD), type_name.data(), type_name_len);
        std::memcpy(static_cast<char*>(lptstrCopy) + sizeof(OD) + type_name_len, source, sizeof(source));
        GlobalUnlock(hglbCopy);
        SetClipboardData(CF_OBJECTDESCRIPTOR, hglbCopy);
    }
    clipboard_seq = GetClipboardSequenceNumber();
fail:
    CloseClipboard();
    return true;
}

