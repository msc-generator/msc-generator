// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <shlobj.h>
#include "Shlwapi.h"
#pragma comment(lib, "Shlwapi.lib")
#include <iostream>
#include <fstream>
#include <filesystem>
#include <sstream>
#include <assert.h>
#include "cairo.h"
#include "cairo-win32.h"

#include "imgui.h"
#include "backends\imgui_impl_dx11.h"
#include "backends\imgui_impl_win32.h"
#include <d3d11.h>
#include <tchar.h>

#include <cstdlib>
#include <shellapi.h>
#include "oleidl.h"
