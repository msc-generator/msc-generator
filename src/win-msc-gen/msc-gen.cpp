/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

// msc-gen.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#undef min
#undef max
#include "commandline.h"
#include "gui.h"
#include "utf8utils.h"
#include "msc.h"
#include "graphchart.h"
#include "xxxchart.h"
#include "blockchart.h"
#include "flowchart.h"
#include "dbgchart.h"
extern "C" {
#include "LooplessSizeMove.h" //from: https://sourceforge.net/projects/win32loopl/
}

using namespace std;


#define REG_SUBKEY_SETTINGS _T("Software\\Zoltan Turanyi\\Msc-generator\\Settings")
#define REG_KEY_LOAD_DATA _T("LoadData")
#define REG_KEY_INSTALLDIR _T("InstallDir")


//https://www.devever.net/~hl/win32con
bool win32con_is_exclusive(void) {
    DWORD pids[2];
    DWORD num_pids = GetConsoleProcessList(pids, sizeof(pids)/sizeof(DWORD));
    return num_pids <= 1;
}

bool win32con_exists(void) {
    return !!GetConsoleWindow();
}

void win32con_hide(void) {
    HWND wnd = GetConsoleWindow();
    if (wnd)
        ShowWindow(wnd, SW_HIDE);
}

/** Get the path to a folder.
* 1==the folder of the executable
* 2==appdata/mscgenerator fodler */
std::wstring GetFolder(unsigned folder)
{
    TCHAR buff[MAX_PATH];
    if (folder == 1) {
        DWORD len;
        TCHAR *buffer = nullptr;
        std::wstring dir;
        LONG res = RegGetValue(HKEY_CURRENT_USER, REG_SUBKEY_SETTINGS,
                               REG_KEY_INSTALLDIR, RRF_RT_REG_SZ, nullptr, nullptr, &len);
        if (res==ERROR_MORE_DATA || res==ERROR_SUCCESS) {
            buffer = (TCHAR*)malloc(len+1);
            res = RegGetValue(HKEY_CURRENT_USER, REG_SUBKEY_SETTINGS,
                              REG_KEY_INSTALLDIR, RRF_RT_REG_SZ, nullptr, buffer, &len);
            if (res==ERROR_SUCCESS && buffer) {
                return buffer;
            }
            free(buffer);
        }
        //If we dont have an installdir, try besides the executable.
        GetModuleFileName(nullptr, buff, MAX_PATH);
        TCHAR *fnamebegin = PathFindFileName(buff);
        *fnamebegin = 0; //equals to 'buff' if no file name is found.
        return buff;
    } else if (folder==2) {
        if (!SUCCEEDED(SHGetFolderPath(nullptr, CSIDL_APPDATA, nullptr, 0, buff)))
            return buff;
        std::wstring ret = buff;
        ret += _T("\\Msc-generator\\");
        return ret;
    }
    return _T("");
}

FILE *OpenNamedFile(const char *utf8filename, bool write, bool binary)
{
    const std::wstring name = ConvertFromUTF8_to_UTF16(utf8filename);
    FILE* in = nullptr;
    if (_wfopen_s(&in, name.c_str(), write ? binary ? _T("wb") : _T("wt") : binary ? _T("rb") : _T("rt")))
        return nullptr;
    return in;
}


/** Reads a file that is included in the document.
* @returns error message ("" if OK), filename actually read and read text*/
std::tuple<std::string, std::string, std::string>
ReadIncludeFile(std::string_view filename, void *app, std::string_view included_from)
{
    std::tuple<std::string, std::string, std::string> ret;
    if (filename.length()==0 || filename[0]==0) {
        std::get<0>(ret) = "Can not include a file of empty name.";
        return ret;
    }
    std::wstring fn = ConvertFromUTF8_to_UTF16(filename);
    //convert from UNIX style to Windows style
    for (wchar_t &t : fn)
        if (t==L'/') t = L'\\';
    //convert from double slashes to single ones.
    for (unsigned u=0; u+1<fn.length(); u++)
        if (fn[u]==L'\\' && fn[u+1]==L'\\')
            fn.erase(fn.begin()+u+1);
    if (fn[0]==L'\\' || (fn[0] && fn[1]==L':')) {
        //full path: starts with a drive letter or a backslash=>use fn only
        if (fn.length()>MAX_PATH) {
            std::get<0>(ret) = "Specified filepath is too long.";
            return ret;
        }
    } else {
        std::wstring from = ConvertFromUTF8_to_UTF16(included_from);
        if (from.length()+fn.length()>MAX_PATH) {
            std::get<0>(ret) = "Specified filepath is too long.";
            return ret;
        }
        fn.insert(fn.begin(), from.c_str(), (const wchar_t *)PathFindFileName(from.c_str()));
    }
    //previously we used this: PathCombine(wbuff, included_from, filename);
    //but that sucked, e.g, could not combine ../../test/ with a filename
    //without deleting the ../../ from the front
    std::get<1>(ret) = ConvertFromUTF16_to_UTF8(fn);
    FILE* file;
    if (!_wfopen_s(&file, fn.c_str(), _T("r")) && file) {
        std::get<2>(ret) = ReadFile(file);
        //If it happens to be Unicode, convert input text to UTF-8
        ConvertToUTF8(std::get<2>(ret), false);
        fclose(file);
    } else
        std::get<0>(ret) = "Could not open file: "+std::get<1>(ret);
    return ret;
}


std::vector<std::pair<std::string, std::string>> ReadDesigns(const std::vector<std::string_view> &extensions)
{
    unsigned basic = 0, user = 0;
    std::vector<std::pair<std::string, std::string>> ret;
    for (auto &ext : extensions) {
        wstring dir = GetFolder(1);
        std::wstring wext = ConvertFromUTF8_to_UTF16(ext);
        WIN32_FIND_DATA find_data;
        HANDLE h = FindFirstFile((dir+_T("designlib.")+wext).c_str(), &find_data);
        if (h == INVALID_HANDLE_VALUE)
            h = FindFirstFile((dir+_T("original_designlib.")+wext).c_str(), &find_data);
        bool bFound = h != INVALID_HANDLE_VALUE;
        while (bFound) {
            ret.emplace_back(ConvertFromUTF16_to_UTF8(dir+find_data.cFileName), "");
            basic++;
            bFound = FindNextFile(h, &find_data);
        }
        FindClose(h);

        dir = GetFolder(2);
        h = FindFirstFile((dir+_T("*.")+wext).c_str(), &find_data);
        bFound = h != INVALID_HANDLE_VALUE;
        while (bFound) {
            user++;
            ret.emplace_back(ConvertFromUTF16_to_UTF8(dir+find_data.cFileName), "");
            bFound = FindNextFile(h, &find_data);
        }
        FindClose(h);
    }
#ifdef _DEBUG
    wprintf(L"Found %d designlibs in %s.\n", basic, GetFolder(1).c_str());
    wprintf(L"Found %d designs in %s.\n", user, GetFolder(2).c_str());
#endif // _DEBUG
    return ret;
}

bool progressbar(const ProgressBase *progress, void *p,
                 ProgressBase::EPreferredAbortMethod a)
{
    HANDLE hOut = HANDLE(p);
    CONSOLE_SCREEN_BUFFER_INFO bInfo;
    COORD crStart, crCurr;
    GetConsoleScreenBufferInfo(hOut, &bInfo);
    crCurr = bInfo.dwCursorPosition; //the old position
    crStart.X = crCurr.X = 0;
    crStart.Y = crCurr.Y;
    SetConsoleCursorPosition(hOut, crStart);
    double percent = progress ? progress->GetPercentage() : 0;
    if (percent>=0 && percent<=100)
        printf("%3d%%", int(floor(percent+0.5)));
    else
        printf("    ");
    SetConsoleCursorPosition(hOut, crCurr);
    return false;
}

// Data
static ID3D11Device *g_pd3dDevice = NULL;
static ID3D11DeviceContext *g_pd3dDeviceContext = NULL;
static IDXGISwapChain *g_pSwapChain = NULL;
static ID3D11RenderTargetView *g_mainRenderTargetView = NULL;

// Forward declarations of helper functions
bool CreateDeviceD3D(HWND hWnd);
void CleanupDeviceD3D();
void CreateRenderTarget();
void CleanupRenderTarget();
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
TextureID TextureFromSurface(cairo_surface_t *rgba_surface);
void DestroyTexture(ImTextureID t);
void SetFullscreen(HWND hwnd, bool fullscreen, bool for_metro);
HICON CreateIconFromBytes(int width, int height, const void* bytes);
const char directory_separator = '\\';
GUIEvents events;
float current_dpi_mul = 1;

/** A callback to enumerate fonts */
BOOL CALLBACK EnumFamCallBack(LPLOGFONT lplf, LPTEXTMETRIC lpntm, DWORD FontType, LPVOID fontset) {
    if (TRUETYPE_FONTTYPE&FontType) {
        std::set<string> *set = static_cast<std::set<string>*>(fontset);
        if (set)
            set->insert(ConvertFromUTF16_to_UTF8(lplf->lfFaceName));
    }
    return 1;
}

void GUIBeep() { MessageBeep(MB_ICONERROR); }

inline std::chrono::nanoseconds ns(const FILETIME& f) noexcept {
    const ULARGE_INTEGER ULI{.LowPart = f.dwLowDateTime, .HighPart = f.dwHighDateTime};
    return std::chrono::nanoseconds{ULI.QuadPart*100};
}

std::vector<std::string> FileListProc(std::string_view prefix_, std::string_view included_from_) {
    std::wstring included_from__ = ConvertFromUTF8_to_UTF16(included_from_);
    std::wstring prefix__ = ConvertFromUTF8_to_UTF16(prefix_);
    //convert UNIX style to Windows style
    std::ranges::replace(included_from__, L'/', L'\\');
    std::ranges::replace(prefix__, L'/', L'\\');

    namespace fs = std::filesystem;

    fs::path included_from = fs::path(included_from__).parent_path();
    fs::path dir = fs::path(prefix__).parent_path();

    if ((included_from.empty() || included_from.is_relative()) && (dir.empty() || dir.is_relative()))
        return{}; //cannot determine directory
    const bool use_included_from = dir.is_relative();
    if (use_included_from) dir = included_from / dir;
    std::vector<std::string> ret;
    try {
        for (auto& e : fs::directory_iterator(dir))
            if (e.is_regular_file() || e.is_symlink() || e.is_directory()) {
                fs::path p = e.path();
                if (use_included_from) {
                    fs::path p2;
                    for (auto i = std::mismatch(p.begin(), p.end(), included_from.begin(), included_from.end()).first;
                         i!=p.end(); i++)
                        p2 /= *i;
                    ret.push_back(ConvertFromUTF16_to_UTF8(p2.native()));
                } else
                    ret.push_back(ConvertFromUTF16_to_UTF8(p.native()));
            }
    } catch (const std::filesystem::filesystem_error&) {

    }
    return ret;
}

/** Maps the short name of a documentation file to its full UTF-16 path.*/
std::map<std::string, std::wstring> Documentation;

std::optional<std::string> ShowDocumentation(const std::string& doc_name) {
    const auto i = Documentation.find(doc_name);
    if (i==Documentation.end()) return "Documentation '"+doc_name+"' not installed.";
    const int ret = _wsystem((L"START /B \"\" \""+i->second+L"\"").c_str());
    if (ret==-1)
        return StrCat("Could not open documentation browser: ", my_strerror(errno));
    //If >0 error is returned the command interpreter has already displayed an error
    //(like file not found.) If the extension is not recognized, 0 is returned and
    //the command interpreter asks the user of what program to use to open (as of Win10).
    return {};
}

std::optional<std::string> ShowLink(std::string_view url) {
    const auto ret = (INT_PTR)ShellExecuteW(NULL, L"open", (L"\"" + ConvertFromUTF8_to_UTF16(url) + L"\"").c_str(), NULL, NULL, SW_SHOW);
    if (ret > 32) return {};
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpMsgBuf,
        0, NULL);
    std::optional<std::string> ret2 = StrCat("Could not open browser: ", ConvertFromUTF16_to_UTF8(((wchar_t*)lpMsgBuf)));
    LocalFree(lpMsgBuf);
    return ret2;
}

int _tmain(int argc, _TCHAR *argv[]) {
    //Generate language packs
    LanguageCollection languages;
    languages.AddLanguage(&MscChart::Factory, FileListProc);
    languages.AddLanguage(&GraphChart::Factory, FileListProc);
    languages.AddLanguage(&BlockChart::Factory, FileListProc);
#ifdef _DEBUG
    languages.AddLanguage(&FlowChart::Factory, FileListProc);
    languages.AddLanguage(&DbgChart::Factory, FileListProc);
    languages.AddLanguage(&XxxChart::Factory, FileListProc);
#endif

    std::vector<std::pair<std::string, std::string>> designs;
    std::vector<std::string> args;
    for (int i = 1; i<argc; i++)
        args.push_back(ConvertFromUTF16_to_UTF8(argv[i]));
    designs = ReadDesigns(languages.GetExtensions());

    HANDLE hOut = GetStdHandle(STD_ERROR_HANDLE);

    DWORD len = 0;
    char *buffer = nullptr;
    std::string load_data;
    LONG res = RegGetValue(HKEY_CURRENT_USER, REG_SUBKEY_SETTINGS,
                           REG_KEY_LOAD_DATA, RRF_RT_REG_SZ, nullptr, nullptr, &len);
    if (res==ERROR_MORE_DATA || res==ERROR_SUCCESS) {
        buffer = (char *)malloc(len+1);
        res = RegGetValue(HKEY_CURRENT_USER, REG_SUBKEY_SETTINGS,
                          REG_KEY_LOAD_DATA, RRF_RT_REG_SZ, nullptr, buffer, &len);
        if (res==ERROR_SUCCESS && buffer) {
            buffer[len] = 0;
            load_data = buffer;
        }
        free(buffer);
    }

    languages.AddLibraries(RegisterLibrariesGUI());

    const bool display_only = !win32con_exists() || win32con_is_exclusive();
    if (display_only) win32con_hide();

    auto ret = do_main(args, languages, std::move(designs), OpenNamedFile, &ReadIncludeFile, nullptr,
                       "\\f(courier new)\\mn(12)", display_only, progressbar, hOut, &load_data);
    if (std::holds_alternative<int>(ret)) {
        //We have completed a real command-line operation
        //Fallthrough to the end
    } else {
        //we need to start the GUI - most of the code below is from the IMGUI example in
        //examples/example_win32_directx11/main.cpp

        // Create application window
        std::string last_window_title;
#define WINDOW_TITLE_PREFIX "Msc-generator GUI"
        ImGui_ImplWin32_EnableDpiAwareness();
        WNDCLASSEX wc = {sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, _T(WINDOW_TITLE_PREFIX), NULL};
        ::RegisterClassEx(&wc);
        HWND hwnd = ::CreateWindow(wc.lpszClassName, _T(WINDOW_TITLE_PREFIX), WS_OVERLAPPEDWINDOW, 100, 100, 1280, 800, NULL, NULL, wc.hInstance, NULL);

        //https://docs.microsoft.com/hu-hu/windows/win32/api/winuser/nf-winuser-createiconfromresourceex?redirectedfrom=MSDN
#include <../Msc-generator-GUI/res/icon.h>
        _ASSERT(msc_icon.bytes_per_pixel == 4);
        HICON icon = CreateIconFromBytes(msc_icon.width, msc_icon.height, msc_icon.pixel_data);
        SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)icon);
        SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)icon);
        SendMessage(hwnd, WM_SETICON, ICON_SMALL2, (LPARAM)icon);

        // Initialize Direct3D
        if (!CreateDeviceD3D(hwnd)) {
            CleanupDeviceD3D();
            ::UnregisterClass(wc.lpszClassName, wc.hInstance);
            return 1;
        }

        current_dpi_mul = ImGui_ImplWin32_GetDpiScaleForHwnd(hwnd);

        // Setup Dear ImGui context
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO &io = ImGui::GetIO(); (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls - Why not :-)?
        io.IniFilename = nullptr;                                 // Manually save status data

        // Setup Dear ImGui style
        ImGui::StyleColorsLight();

        // Setup Platform/Renderer backends
        ImGui_ImplWin32_Init(hwnd);
        ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);

        //Calculate canonical path and name of the file to open
        if (auto &init = std::get<GUIInit>(ret); init.file_to_open_path.size()) {
            std::filesystem::path path = ConvertFromUTF8_to_UTF16(init.file_to_open_path);
            std::error_code ec;
            path = canonical(path, ec);
            if (!ec) {
                init.file_to_open_path = ConvertFromUTF16_to_UTF8(path.native());
                path = path.filename();
                if (!path.empty())
                    init.file_to_open_name = ConvertFromUTF16_to_UTF8(path.native());
            }
        }

        // Enumerate the fonts available - TODO: This just returns one character names
        HDC hdc = GetDC(hwnd);
        EnumFontFamilies(hdc, nullptr, (FONTENUMPROC)&EnumFamCallBack, LPARAM(&std::get<GUIInit>(ret).languages.cshParams->fontnames));
        ReleaseDC(hwnd, hdc);

        const std::vector example_dirs = {
            ConvertFromUTF16_to_UTF8([argv]()->std::wstring {
                if (!argv[0] || !argv[0][0]) return {};
                std::filesystem::path p(argv[0]);
                try { return (canonical(p).parent_path().parent_path() / L"examples").native()+L"\\"; } catch (std::filesystem::filesystem_error&) { return {}; }
            }()), //For a binary compiled in-place, we assume C:\XXX\Release\msc-gen.exe as argv[0]. We return "C:\XXX\examples\"
            ConvertFromUTF16_to_UTF8(GetFolder(1)+L"examples\\"),   //<INSTALLDIR>\examples (for a binary properly installed)
            ConvertFromUTF16_to_UTF8(GetFolder(2)+L"examples\\"),   //<C:\Users\<user>\AppData\Roaming\Msc-generator\examples (for ... I dunno why)
        };

        //Fill in what documentation files do we have. Search through some dirs
        const std::vector doc_dirs = {
            [argv]()->std::wstring {
                if (!argv[0] || !argv[0][0]) return {};
                std::filesystem::path p(argv[0]);
                try { return (canonical(p).parent_path().parent_path() / L"doc").native(); } catch (std::filesystem::filesystem_error&) { return {}; }
            }(), //For a binary compiled in-   place, we assume C:\XXX\Release\msc-gen.exe as argv[0]. We return "C:\XXX\doc\"
            GetFolder(1),   //<INSTALLDIR> (for a binary properly installed)
            GetFolder(2),   //<C:\Users\<user>\AppData\Roaming\Msc-generator (for ... I dunno why)
        };
        for (auto [name, file] : std::map<const char*, const wchar_t*>{{"PDF", L"msc-gen.pdf"}, {"Windows", L"msc-gen.chm"}})
            for (const std::wstring& dir : doc_dirs)
                if (std::wstring path = dir+L"\\"+file;
                    INVALID_FILE_ATTRIBUTES != GetFileAttributes(path.c_str())) {
                    Documentation[name] = std::move(path);
                    break;
                }
        std::vector<std::string> doc_names;
        for (const auto& [name, _] : Documentation)
            doc_names.push_back(name);

        GUIWindowGeometry w = InitGUI(std::get<GUIInit>(ret), current_dpi_mul, ConvertFromUTF16_to_UTF8(GetFolder(2)),
                                      ConvertFromUTF16_to_UTF8(std::filesystem::current_path().native()),
                                      example_dirs, std::move(doc_names));
        if (HMONITOR m = ::MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST)) {
            MONITORINFO i = {.cbSize = sizeof(MONITORINFO)};
            ::GetMonitorInfo(m, &i);
            w.ensure_fit_in(i.rcMonitor.right - i.rcMonitor.left, i.rcMonitor.bottom - i.rcMonitor.top);
        }
        ::SetWindowPos(hwnd, nullptr, w.x, w.y, w.cx, w.cy, SWP_SHOWWINDOW | SWP_NOZORDER | (w.has_pos() ? 0 : SWP_NOMOVE) | (w.has_size() ? 0 : SWP_NOSIZE));

        DragAcceptFiles(hwnd, TRUE);

        // Main loop
        while (true) {
            //We do not sleep to curb max fps, as FPS is synced to monitor fps via vsync below.

            events.dropped.clear();
            // Poll and handle messages (inputs, window resize, etc.)
            // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
            // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
            // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
            // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
            MSG msg;
            while (::PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE)) {
                ::TranslateMessage(&msg);
                ::SizingCheck(&msg); //from LoopLess Move/Size Lib
                ::DispatchMessage(&msg);
                if (msg.message == WM_QUIT)
                    goto cleanup;
            }
            if (PreFrameGUI(current_dpi_mul))
                ImGui_ImplDX11_CreateDeviceObjects();

            // Start the Dear ImGui frame
            ImGui_ImplDX11_NewFrame();
            ImGui_ImplWin32_NewFrame();
            ImGui::NewFrame();

            if (FILETIME start, stop, kernel, user
                ; GetProcessTimes(GetCurrentProcess(), &start, &stop, &kernel, &user))
                events.total_cpu = std::chrono::duration_cast<std::chrono::microseconds>(ns(user)+ns(kernel));
            else
                events.total_cpu.reset();

            GUIReturn res = DoGUI(events);
            events.request_exit = false;

            // Rendering
            ImGui::Render();
            g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
            ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

            g_pSwapChain->Present(1, 0); // Present with vsync
            //g_pSwapChain->Present(0, 0); // Present without vsync

            if (res.window_title.size() && res.window_title != last_window_title) {
                last_window_title = res.window_title;
                ::SetWindowTextW(hwnd, ConvertFromUTF8_to_UTF16(WINDOW_TITLE_PREFIX " - " + last_window_title).c_str());
            }
            SetFullscreen(hwnd, res.action == GUIReturn::FullScreen, false); //does nothing if we are in the correct state.

            if (res.action == GUIReturn::Exit) break;
            if (res.action == GUIReturn::Focus) ::SetForegroundWindow(hwnd);
        }
    cleanup:
        DragAcceptFiles(hwnd, FALSE);
        RECT r;
        ::GetWindowRect(hwnd, &r);
        ShutdownGUI(GUIWindowGeometry{r.left, r.top, r.right-r.left, r.bottom-r.top});

        // Cleanup
        ImGui_ImplDX11_Shutdown();
        ImGui_ImplWin32_Shutdown();
        ImGui::DestroyContext();

        CleanupDeviceD3D();
        ::DestroyWindow(hwnd);
        ::UnregisterClass(wc.lpszClassName, wc.hInstance);

        LSMCleanup();

        ret = EXIT_SUCCESS;
    }
    //Finally: common to cmdline and GUI: Write out progress balance data
    RegSetKeyValue(HKEY_CURRENT_USER, REG_SUBKEY_SETTINGS,
                   REG_KEY_LOAD_DATA, REG_SZ, load_data.c_str(), load_data.length()+1);
    return std::get<int>(ret);
}

// Helper functions

//from: https://stackoverflow.com/questions/2382464/win32-full-screen-and-hiding-taskbar
void SetFullscreen(HWND hwnd, bool fullscreen, bool for_metro) {
    //Saved window state while fullscreen
    static bool fullscreen_ = false;
    static bool maximized_ = false;
    static LONG style_ = 0, ex_style_ = 0;
    static RECT window_rect_;

    if (fullscreen==fullscreen_) return;

    // Save current window state if not already fullscreen.
    if (!fullscreen_) {
      // Save current window information.  We force the window into restored mode
      // before going fullscreen because Windows doesn't seem to hide the
      // taskbar if the window is in the maximized state.
        maximized_ = !!::IsZoomed(hwnd);
        if (maximized_)
            ::SendMessage(hwnd, WM_SYSCOMMAND, SC_RESTORE, 0);
        style_ = GetWindowLong(hwnd, GWL_STYLE);
        ex_style_ = GetWindowLong(hwnd, GWL_EXSTYLE);
        GetWindowRect(hwnd, &window_rect_);
    }

    fullscreen_ = fullscreen;

    if (fullscreen_) {
        // Set new window style and size.
        SetWindowLong(hwnd, GWL_STYLE,
                      style_ & ~(WS_CAPTION | WS_THICKFRAME));
        SetWindowLong(hwnd, GWL_EXSTYLE,
                      ex_style_ & ~(WS_EX_DLGMODALFRAME |
                                    WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE));

        // On expand, if we're given a window_rect, grow to it, otherwise do
        // not resize.
        if (!for_metro) {
            MONITORINFO monitor_info;
            monitor_info.cbSize = sizeof(monitor_info);
            GetMonitorInfo(MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST),
                           &monitor_info);
            SetWindowPos(hwnd, NULL, monitor_info.rcMonitor.left, monitor_info.rcMonitor.top,
                         monitor_info.rcMonitor.right - monitor_info.rcMonitor.left,
                         monitor_info.rcMonitor.bottom - monitor_info.rcMonitor.top,
                         SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);
        }
    } else {
        // Reset original window style and size.  The multiple window size/moves
        // here are ugly, but if SetWindowPos() doesn't redraw, the taskbar won't be
        // repainted.  Better-looking methods welcome.
        SetWindowLong(hwnd, GWL_STYLE, style_);
        SetWindowLong(hwnd, GWL_EXSTYLE, ex_style_);

        if (!for_metro) {
            // On restore, resize to the previous saved rect size.
            SetWindowPos(hwnd, NULL, window_rect_.left, window_rect_.top,
                         window_rect_.right - window_rect_.left,
                         window_rect_.bottom - window_rect_.top,
                         SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);
        }
        if (maximized_)
            ::SendMessage(hwnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
    }
}

bool CreateDeviceD3D(HWND hWnd) {
    // Setup swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 2;
    sd.BufferDesc.Width = 0;
    sd.BufferDesc.Height = 0;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    UINT createDeviceFlags = 0;
    //createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevelArray[2] = {D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0,};
    if (D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevelArray, 2, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &featureLevel, &g_pd3dDeviceContext) != S_OK)
        return false;

    CreateRenderTarget();
    return true;
}

void CleanupDeviceD3D() {
    CleanupRenderTarget();
    if (g_pSwapChain) { g_pSwapChain->Release(); g_pSwapChain = NULL; }
    if (g_pd3dDeviceContext) { g_pd3dDeviceContext->Release(); g_pd3dDeviceContext = NULL; }
    if (g_pd3dDevice) { g_pd3dDevice->Release(); g_pd3dDevice = NULL; }
}

void CreateRenderTarget() {
    ID3D11Texture2D *pBackBuffer;
    g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
    if (!pBackBuffer) return;
    g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_mainRenderTargetView);
    pBackBuffer->Release();
}

void CleanupRenderTarget() {
    if (g_mainRenderTargetView) { g_mainRenderTargetView->Release(); g_mainRenderTargetView = NULL; }
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//Since our compile target is Windows Vista (see targetver.h) we do not have this defined in
//windows.h.
#define WM_DPICHANGED       0x02E0
// Win32 message handler
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
        return true;

    switch (msg) {
    case WM_SIZE:
        if (g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED) {
            CleanupRenderTarget();
            g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
            CreateRenderTarget();
        }
        return 0;
    case WM_SYSCOMMAND:
        if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
            return 0;
        break;
    case WM_DESTROY:
        ::PostQuitMessage(0);
        return 0;
    case WM_CLOSE:
        events.request_exit = true;
        return 0;
    case WM_DPICHANGED:
    {
        current_dpi_mul = HIWORD(wParam)/float(USER_DEFAULT_SCREEN_DPI);
        RECT* const prcNewWindow = (RECT*)lParam;
        ::SetWindowPos(hWnd,
                       NULL,
                       prcNewWindow->left,
                       prcNewWindow->top,
                       prcNewWindow->right - prcNewWindow->left,
                       prcNewWindow->bottom - prcNewWindow->top,
                       SWP_NOZORDER | SWP_NOACTIVATE);
        break;
    }
    case WM_DROPFILES:
        HDROP hDrop = (HDROP)wParam;
        const int num = DragQueryFileW(hDrop, 0xFFFFFFFF, nullptr, 0);
        wchar_t buff[MAX_PATH+1];
        for (int i = 0; i<num; i++)
            if (DragQueryFileW(hDrop, i, buff, MAX_PATH+1))
                events.dropped.full_names.emplace_back(ConvertFromUTF16_to_UTF8(buff));
        return 0;
    }
    return ::LSMProc(hWnd, msg, wParam, lParam);
}

inline uint32_t rot8(uint32_t x) {
    const uint32_t mask = (CHAR_BIT*sizeof(x)-1);
    constexpr unsigned n = 24;
    return (x<<n) | (x>>(unsigned(0-n)&mask));
}

// Simple helper function to load an image into a DX11 texture with common settings
TextureID TextureFromSurface(cairo_surface_t *rgba_surface) {
    // Load from disk into a raw RGBA buffer
    const int image_width = cairo_image_surface_get_width(rgba_surface);
    const int image_height = cairo_image_surface_get_height(rgba_surface);

    if (image_width<=0 || image_height<=0) return {};

    // Create texture
    D3D11_TEXTURE2D_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    desc.Width = image_width;
    desc.Height = image_height;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.SampleDesc.Count = 1;
    desc.Usage = D3D11_USAGE_DEFAULT;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags = 0;

    ID3D11Texture2D *pTexture = NULL;
    D3D11_SUBRESOURCE_DATA subResource;
    const unsigned char * const img_data = cairo_image_surface_get_data(rgba_surface);
    //In cairo ARGB values are stored in native endianness.
    //On a little-endian x86 machine this translates to BGRA byte-order.
    int pixels = image_width*image_height;
    for (uint32_t *p = (uint32_t *)img_data; pixels>0; --pixels, ++p)
        *p = rot8(_byteswap_ulong(*p)); //convert from BGRA to ARGB then to RGBA //TODO: Fix this to work on big-endian, too.
    subResource.pSysMem = img_data;
    subResource.SysMemPitch = desc.Width * 4;
    subResource.SysMemSlicePitch = 0;
    g_pd3dDevice->CreateTexture2D(&desc, &subResource, &pTexture);

    if (pTexture) {
        // Create texture view
        D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
        ZeroMemory(&srvDesc, sizeof(srvDesc));
        srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
        srvDesc.Texture2D.MipLevels = desc.MipLevels;
        srvDesc.Texture2D.MostDetailedMip = 0;
        ID3D11ShaderResourceView* ret;
        g_pd3dDevice->CreateShaderResourceView(pTexture, &srvDesc, &ret);
        pTexture->Release();
        if (ret)
            return (ImTextureID)ret;
    }
    return {};
}

void DestroyTexture(ImTextureID t) {
    if (t) ((ID3D11ShaderResourceView *)t)->Release();
}

//https://stackoverflow.com/a/62614596
HICON CreateIconFromBytes(int width, int height, const void* bytes) {
    HICON hIcon = NULL;

    ICONINFO iconInfo = {
        TRUE, // fIcon, set to true if this is an icon, set to false if this is a cursor
        NULL, // xHotspot, set to null for icons
        NULL, // yHotspot, set to null for icons
        NULL, // Monochrome bitmap mask, set to null initially
        NULL  // Color bitmap mask, set to null initially
    };

    uint32_t* rawBitmap = new uint32_t[width * height];
    const uint32_t* in_bytes = (const uint32_t*)bytes;

    ULONG uWidth = (ULONG)width;
    ULONG uHeight = (ULONG)height;
    uint32_t* bitmapPtr = rawBitmap;
    for (ULONG y = 0; y < uHeight; y++) {
        for (ULONG x = 0; x < uWidth; x++) {
            // Bytes are expected to be in ARGB order (8 bits each)
            // Swap G and B bytes, so that it is in ABGR order for windows
            uint32_t pixel = in_bytes[x + y * width];
            uint8_t A = (pixel & 0xff000000) >> 24;
            uint8_t R = (pixel & 0xff0000) >> 16;
            uint8_t G = (pixel & 0xff00) >> 8;
            uint8_t B = (pixel & 0xff);
            *bitmapPtr = (A << 24) | (B << 16) | (G << 8) | R;
            bitmapPtr++;
        }
    }

    iconInfo.hbmColor = CreateBitmap(width, height, 1, 32, rawBitmap);
    if (iconInfo.hbmColor) {
        iconInfo.hbmMask = CreateBitmap(width, height, 1, 1, nullptr);
        if (iconInfo.hbmMask) {
            hIcon = CreateIconIndirect(&iconInfo);
            DeleteObject(iconInfo.hbmMask);
        }
        DeleteObject(iconInfo.hbmColor);
    }

    delete[] rawBitmap;

    return hIcon;
}
