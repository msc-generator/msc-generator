%define api.prefix {msc_compile_}
%param{msc::MscChart &chart}
%parse-param{procedure_parse_helper &proc_helper}
%param{msc::lexing_state &current_state}
%code requires { #undef C_S_H_IS_COMPILED }
