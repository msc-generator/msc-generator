                                                                /*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file MscGenConf.cpp File for the Config object common between the DLL and App
 * @ingroup Msc_generator_files */


#include "stdafx.h"
#include "MscGenConf.h"
#include "ChartData.h"
#ifndef SHARED_HANDLERS
#include "MainFrm.h" //to set language
#include "Msc-generator.h" //to set data recovery with lang
#endif
#include "msc.h"
#include "msccsh.h"
#include "graphchart.h"
#include "flowchart.h"
#include "xxxchart.h"
#include "blockchart.h"
#include "commandline.h" //VersionText
#include "utf8utils.h"

 // CAboutDlg dialog used for App About

CMscGenConf::CMscGenConf()
{
    //Determine and cache current scaling - this shall be
    //updated when a WM_DPICHANGE event happens
    //But we do not support that yet, as this is available only on Win8.1
    HDC hdc = GetDC(nullptr);
    if (hdc) {
        m_DPIScaling = double(GetDeviceCaps(hdc, LOGPIXELSX))/96; //96 is the default DPI, when we have 150% zoom.
        ReleaseDC(nullptr, hdc);
    } else {
        m_DPIScaling = 1;
    }
    m_bShowControls = false;
    m_bAutoPaginate = false;
    m_bAutoHeading = true;
    m_bGraphUsesOriginalEngine = false;
    m_iScale4Pagination = -1; //Percentage, -1=Fit Witdth, -2=Fit Page
    m_iPageAlignment = -4;
    m_PrinterScale = XY(1, 1);
    m_PhyPrinterPageSize = GetPhysicalPageSize(EPageSize::A4P);
    m_pageSize = EPageSize::A4P;
    for (unsigned u = 0; u<4; u++)
        m_printer_phy_margins[u] = 0;
    for (unsigned u = 0; u<4; u++)
        m_printer_usr_margins[u] = 36;  //uniform half inch margin

    m_bRecoveryOverwrites = true;
    m_bRecoveryDeletes = true;
    m_bTechnicalInfo = false;
    m_bCurrentLineHighLight = true;
    m_bShowLineNums = true;
    m_bSkipCopyright = false;

    m_lang = nullptr;
}

FILE* OpenNamedFile(const char* utf8filename, bool write, bool binary) {
    const std::wstring name = ConvertFromUTF8_to_UTF16(utf8filename);
    FILE* in = nullptr;
    if (_wfopen_s(&in, name.c_str(), write ? binary ? _T("wb") : _T("wt") : binary ? _T("rb") : _T("rt")))
        return nullptr;
    return in;
}

CString GetFolder(unsigned folder);

CString CMscGenConf::Init()
{
//Initialize charts only here, so all static library variables (like Gvc)
//are already up.
    bool ret = m_languages.AddLanguage(&MscChart::Factory, FileListProc);
    ret &= m_languages.AddLanguage(&GraphChart::Factory, FileListProc);
    ret &= m_languages.AddLanguage(&BlockChart::Factory, FileListProc);
#ifdef _DEBUG
    ret &= m_languages.AddLanguage(&FlowChart::Factory, FileListProc);
    ret &= m_languages.AddLanguage(&XxxChart::Factory, FileListProc);
#endif
    if (ret==false) return L"Not a single language has been compiled into me. Cannot work.";
    m_lang = m_languages.GetLanguage("signalling");
    _ASSERT(m_lang);

    const std::vector<std::string> example_dirs = {
        ConvertFromUTF16_to_UTF8([]()->std::wstring {
            CString path;
            GetModuleFileName(NULL, path.GetBuffer(MAX_PATH), MAX_PATH);
            path.ReleaseBuffer();
            std::filesystem::path p((const wchar_t*) path);
            try { return (canonical(p).parent_path().parent_path() / L"examples").native()+L"\\"; }
            catch (std::filesystem::filesystem_error&) { return {}; }
        }()), //For a binary compiled in-place, we assume C:\XXX\Release\msc-gen.exe as argv[0]. We return "C:\XXX\examples\"
        (const char*)AsUTF8(GetFolder(1)+L"examples\\"),   //<INSTALLDIR>\examples (for a binary properly installed)
        (const char*)AsUTF8(GetFolder(2)+L"examples\\"),   //<C:\Users\<user>\AppData\Roaming\Msc-generator\examples (for ... I dunno why)
    };

    m_examples.emplace(&m_languages).Load(example_dirs, OpenNamedFile);
    srand((unsigned)time(nullptr));

    //Read options from the registry
    ReadRegistryValues(false);

    return ReadAllDesigns();
}

bool CMscGenConf::SetLanguage(const LanguageData *l)
{
    if (l &&  m_lang!=l) {
        m_lang = l;
#ifndef SHARED_HANDLERS
        CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(theApp.GetMainWnd());
        if (pMainWnd) {
            pMainWnd->SetLanguageOnRibbon();
            //if the recovery panel is on and we filter by language type, re-create list
            if (GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE) &&
                pMainWnd->m_wndAutoSaveFiles.IsVisible()) {
                pMainWnd->m_wndAutoSaveFiles.CollectAutoSavedFiles(theApp.GetDataRecoveryHandler()->GetAutosavePath(), m_lang);
                pMainWnd->m_wndAutoSaveFiles.ShowIfHasEntries();
            }
        }
#endif
    }
    return l!=nullptr;
}




double CMscGenConf::GetCurrentDPIScalingFactor() const
{
    return m_DPIScaling;
}

/** Adjust the margins specified by the user such that
* there is always at least 72 points of printable space among them
* (if possible).
* @returns FALSE, if paper is too small for this.*/
bool CMscGenConf::NormalizeUserMargins()
{
    //Normalize user margins, have at least 72 points of printable space
    const XY ps = GetPrintablePaperSize();
    if (ps.x<0) {
        m_printer_usr_margins[0] += ps.x/2;
        m_printer_usr_margins[1] += ps.x/2;
    }
    if (ps.y<0) {
        m_printer_usr_margins[2] += ps.y/2;
        m_printer_usr_margins[3] += ps.y/2;
    }
    return ps.x<0 || ps.y<0;
}


/** Convert a libmscgen color sytnax flag (defined in csh.h) to
 * that of Windows' CHARFORMAT*/
DWORD ConvertMscColorSyntaxFlagsToCHARFORMATFlags(int input)
{
	return ((input & COLOR_FLAG_BOLD     ) ? CFM_BOLD      : 0) |
		   ((input & COLOR_FLAG_ITALICS  ) ? CFM_ITALIC    : 0) |
		   ((input & COLOR_FLAG_UNDERLINE) ? CFM_UNDERLINE : 0) |
		   ((input & COLOR_FLAG_COLOR    ) ? CFM_COLOR     : 0);
}


/** Convert a libmscgen color sytnax appearance (defined in csh.h) to
 * that of Windows' CHARFORMAT*/
void ConvertMscCshAppearanceToCHARFORMAT(const ColorSyntaxAppearance &app, CHARFORMAT &cf)
{
	cf.cbSize      = sizeof(cf);
    cf.dwMask      = ConvertMscColorSyntaxFlagsToCHARFORMATFlags(app.mask);
	cf.dwEffects   = ConvertMscColorSyntaxFlagsToCHARFORMATFlags(app.effects & ~COLOR_FLAG_COLOR);
	cf.crTextColor = RGB(app.r, app.g, app.b);
    //cf.dwMask |= CFM_CHARSET | CFM_FACE;
    //cf.bCharSet = DEFAULT_CHARSET;
    //cf.bPitchAndFamily = FIXED_PITCH | FF_MODERN;
    //cf.szFaceName[0] = 0;
}




/** Read our stored settings from the registry and reads the design library.
* @param [in] reportProblem If true, we report any problems.*/
void CMscGenConf::ReadRegistryValues(bool /*reportProblem*/)
{
    //Load Registry values
    m_Pedantic = GetRegistryInt(REG_KEY_PEDANTIC, FALSE);
    m_Warnings = GetRegistryInt(REG_KEY_WARNINGS, TRUE);
    m_FullPath = GetRegistryInt(REG_KEY_FULL_PATH_IN_ERRORS, FALSE);
    m_bPageBreaks = GetRegistryInt(REG_KEY_PB_EDITING, FALSE);
    m_uFallbackResolution = GetRegistryInt(REG_KEY_FALLBACK_RESOLUTION, 300);
    m_bShowCsh = GetRegistryInt(REG_KEY_CSHENABLED, TRUE);
    m_languages.cshParams->color_scheme = GetRegistryInt(REG_KEY_CSHSCHEME, 1);
    if (m_languages.cshParams->color_scheme >= CSH_SCHEME_MAX) m_languages.cshParams->color_scheme = 1;
    m_bSmartIndent = GetRegistryInt(REG_KEY_SMARTINDENT, TRUE);
    m_bTABIndents = GetRegistryInt(REG_KEY_TABINDENTS, TRUE);;
    m_bShowCshErrors = GetRegistryInt(REG_KEY_CSHERRORS, TRUE);
    m_bShowCshErrorsInWindow = GetRegistryInt(REG_KEY_CSHERRORSINWINDOW, FALSE);
    m_trackLineColor = GetRegistryInt(REG_KEY_TRACKLINERGBA, RGBA(128, 0, 0, 255));
    m_trackFillColor = GetRegistryInt(REG_KEY_TRACKFILLRGBA, RGBA(255, 0, 0, 128));
    m_bHints = GetRegistryInt(REG_KEY_HINT, TRUE);
    m_bHintLineStart = GetRegistryInt(REG_KEY_HINT_LINESTART, TRUE);
    m_bHintEntity = GetRegistryInt(REG_KEY_HINT_ENTITY, TRUE);
    m_bHintEscape = GetRegistryInt(REG_KEY_HINT_ESCAPE, TRUE);
    m_bHintAttrName = GetRegistryInt(REG_KEY_HINT_ATTRNAME, TRUE);
    m_bHintAttrValue = GetRegistryInt(REG_KEY_HINT_ATTRVALUE, TRUE);
    m_bHintKeywordMarker = GetRegistryInt(REG_KEY_HINT_KEYWORD_MARKER, TRUE);
    m_iHintFilter = GetRegistryInt(REG_KEY_HINT_FILTER, 2);
    m_bHintCompact = GetRegistryInt(REG_KEY_HINT_COMPACT, TRUE);
    m_bShowControls = GetRegistryInt(REG_KEY_SHOW_CONTROLS, TRUE);
    m_bAutoPaginate = GetRegistryInt(REG_KEY_AUTO_PAGINATE, FALSE);
    m_bAutoHeading = GetRegistryInt(REG_KEY_AUTO_HEADING, TRUE);
    m_iScale4Pagination = GetRegistryInt(REG_KEY_SCALE4PAGINATION, -1);
    m_iPageAlignment = GetRegistryInt(REG_KEY_PAGE_ALIGNMENT, -4);
    m_mscgen_compat = EMscgenCompat(GetRegistryInt(REG_KEY_MSCGEN_COMPAT, int(EMscgenCompat::AUTODETECT)));
    msc::MscCsh *p = dynamic_cast<msc::MscCsh*>(m_languages.GetLanguage("signalling")->pCsh.get());
    if (p)
        p->mscgen_compat = m_mscgen_compat;
    m_bWarnMscgenCompat = GetRegistryInt(REG_KEY_MSCGEN_COMPAT_WARN, TRUE);

    m_bRecoveryOverwrites = GetRegistryInt(REG_KEY_RECOVERY_OVERWRITE, 1);
    m_bRecoveryDeletes = GetRegistryInt(REG_KEY_RECOVERY_DELETE, 1);
    m_bTechnicalInfo = GetRegistryInt(REG_KEY_TECHNICAL_INFO, 0);
    m_bCurrentLineHighLight = GetRegistryInt(REG_KEY_CURRENT_LINE_HIGHLIGHT, 1);
    m_bShowLineNums = GetRegistryInt(REG_KEY_SHOW_LINE_NUMS, 1);
    m_bSkipCopyright = GetRegistryInt(REG_KEY_SKIP_COPYRIGHT, 0);
    m_autopaste = GetRegistryInt(REG_KEY_AUTO_PASTE, 1);
    m_instant_compilation = GetRegistryInt(REG_KEY_INSTANT_COMPILATION, 1);
    m_auto_save = GetRegistryInt(REG_KEY_AUTO_SAVE, 0);

    CString val;
    val = GetRegistryString(REG_KEY_PAGE_MARGIN_L, _T("28.3464567")); //so many points per cm
    if (_ttof(val)) //we dont let the user specify 0...
        m_printer_usr_margins[0] = _ttof(val);
    val = GetRegistryString(REG_KEY_PAGE_MARGIN_R, _T("28.3464567")); //so many points per cm
    if (_ttof(val))
        m_printer_usr_margins[1] = _ttof(val);
    val = GetRegistryString(REG_KEY_PAGE_MARGIN_T, _T("28.3464567")); //so many points per cm
    if (_ttof(val))
        m_printer_usr_margins[2] = _ttof(val);
    val = GetRegistryString(REG_KEY_PAGE_MARGIN_B, _T("28.3464567")); //so many points per cm
    if (_ttof(val))
        m_printer_usr_margins[3] = _ttof(val);

    m_NppPath = _T("C:\\Program Files\\Notepad++\\notepad++.exe");
    CFileFind finder;
    if (!finder.FindFile(m_NppPath))
        m_NppPath.Empty(); //empty value means no NPP on this system

    switch (GetRegistryInt(REG_KEY_TEXTEDITORTYPE, NPP)) {
    case 1:
        if (m_NppPath.GetLength()) {
            m_sStartTextEditor = _T("\"") +m_NppPath+ _T("\" -multiInst -nosession %n");
            m_sJumpToLine = _T("\"") +m_NppPath+ _T("\" -n%l -nosession %n");
            m_iTextEditorType = NPP;
            break;
        }
        FALLTHROUGH; //if notepad++ not found
    default:
    case 0:
        m_sStartTextEditor = "Notepad.exe %n";
        m_sJumpToLine = "";
        m_iTextEditorType = NOTEPAD;
        break;
    case 2:
        m_sStartTextEditor = GetRegistryString(REG_KEY_STARTTEXTEDITOR, _T("Notepad.exe %n"));
        m_sJumpToLine = GetRegistryString(REG_KEY_JUMPTOLINE, _T(""));
        m_iTextEditorType = OTHER;
        break;
    }
    auto ls = m_languages.GetLanguages();
    for (auto &s_lang : ls) {
        CString key = REG_KEY_DEFAULTTEXT + AsUnicode(s_lang);
        CStringA tmp;
        tmp = GetRegistryString(key, _T(""));
        //read old default text
        if (tmp.GetLength()==0 && s_lang == "signalling")
            tmp = GetRegistryString(key, _T(""));
        if (tmp.GetLength())
            m_languages.GetLanguageToModify(s_lang)->default_text = tmp;
    }

    m_CopyrightText = "\\md(0)\\mu(2)\\mr(0)\\mn(10)\\f(arial)\\pr\\c(150,150,150)"
        "https://gitlab.com/msc-generator ";
    m_CopyrightText.Append(VersionText().c_str());

    //Complie csh colors from the variables defined in csh.{cpp,h} into CHARFORMAT elements for faster CSH
    MscInitializeCshAppearanceList();
    for (int scheme = 0; scheme<CSH_SCHEME_MAX; scheme++)
        for (int i = 0; i<COLOR_MAX; i++)
            ConvertMscCshAppearanceToCHARFORMAT(MscCshAppearanceList[scheme][i], m_csh_cf[scheme][i]);
}

/** Get the path to a folder.
* 1==the folder of the executable
* 2==appdata/mscgenerator fodler
* other values return the empty string*/
CString GetFolder(unsigned folder)
{
    TCHAR buff[MAX_PATH];
    if (folder == 1) {
        CString dir = GetRegistryString(REG_KEY_INSTALLDIR, L"");
        if (dir.GetLength()) return dir;
        //If we dont have an installdir, try besides the executable.
        GetModuleFileName(nullptr, buff, MAX_PATH);
        TCHAR *fnamebegin = PathFindFileName(buff);
        *fnamebegin = 0; //equals to 'buff' if no file name is found.
        return buff;
    } else if (folder==2) {
        if (!SUCCEEDED(SHGetFolderPath(nullptr, CSIDL_APPDATA, nullptr, 0, buff)))
            return buff;
        CString ret = buff;
        ret += _T("\\Msc-generator\\");
        return ret;
    }
    return _T("");
}


/** Read the designs from one design library.
* Only the text of the design libraries is collected, no parsing here.
* @param [in] folder Which folder to look for. 0=none - filename contains folder,
*                    1=the folder of the executable, 2=the appdata folder of the user
* @param [in] fileName The name of the file to read from.
*                      Searched in the executable's directory.
*                      Can contain wildcards, all matching files are loaded.
* @param [in] language The name of the language for which fileName is a design lib (or wildcard) for
* @returns The designlibs (filename->text pairs) - only for files that exists*/
std::vector<std::pair<std::string, std::string>>
CMscGenConf::ReadDesigns(unsigned folder, const CString &fileName, std::string_view language)
{
    std::vector<std::pair<std::string, std::string>> ret;
    CString designlib_filename = GetFolder(folder) + fileName;

    CFileFind finder;
    bool errors = false;
    BOOL bFound = finder.FindFile(designlib_filename);
    CStringA text;
    while (bFound) {
        bFound = finder.FindNextFile();
        CChartData data;
        if (data.Load(finder.GetFilePath(), false))
            ret.emplace_back(AsUTF8(finder.GetFilePath()).GetString(), data.GetText().GetString());  //We convert from CStringA to std::string here...
    }
    return ret;
}

/** Read all the design libraries belonging to our supported languages. */
CString CMscGenConf::ReadAllDesigns()
{
    TCHAR currentDir[MAX_PATH];
    GetCurrentDirectory(MAX_PATH, currentDir);

    std::vector<std::pair<std::string, std::string>> design_files;
    for (auto &s_lang : m_languages.GetLanguages()) {
        for (auto &ext : m_languages.GetLanguage(s_lang)->extensions) {
            const CString wext = AsUnicode(ext);
            std::vector<std::pair<std::string, std::string>> tmp = ReadDesigns(1, _T("designlib.")+wext, s_lang);
            if (tmp.size()==0) //designlib.signalling not found in install dir
                tmp = ReadDesigns(1, _T("original_designlib."+wext), s_lang); //use original_designlib.signalling

            std::ranges::move(tmp, std::back_inserter(design_files));
            tmp = ReadDesigns(2, _T("*.")+wext, s_lang); //load designs from appdata
            std::ranges::move(tmp, std::back_inserter(design_files));
        }
    }
    const MscError errors = m_languages.CompileDesignLibs(std::move(design_files));
    if (errors.GetErrorNum(true, false)==0) return L"";

    CStringA errstr = "There were errors in the design library";
    errstr += " file.\n";
    for (unsigned u = 0; u<std::min(10U, errors.GetErrorNum(true, false)); u++) {
        auto sv = errors.GetErrorText(u, true, false);
        errstr += CStringA(sv.data(), int(sv.length())); //we always show full path
        errstr += "\n";
    }
    return AsUnicode(errstr);
}

std::tuple<std::string, std::string, std::string>
CMscGenConf::StaticReadIncludeFile(std::string_view filename, void * config, std::string_view included_from)
{
    CMscGenConf *c = reinterpret_cast<CMscGenConf *>(config);
    _ASSERT(c==GetConf());
    _ASSERT(c);
    if (c) return c->ReadIncludeFile(AsUnicode(filename), AsUnicode(included_from));
    else return{"Internal Error, could not found CMscGenConf.", "",  ""};
}

/** Reads a file that is included in the document.
* @returns error message ("" if OK), filename actually read and read text*/
std::tuple<std::string, std::string, std::string>
CMscGenConf::ReadIncludeFile(CStringW && filename, CStringW &&included_from)
{
    std::tuple<std::string, std::string, std::string> ret;
    filename.Replace(L"/", L"\\"); //convert UNIX style to Windows style
    filename.Replace(L"\\\\", L"\\"); //convert single slash
    if (filename.GetLength()==0) {
        std::get<0>(ret) = "Can not include a file of empty name.";
    } else if (included_from.GetLength()==0 && PathIsRelative(filename)) {
        std::get<0>(ret) = "This is an embedded or yet unsaved chart. Here I can only include files with a fully specified path.";
    } else {
        std::wstring wbuff;
        if (filename[0]==L'\\' || (filename[0] && filename[1]==L':')) {
            //full path: starts with a drive letter or a backslash=>use fn only
            if (filename.GetLength()>MAX_PATH) {
                std::get<0>(ret) = "Specified filepath is too long.";
                return ret;
            }
            wbuff = filename;
        } else {
            if (included_from.GetLength()+filename.GetLength()>MAX_PATH) {
                std::get<0>(ret) = "Specified filepath is too long.";
                return ret;
            }
            wbuff.assign((const wchar_t*)included_from, (const wchar_t*)PathFindFileName(included_from));
            wbuff.append(filename); //copy to the beginning of the filename
        }
        //previously we used this: PathCombine(wbuff, included_from, filename);
        //but that sucked, e.g, could not combine ../../test/ with a filename
        //without deleting the ../../ from the front
        std::string utf8 = ConvertFromUTF16_to_UTF8(wbuff);
        std::get<1>(ret) = utf8;
        FILE *file;
        if (_wfopen_s(&file, wbuff.c_str(), _T("r")) || !file) {
            std::get<0>(ret) = "Could not open file: "+std::get<1>(ret);
        } else {
            std::get<2>(ret) = ReadFile(file);
            //If it happens to be Unicode, convert input text to UTF-8
            ConvertToUTF8(std::get<2>(ret), false);
            fclose(file);
        }
    }
    return ret;
}

std::vector<std::string> CMscGenConf::FileListProc(std::string_view prefix, std::string_view included_from)
{
    auto c = GetConf();
    if (c) return c->FileListProc(AsUnicode(prefix), AsUnicode(included_from));
    else return{};
}

std::vector<std::string> CMscGenConf::FileListProc(CStringW && prefix, CStringW && included_from)
{
    prefix.Replace(L"\\\\", L"\\"); //convert double backslashes
    CStringW dir = prefix;
    //set the beginning of the filename to zero - leaving only the path
    dir.Replace(L"/", L"\\"); //convert UNIX style to Windows style
    int l = dir.ReverseFind(L'\\')+1;  //Reversefind returns -1 if not found this makes 'l' zero.
    prefix.Delete(l, MAX_PATH); //truncate 'prefix' at the same place as 'dir' - but prefix may have UNIX slashes and PathFileName may not process it well.
    dir.Delete(l, MAX_PATH);    //truncate 'dir'
    if (included_from.GetLength()==0 && (dir.GetLength()==0 || PathIsRelative(dir)))
        return{}; //cannot determine directory
    *PathFindFileName(included_from) = 0; //set the beginning of the filename to zero - leaving only the path
    wchar_t wbuff[MAX_PATH];
    PathCombine(wbuff, included_from, dir);
    wbuff[wcslen(wbuff)+1] = 0;
    wbuff[wcslen(wbuff)] = L'*';     //append "*"
    std::vector<std::string> ret;

    CFileFind finder;
    BOOL bFound = finder.FindFile(wbuff);
    CStringA text;
    while (bFound) {
        bFound = finder.FindNextFile();
        CStringW fname = finder.GetFileName();
        if (fname==L".") continue; //Remove '.', but keep '..'
        fname = prefix+fname;
        if (finder.IsDirectory())
            fname += L"\\";
        std::string_view utf16(reinterpret_cast<const char*>(fname.GetString()), fname.GetLength()*2);
        ret.push_back(ConvertFromUTF16_to_UTF8(utf16));
    }
    return ret;
}

