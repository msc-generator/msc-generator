/*
This file is part of Msc-generator.
Copyright (C) 2008-2024 Zoltan Turanyi
Distributed under GNU Affero General Public License.

Msc-generator is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Msc-generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file OutputView.cpp The implementation of the view showing error lists.
* @ingroup Msc_generator_files */

#include "stdafx.h"
#ifndef SHARED_HANDLERS
#include "Msc-generator.h"
#include "MainFrm.h"
#endif
#include "OutputView.h"
#include <iterator>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COutputList1


COutputList::EMessageType COutputList::DetermineMessageType(const CString & strText) const
{
    if (strText.Find(L"error: ")>=0)
        return EMessageType::ERR;
    else if (strText.Find(L"warning: ")>=0)
        return EMessageType::WARNING;
    else if (strText.Find(L"info: ")>=0)
        return EMessageType::TECHNICAL_INFO;
    else if (strText.Find(L": (")>=0)
        return EMessageType::AUX;
    else if (strText.Find(L"Hint: ")>=0)
        return EMessageType::HINT;
    return EMessageType::UNKNOWN;
}

void COutputList::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    CClientDC dc(this);
    CString strText("MM");
    //GetText(lpMeasureItemStruct->itemID, strText); //we have fixed height for all elements
    CSize size = dc.GetTextExtent(strText);
    lpMeasureItemStruct->itemHeight = size.cy;
    //lpMeasureItemStruct->itemWidth = size.cx;
}


void COutputList::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    //CRect members to store the position of the items
    CRect rItem;
    CDC* dc = CDC::FromHandle(lpDrawItemStruct->hDC);

    if ((int)lpDrawItemStruct->itemID < 0) {
        // If there are no elements in the CListBox
        // based on whether the list box has Focus  or not
        // draw the Focus Rect or Erase it,
        if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && (lpDrawItemStruct->itemState & ODS_FOCUS)) {
            dc->DrawFocusRect(&lpDrawItemStruct->rcItem);
        } else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && !(lpDrawItemStruct->itemState & ODS_FOCUS)) {
            dc->DrawFocusRect(&lpDrawItemStruct->rcItem);
        }
        return;
    }

    // String to store the text
    CString strText;
    // Get the item text.
    GetText(lpDrawItemStruct->itemID, strText);

    //Initialize the CListBox Item's row size
    rItem = lpDrawItemStruct->rcItem;

    UINT nFormat = DT_LEFT | DT_SINGLELINE | DT_VCENTER;
    if (GetStyle() & LBS_USETABSTOPS)
        nFormat |= DT_EXPANDTABS;

	//Color for this item. First is a paler version, second is a stronger one.
	std::pair<COLORREF, COLORREF> color;

	if (lpDrawItemStruct->CtlID == IDC_HINT_ERROR_LIST)
		color = {RGB(192, 192, 255), RGB(0, 0, 128)}; //hints are bluish
	else if (lpDrawItemStruct->CtlID == IDC_ERROR_LIST)
		switch (DetermineMessageType(strText)) {
			case EMessageType::ERR:
				color = {RGB(255, 192, 192), RGB(128, 0, 0)}; //errors redish
				break;
			case EMessageType::WARNING:
				color = {RGB(192, 255, 192), RGB(0, 128, 0)}; //warnings greenish
				break;
			case EMessageType::TECHNICAL_INFO:
				color = {RGB(192, 224, 255), RGB(0, 64, 128)}; //info green-bluish
				break;
			default: //do nothing for hints and unknown
				_ASSERT(0);
				FALLTHROUGH;
			case EMessageType::AUX:
				color = {RGB(0,0,0), RGB(128, 128, 128)}; //additional info greyish
				break;
		}

    bool selected = false;
    // If CListBox item selected, draw the highlight rectangle.
    // Or if CListBox item deselected, draw the rectangle using the window color.
    if ((lpDrawItemStruct->itemState & ODS_SELECTED) &&
        (lpDrawItemStruct->itemAction & (ODA_SELECT | ODA_DRAWENTIRE))) {
        CBrush br(color.second);
        dc->FillRect(&rItem, &br);
        selected = true;
    } else if (!(lpDrawItemStruct->itemState & ODS_SELECTED) && (lpDrawItemStruct->itemAction & ODA_SELECT)) {
        CBrush br(::GetSysColor(COLOR_WINDOW));
        dc->FillRect(&rItem, &br);
    }

    // If the CListBox item has focus, draw the focus rect.
    // If the item does not have focus, erase the focus rect.
    if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && (lpDrawItemStruct->itemState & ODS_FOCUS)) {
        dc->DrawFocusRect(&rItem);
    } else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && !(lpDrawItemStruct->itemState & ODS_FOCUS)) {
        dc->DrawFocusRect(&rItem);
    } else {
        //If we do a non-focus operation, we draw the text
        //(Drawing the text for focus operations would just draw it twice - ugly)
        // To draw the Text in the CListBox set the background mode to Transparent 
        int iBkMode = dc->SetBkMode(TRANSPARENT);
		if (selected)
			dc->SetTextColor(RGB(255, 255, 255));
		else
			dc->SetTextColor(color.second);
        dc->SelectObject(&afxGlobalData.fontRegular);

        //Draw the Text
        dc->TextOut(rItem.left, rItem.top, strText);
    }
}

/** Called by the framework at events reflected back to us.
 * We handle double-clicks and notify the document that an error was selected.*/
BOOL COutputList::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
    if (message==WM_COMMAND) 
        switch (HIWORD(wParam)) {
        case LBN_DBLCLK:
            CWnd *pParent = GetParent();
            _ASSERT(pParent);
            if (pParent) {
                COutputViewBar *pBar = dynamic_cast<COutputViewBar*>(pParent->GetParent());
                _ASSERT(pBar);
                if (pBar)
                    pBar->JumpToCurrentError(false);
            }
    }
    return CListBox::OnChildNotify(message, wParam, lParam, pResult); 
}

/////////////////////////////////////////////////////////////////////////////
// COutputBar

BEGIN_MESSAGE_MAP(COutputViewBar, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

/** Create the windows objects.*/
int COutputViewBar::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create tabs window:
	if (!m_wndTabs.Create(CMFCTabCtrl::STYLE_FLAT, rectDummy, this, 1))
	{
		TRACE0("Failed to create output tab\n");
		return -1;      // fail to create
	}
    m_wndTabs.HideNoTabs();

	// Create error lists:
	const DWORD dwStyle = LBS_NOINTEGRALHEIGHT | WS_CHILD | WS_VISIBLE | 
                          WS_HSCROLL | WS_VSCROLL | LBS_NOTIFY | LBS_OWNERDRAWFIXED | LBS_HASSTRINGS;

	if (!m_wndOutput.Create(dwStyle, rectDummy, &m_wndTabs, IDC_ERROR_LIST) ||
        !m_wndOutputHints.Create(dwStyle, rectDummy, &m_wndTabs, IDC_HINT_ERROR_LIST))
	{
		TRACE0("Failed to create error lists\n");
		return -1;      // fail to create
	}

	//m_Font.CreateFont(16, 0, 0, 0, FW_NORMAL, false, false, false, ANSI_CHARSET, 
	//                  OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
	//                  FIXED_PITCH|FF_MODERN, nullptr);
    //m_wndOutput.SetFont(&m_Font);
    //m_wndOutputHints.SetFont(&m_Font);
    m_wndOutput.SetFont(&afxGlobalData.fontRegular);
    m_wndOutputHints.SetFont(&afxGlobalData.fontRegular);

	m_wndTabs.AddTab(&m_wndOutput, _T("Compilation Errors"));
	m_wndTabs.AddTab(&m_wndOutputHints, _T("Error Hints"));

	return 0;
}

/** The framework calls this when the pane changes size.
 * We adjust scroll sizes and the size of the tab control.*/
void COutputViewBar::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	m_wndTabs.SetWindowPos (nullptr, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);

	AdjusrHorzScroll(m_wndOutput);
	AdjusrHorzScroll(m_wndOutputHints);
}

/** Set the horizontal size of the virtual canvas in wndListBox.*/
void COutputViewBar::AdjusrHorzScroll(CListBox& wndListBox)
{
	CClientDC dc(this);
	CFont* pOldFont = dc.SelectObject(&m_Font);

	int cxExtentMax = 0;

	for (int i = 0; i < wndListBox.GetCount(); i ++)
	{
		CString strItem;
		wndListBox.GetText(i, strItem);

		if (cxExtentMax < dc.GetTextExtent(strItem).cx)
			cxExtentMax = dc.GetTextExtent(strItem).cx;
	}

	wndListBox.SetHorizontalExtent(cxExtentMax);
	dc.SelectObject(pOldFont);
}

/** Called by the framework to repaint.
 * We draw a bit of a border.*/
void COutputViewBar::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rectTree;
	m_wndOutput.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

/** Called by the framework when we get the focus.
 * We pass the focus to the compilation list.*/
void COutputViewBar::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndOutput.SetFocus();
}


/**Copy the compilation errors from 'chart' to the compilation error listbox.
 * Also store the line/col values so we can jump there.
 * We also activate the compilation error pane, if there were errors.
 * If no errors we entirely hide us.*/
void COutputViewBar::ShowCompilationErrors(const CDrawingChartData &chart)
{
    if (!::IsWindow(m_wndOutput)) return;
	m_wndOutput.ResetContent();
    compilation_errors.clear();
    auto pConf = GetConf();
    if (!pConf) return;
	unsigned num = chart.GetErrorNum(pConf->m_Warnings, pConf->m_bTechnicalInfo);
    error_pos.resize(num);
	for (unsigned i=0; i<num; i++) {
        compilation_errors.push_back(AsUnicode(chart.GetErrorText(i, pConf->m_Warnings, pConf->m_bTechnicalInfo)));
        m_wndOutput.AddString(compilation_errors.back());
        error_pos[i] = chart.GetErrorFileLineCol(i, pConf->m_Warnings, pConf->m_bTechnicalInfo);
    }
    //Select the first error
    if (error_pos.size()>0)
        m_wndTabs.SetActiveTab(0);
    const int tab = m_wndTabs.GetActiveTab();
    if (tab==0) {
        COutputList *const ol = tab==0 ? &m_wndOutput : &m_wndOutputHints;
        int maxsel = ol->GetCount();
        if (maxsel != LB_ERR && maxsel!=0) {
            ol->SetCurSel(maxsel-1);
            NextError(true, false);
            //If we dont have any error or warning to act as a large step, 
            //NextError takes to the last entry.
            if (ol->GetCurSel()==maxsel-1) {
                const std::list<CString> &strs = tab==0 ? compilation_errors : compilation_errors_hint;
                //if that is not eligible for the last step, we selet the first instead.
                if (ol->DetermineMessageType(strs.back())>COutputList::EMessageType::HINT)
                    NextError(true, true); //cycles back to the first error
            }
        }
    }
    //turn off only if m_bShowCshErrorsInWindow is false && no errors
    ShowPane(error_pos.size()>0, false, true);
}


/**Copy the hinted errors from 'errors' and 'err_pos' to the compilation error listbox.
* Also store the line/col values so we can jump there.
* We also activate the hints error pane, if there were errors.
* If no errors we entirely hide us.*/
void COutputViewBar::ShowCshErrors(const std::vector<CString> &errors,
                                   std::vector<FileLineCol> &&err_pos) 
{
    if (!::IsWindow(m_wndOutputHints)) return;
    m_wndOutputHints.SetRedraw(false);
	m_wndOutputHints.ResetContent();
    for (auto &s : errors) 
        m_wndOutputHints.AddString(s);
    error_pos_hint = std::move(err_pos);
    //ShowPane(true, true, false);  //do not do this! If user wants them, let him turn errors on
    m_wndOutputHints.SetRedraw(true);
    //turn off only if m_bShowCshErrorsInWindow is false && no errors
    if (error_pos.size()==0 && error_pos_hint.size()>0) 
        m_wndTabs.SetActiveTab(1); //bring us forth if new syntax errors appear and there are no comp errors
    if (error_pos.size()>0 || (GetConf()->m_bShowCshErrorsInWindow && error_pos_hint.size()>0))
        ShowPane(true, false, true);
}

/** We jump to the next/prev error (in the currently showing list).
 * @param [in] next True if we shall step forward, false if backward.
 * @param [in] small_step If true we step into grey stuff (auxiliary info), as well. 
 *                        Else we jump to the next red/green/blue errors/warnings/hints only.*/
void COutputViewBar::NextError(bool next, bool small_step)
{
    const int tab = m_wndTabs.GetActiveTab();
    if (tab<0) return;
    COutputList *const ol = tab==0 ? &m_wndOutput : &m_wndOutputHints;
	int maxsel = ol->GetCount();
    if (maxsel == LB_ERR || maxsel==0) return;
	int cursel = ol->GetCurSel();
    if (cursel<0) cursel = next ? maxsel-1 : 0;
    const int orig_cursel = cursel;
	cursel = (cursel + (next ? 1 : -1) + maxsel) % maxsel;
    const std::list<CString> &strs = tab==0 ? compilation_errors : compilation_errors_hint;
    if (!small_step && tab==0)
        while (cursel!=orig_cursel) {
            auto i = strs.begin();
            std::advance(i, cursel);
            if (ol->DetermineMessageType(*i)<=COutputList::EMessageType::HINT)
                break;
            cursel = (cursel + (next ? 1 : -1) + maxsel) % maxsel;
        }
	ol->SetCurSel(cursel);
}

/** Gets the line and column of the current error.
 * @returns nothing if there is no error currently selected. 
 *          An invalid filepos if the current error has no pos associated.*/
std::optional<FileLineCol> COutputViewBar::GetCurrentErrorLine()
{
    const int tab = m_wndTabs.GetActiveTab();
    if (tab<0) return {};
    COutputList *const ol = tab==0 ? &m_wndOutput : &m_wndOutputHints;
	int maxsel = ol->GetCount();
    if (maxsel == LB_ERR || maxsel==0) return {}; //no error
	int cursel = ol->GetCurSel();

    const auto &e = tab==0 ? error_pos : error_pos_hint;
    //exit if we do not know the location of the error
    if (cursel >= int(e.size()) || cursel<0) return {};

    //Return the error loc (may be invalid)
    return e[cursel];
}

void COutputViewBar::JumpToCurrentError(bool silent)
{
#ifndef SHARED_HANDLERS
    CMscGenDoc *pDoc = GetMscGenDocument();
    const auto l = GetCurrentErrorLine();
    if (!l || !pDoc || 
        (l->file != CMscGenDoc::FILE_NO_FOR_CSH_ERRORS && 
         !pDoc->m_ChartShown.IsErrorInFile(l.value()))) {
        if (!silent)
            MessageBeep(MB_ICONASTERISK);
        return;
    }

    //Turn tracking off
    if (pDoc->m_bTrackMode) pDoc->SetTrackMode(false);
    else pDoc->StartFadingAll();

    //Jump in the internal and external editors
    if (IsInternalEditorVisible())
        theApp.m_pWndEditor->m_ctrlEditor.JumpToLine(l->line, l->col);
    if (pDoc->m_ExternalEditor.IsRunning())
        pDoc->m_ExternalEditor.JumpToLine(l->line, l->col);

    //Show tracking boxes for the error
    pDoc->AddAnimationTrackRect(pDoc->m_ChartShown.GetArc(l->line, l->col));
#endif
}

////////////////////////////////////////////////////////////

SAutoSavedFile::SAutoSavedFile(const CString & d_name, CTime time)
    : disk_name(d_name), date(time)
{
    int fname_pos = disk_name.ReverseFind('\\');
    int dot = disk_name.Find('.', std::max(0, fname_pos));
    display_name = disk_name.Mid(std::max(0, dot+1));
    CTimeSpan since = CTime::GetCurrentTime()-time;
    if (since >= CTimeSpan(7, 0, 0, 0))
        date_string = date.Format(_T("%Y-%m-%d - %H:%M"));
    else if (since >= CTimeSpan(2, 0, 0, 0))
        date_string = since.Format(_T("%D days ago"));
    else if (since >= CTimeSpan(1, 0, 0, 0))
        date_string = since.Format(_T("%D day ago"));
    else if (since >= CTimeSpan(0, 2, 0, 0))
        date_string = since.Format(_T("%H hours ago"));
    else if (since >= CTimeSpan(0, 1, 0, 0))
        date_string = since.Format(_T("%H hour ago"));
    else if (since >= CTimeSpan(0, 0, 2, 0))
        date_string = since.Format(_T("%M minutes ago"));
    else if (since >= CTimeSpan(0, 0, 1, 0))
        date_string = since.Format(_T("%M minute ago"));
    else if (since >= CTimeSpan(0, 0, 0, 2))
        date_string = since.Format(_T("%S seconds ago"));
    else
        date_string = since.Format(_T("%S second ago"));
    if (date_string[0]=='0')
        date_string.Delete(0, 1);
}

std::string SAutoSavedFile::GenerateListBoxText() const
{
    return (const char*)AsUTF8(_T("\\pl\\mu(5)\\md(5)\\ml(5)\\mr(5)") + display_name + _T("\\n\\b\\-") + date_string);
}



const SAutoSavedFile * CAutoSavedFilesList::GetFileByIndex(int index) 
{ 
    if (index<0 || (unsigned)index>=m_files.size())
        return nullptr;
    else 
        return &m_files[index]; 
}

CAutoSavedFilesList::CAutoSavedFilesList()
{
    m_format.Default();
}

void CAutoSavedFilesList::DrawItem(LPDRAWITEMSTRUCT lpItem)
{
    //Do not draw focus rectangle (or anything else) for empty lists
    if (lpItem->itemID==-1) return;
    //We do not differentiate in appearance whether we have the focus or not,
    //so for ODA_FOCUS, nothing to do
    //if (lpItem->itemAction != ODA_DRAWENTIRE && lpItem->itemAction != ODA_SELECT) return;

    auto const item = GetFileByIndex(lpItem->itemID);
    if (item==nullptr) return;

    /* Cairo 1.12.8 has a bug with Windows Display surfaces, when the original clip of the
    surface is nonzero. This results in the surface having a nonzero extent origin. When
    operations requiring fallback are done, the map_to_image function screws up.
    This bug applies here as the DC we get here is clipped to the actual item.
    We work around by first creating a DIB-based DC (no clip, zero extent origin)
    and by BitBlt-ing that to the original DC.*/

    CDC origDC, memDC;
    origDC.Attach(lpItem->hDC);
    memDC.CreateCompatibleDC(&origDC);
    CBitmap *oldBitmap;
    CBitmap bitmap;
    bitmap.CreateCompatibleBitmap(&origDC, lpItem->rcItem.right-lpItem->rcItem.left,
        lpItem->rcItem.bottom - lpItem->rcItem.top);
    oldBitmap = memDC.SelectObject(&bitmap);
    memDC.FillSolidRect(0, 0, lpItem->rcItem.right-lpItem->rcItem.left,
        lpItem->rcItem.bottom - lpItem->rcItem.top, origDC.GetBkColor());
    Canvas canvas(Canvas::WIN, memDC.m_hDC);
    const std::string text = item->GenerateListBoxText();
    ShapeCollection dummy_shapes;
    Label label(text, canvas, dummy_shapes, m_format);
    const ColorType black(0, 0, 0);
    const FillAttr fill(black.Lighter(0.75), EGradientType::DOWN);
    LineAttr line(ELineType::DASHED, black.Lighter(0.5), 1, ECornerType::ROUND, 3);
    Block b(0, lpItem->rcItem.right-lpItem->rcItem.left,
        0, lpItem->rcItem.bottom - lpItem->rcItem.top);
    canvas.Fill(b, line, fill);
    label.Draw(canvas, dummy_shapes, 0, lpItem->rcItem.right-lpItem->rcItem.left, 0);
    if (lpItem->itemState & ODS_SELECTED)
        canvas.Line(b.CreateExpand(-1), line);
    canvas.CloseOutput();
    origDC.BitBlt(lpItem->rcItem.left, lpItem->rcItem.top,
        lpItem->rcItem.right-lpItem->rcItem.left,
        lpItem->rcItem.bottom - lpItem->rcItem.top,
        &memDC, 0, 0, SRCCOPY);
    memDC.SelectObject(oldBitmap);
    origDC.Detach();
}

void CAutoSavedFilesList::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    auto const item = GetFileByIndex(lpMeasureItemStruct->itemID);
    if (item==nullptr) return;
    Canvas canvas(Canvas::Empty::Query);
    const std::string text = item->GenerateListBoxText();
    ShapeCollection dummy_shapes;
    Label label(text, canvas, dummy_shapes, m_format);
    XY wh = label.getTextWidthHeight();
    lpMeasureItemStruct->itemHeight = (unsigned) wh.y;
    lpMeasureItemStruct->itemWidth = (unsigned) wh.x;
}

int CAutoSavedFilesList::CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct)
{
    // return -1 = item 1 sorts before item 2
    // return 0 = item 1 and item 2 sort the same
    // return 1 = item 1 sorts after item 2
    auto const item1 = GetFileByIndex(lpCompareItemStruct->itemID1);
    auto const item2 = GetFileByIndex(lpCompareItemStruct->itemID2);
    if (*item1<*item2) return -1;
    if (*item2<*item1) return -2;
    return 0;
}

/** Called by the framework at events reflected back to us.
* We handle double-clicks and notify the document that a recovered file is to be re-used.*/
BOOL CAutoSavedFilesList::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT * pResult)
{
    if (message==WM_COMMAND)
        switch (HIWORD(wParam)) {
        case LBN_DBLCLK:
            CWnd *pParent = GetParent();
            _ASSERT(pParent);
            if (pParent) {
                CAutoSavedFilesBar *pBar = dynamic_cast<CAutoSavedFilesBar*>(pParent);
                CMscGenDoc *pDoc = GetMscGenDocument();
                _ASSERT(pBar && pDoc);
                if (pDoc) {
                    int s = GetCurSel();
                    if (s>=0)
                        pDoc->RecoverAutoSavedFile(m_files[s].disk_name);
                    return true;
                }
            }
        }
    return CListBox::OnChildNotify(message, wParam, lParam, pResult);
}

void CAutoSavedFilesList::AddFiles(const CString &dir, const LanguageData * lang)
{
    CFileFind finder;
    for (auto &s : lang->extensions) {
        BOOL bFound = finder.FindFile(dir+_T("\\*.")+AsUnicode(s));
        while (bFound) {
            bFound = finder.FindNextFile();
            CTime time;
            finder.GetLastWriteTime(time);
            m_files.emplace_back(finder.GetFilePath(), time);
        }
    }
}


BEGIN_MESSAGE_MAP(CAutoSavedFilesBar, CDockablePane)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_PAINT()
//    ON_WM_SETFOCUS()
END_MESSAGE_MAP()


/** Create the windows objects.*/
int CAutoSavedFilesBar::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // Create error lists:
    const DWORD dwStyle1 = WS_CHILD | WS_VISIBLE | LBS_OWNERDRAWVARIABLE |
        WS_VSCROLL | WS_HSCROLL | LBS_NOTIFY;

    if (!m_wndFiles.Create(dwStyle1, rectDummy, this, IDC_AUTOSAVE_FILE_LIST)) {
        TRACE0("Failed to create filename list\n");
        return -1;      // fail to create
    }

    //const DWORD dwStyle2 = WS_CHILD | WS_VISIBLE | BS_CHECKBOX;
    //const DWORD dwStyle3 = WS_CHILD | WS_VISIBLE | BS_CHECKBOX;
    //if (!m_lang_filter.Create(_T("Filter to this language"), dwStyle2, rectDummy, this, IDC_AUTOSAVE_FILTER_BOX) ||
    //    !m_delete_all.Create(_T("Delete All"), dwStyle3, rectDummy, this, IDC_AUTOSAVE_DELETE_ALL)) {
    //    TRACE0("Failed to create filename list\n");
    //    return -1;      // fail to create
    //}

    return 0;
}

/** The framework calls this when the pane changes size.
* We adjust scroll size.*/
void CAutoSavedFilesBar::OnSize(UINT nType, int cx, int cy)
{
    CDockablePane::OnSize(nType, cx, cy);

    m_wndFiles.SetWindowPos(nullptr, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
    //m_lang_filter.SetWindowPos(nullptr, 5, cy-50, 50, 40,  SWP_NOZORDER);
    //m_delete_all.SetWindowPos(nullptr, 50, cy-50, 50, 40, SWP_NOZORDER);

    Canvas canvas(Canvas::Empty::Query);
    int cxExtentMax = 0;

    for (int i = 0; i < m_wndFiles.GetCount(); i++) {
        auto const item = m_wndFiles.GetFileByIndex(i);
        const std::string text = item->GenerateListBoxText();
        ShapeCollection dummy_shapes;
        Label label(text, canvas, dummy_shapes, m_wndFiles.m_format);
        XY wh = label.getTextWidthHeight();
        if (cxExtentMax < wh.x)
            cxExtentMax = (unsigned)wh.x;
    }

    m_wndFiles.SetHorizontalExtent(cxExtentMax);
}

/** Called by the framework to repaint.
* We draw a bit of a border.*/
void CAutoSavedFilesBar::OnPaint()
{
    CPaintDC dc(this); // device context for painting

    CRect rectTree;
    m_wndFiles.GetWindowRect(rectTree);
    ScreenToClient(rectTree);

    rectTree.InflateRect(1, 1);
    dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

/** Read the auto-saved files from disk and show pane if they exist.
 * Exclude the saved files of currently opened files.
 * if 'force_show' is true, we show even the empty window.*/
bool CAutoSavedFilesBar::CollectAutoSavedFiles(CString dir, const LanguageData *lang)
{
    auto pConf = GetConf();
    if (!pConf) return false;
    m_wndFiles.m_files.clear();
    m_wndFiles.ResetContent();
    if (lang) {
        m_wndFiles.AddFiles(dir, lang);
    } else {
        auto langs = pConf->m_languages.GetLanguages();
        for (auto &s : langs)
            m_wndFiles.AddFiles(dir, pConf->m_languages.GetLanguage(s));
    }
    //remove the current document
    CString autosave_name =
        dynamic_cast<CMscGenDataRecoveryHandler*>(theApp.GetDataRecoveryHandler())->CurrentAutosaveName();
    auto i = std::find_if(m_wndFiles.m_files.begin(), m_wndFiles.m_files.end(), 
        [&autosave_name](const SAutoSavedFile &f) {return autosave_name==f.disk_name; });
    if (i!=m_wndFiles.m_files.end())
        m_wndFiles.m_files.erase(i);

    std::sort(m_wndFiles.m_files.begin(), m_wndFiles.m_files.end());
    for (auto &f : m_wndFiles.m_files)
        m_wndFiles.AddString(f.disk_name);
    return m_wndFiles.m_files.size();
}

/** Show/hide the autosave pane depending on whether we have files to recover or not.
 * @param [in] force_show If true we show the pane even if empty.
 * @returns true if we show the pane*/
bool CAutoSavedFilesBar::ShowIfHasEntries(bool force_show)
{
    ShowPane(force_show||m_wndFiles.m_files.size(), false, true);
    return force_show||m_wndFiles.m_files.size();
}




void CAutoSavedFilesBar::DeleteFilesAndHide()
{
    for (auto &f : m_wndFiles.m_files) {
        try {
            CFile::Remove(f.disk_name);
        }
        catch (CFileException *e) {
            //do noting
        }
    }
#ifndef SHARED_HANDLERS
    //re-create the list
    CollectAutoSavedFiles(
        theApp.GetDataRecoveryHandler()->GetAutosavePath(),
        theApp.GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE) ? GetConf()->m_lang : nullptr);
    if (ShowIfHasEntries())
        return;
    //Switch to the main category if no entries
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
    if (pMainWnd)
        pMainWnd->ActivateHomeCategory();
#endif
}

