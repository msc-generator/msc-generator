/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file ChartData.cpp The definition of the CChartData and CDrawingChartData classes.
* @ingroup Msc_generator_files */

#include "stdafx.h"
#include "MscGenConf.h"
#include "ChartData.h"
#include "csh.h"
#include "utf8utils.h"
#include "graphchart.h"
#include "msc.h"
#include "blockchart.h"
#include "version.h"

void EnsureCRLF(CStringA &str)
{
	int pos = str.Find('\n');
	while (pos >= 0) {
		if (pos == 0 || str[pos-1]!='\r') {
			str.Insert(pos, '\r');
			pos++;
		}
		pos = str.Find('\n', pos+1);
	}
    if (str.GetLength() && str[str.GetLength()-1]!='\n')
        str += "\r\n";
}

//CChartData

void CChartData::swap(CChartData &o)
{
    std::swap(m_text, o.m_text);
    std::swap(m_ForcedDesign, o.m_ForcedDesign);
    std::swap(m_graphLayout, o.m_graphLayout);
    m_GUIState.swap(o.m_GUIState),
    std::swap(m_page_size, o.m_page_size);
    std::swap(m_addHeading, o.m_addHeading);
    std::swap(m_fitWidth, o.m_fitWidth);
    std::swap(ver_a, o.ver_a);
    std::swap(ver_b, o.ver_b);
    std::swap(ver_c, o.ver_c);
    std::swap(m_sel, o.m_sel);
    std::swap(m_scroll, o.m_scroll);
}

/** Sets the name of the document file on disk. Full Path preferably.*/

void CChartData::SetFileName(const CStringW & fn)
{
    m_FileName = AsUTF8(fn);
}


//rets -1 if "this" has newer version, 0 if equal, +1 if "this" has older
//If we have no version or the other is invalid, return 0
int CChartData::CompareVersion(unsigned a, unsigned b, unsigned c) const
{
    if (!HasVersion() || ver_a==0) return 0;
    if (a<ver_a) return -1;
    if (a>ver_a) return +1;
    if (b<ver_b) return -1;
    if (b>ver_b) return +1;
    if (c<ver_c) return -1;
    if (c>ver_c) return +1;
    return 0;
}


BOOL CChartData::Save(const CString &fileName, bool as_unicode)
{
	RemoveSpacesAtLineEnds();
    CStringA text = m_text;
    EnsureCRLF(text);
    TRY {
		CStdioFile outfile(fileName, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary);
        if (text.GetLength()>0)
            TRY {
                if (as_unicode) {
                    std::wstring utf16 = ConvertFromUTF8_to_UTF16(std::string_view(text, text.GetLength()));
                    outfile.Write(utf16.c_str(), (UINT)utf16.length()*2);
                } else {
                    outfile.Write(text, text.GetLength());
                }
            } CATCH(CFileException, pEx) {
                pEx->ReportError();
                outfile.Close();
                return FALSE;
            }
            END_CATCH
		outfile.Close();
	} CATCH(CFileException, pEx) {
		pEx->ReportError();
		return FALSE;
	}
	END_CATCH  // cppcheck-suppress unknownMacro
	return TRUE;
}



BOOL CChartData::Load(const CString &fileName, BOOL force_as_unicode, bool *was_unicode, BOOL reportError)
{
	unsigned length = 0;
    std::string buff;
	if (fileName.GetLength() == 0) return FALSE;
	CStdioFile infile;
	CFileException Ex;
	if (!infile.Open(fileName, CFile::modeRead | CFile::typeText, &Ex)) {
		if (reportError) Ex.ReportError();
		return FALSE;
	}
	length = unsigned(infile.GetLength());
	if (length>0) {
        buff.resize(length);
		TRY {
			length = infile.Read(buff.data(), length);
		} CATCH(CFileException, pEx) {
			infile.Close();
			if (reportError) pEx->ReportError();
			return FALSE;
		}
		END_CATCH
        buff.resize(length);
        //Convert from Unicode
        bool _was_unicode = ConvertToUTF8(buff, force_as_unicode);
        if (was_unicode)
            *was_unicode = _was_unicode;
	}
	infile.Close();
	Delete();
	m_text = buff.c_str();
	RemoveSpacesAtLineEnds();
	RemoveCRLF(m_text);
    const int tabsize = GetRegistryInt(REG_KEY_TABSIZE, 4);
    ReplaceTAB(m_text, tabsize);
    ver_a = ver_b = ver_c = 0;
	return TRUE;
}

void CChartData::RemoveSpacesAtLineEnds()
{
	int pos = m_text.Find('\x0d');  //any \x0a comes *after* \x0d
	while (pos >= 0) {
		int spaces = 0;
		while (pos>spaces+1  && (m_text[pos-spaces-1] == ' ' || m_text[pos-1-spaces] == '\t')) spaces++;
		if (spaces) {
			m_text.Delete(pos-spaces, spaces);
			pos -= spaces;
		}
		pos = m_text.Find('\x0d', pos+1);
	}
}


CDrawingChartData::CDrawingChartData(const CChartData&o) :
    compiled(false), m_main_file_no(-1), m_cacheType(CACHE_RECORDING), m_cache_EMF(nullptr),
    m_cache_rec(nullptr), m_cache_rec_full_no_pb(nullptr), m_wmf_size(0),
    m_fallback_resolution(300), m_page(0), m_pageBreaks(false),
    m_pedantic(false), m_callback(nullptr), m_callback_data(nullptr)
{
    operator=(o);
}

CDrawingChartData::CDrawingChartData(const CDrawingChartData&o) :
    CChartData(o),
    compiled(false), m_main_file_no(-1), m_cacheType(o.m_cacheType), m_cache_EMF(nullptr),
    m_cache_rec(nullptr), m_cache_rec_full_no_pb(nullptr), m_wmf_size(0),
    m_fallback_resolution(o.m_fallback_resolution), m_page(o.m_page),
    m_pageBreaks(o.m_pageBreaks), m_pedantic(o.m_pedantic),
    m_copyright(o.m_copyright),
    m_callback(o.m_callback), m_callback_data(o.m_callback_data), m_load_data(o.m_load_data)
{
}

CDrawingChartData::CDrawingChartData(CDrawingChartData &&o) noexcept :
    CChartData(std::move(o)),
    compiled(false), m_main_file_no(-1), m_cacheType(o.m_cacheType), m_cache_EMF(nullptr),
    m_cache_rec(nullptr), m_cache_rec_full_no_pb(nullptr), m_wmf_size(0),
    m_fallback_resolution(o.m_fallback_resolution), m_page(o.m_page),
    m_pageBreaks(o.m_pageBreaks), m_pedantic(o.m_pedantic),
    m_copyright(std::move(o.m_copyright)),
    m_callback(o.m_callback), m_callback_data(o.m_callback_data),
    m_load_data(std::move(o.m_load_data))
{
}
void CDrawingChartData::Delete(void)
{
    Invalidate();
    CChartData::Delete();
    m_copyright.Empty();
    m_callback=nullptr;
    m_callback_data=nullptr;
    m_load_data.Empty();
}


void CDrawingChartData::swap(CDrawingChartData &o)
{
    CChartData::swap(o);
    std::swap(m_chart, o.m_chart);
    std::swap(compiled, o.compiled);
    std::swap(m_main_file_no, o.m_main_file_no);
    std::swap(m_cacheType, o.m_cacheType);
    std::swap(m_cache_EMF, o.m_cache_EMF);
    std::swap(m_cache_rec, o.m_cache_rec);
    std::swap(m_cache_rec_full_no_pb, o.m_cache_rec_full_no_pb);
    m_fallback_image_places.swap(o.m_fallback_image_places);
    std::swap(m_wmf_size, o.m_wmf_size);
    std::swap(m_fallback_resolution, o.m_fallback_resolution);
    std::swap(m_page, o.m_page);
    std::swap(m_pageBreaks, o.m_pageBreaks);
    std::swap(m_pedantic, o.m_pedantic);
    std::swap(m_copyright, o.m_copyright);
    std::swap(m_callback, o.m_callback);
    std::swap(m_callback_data, o.m_callback_data);
    std::swap(m_load_data, o.m_load_data);
}

std::string CDrawingChartData::GetAllCollapsedGUIState() const
{
    GraphChart *g = dynamic_cast<GraphChart *>(GetChart());
    return g ? g->GetAllCollapsedGUIState().Serialize() : std::string();
}

std::string CDrawingChartData::GetAllExpandedGUIState() const
{
    GraphChart *g = dynamic_cast<GraphChart *>(GetChart());
    return g ? g->GetAllExpandedGUIState().Serialize() : std::string();
}

void CDrawingChartData::ClearCache() const
{
    if (m_cache_EMF) {
        DeleteEnhMetaFile(m_cache_EMF);
        m_cache_EMF = nullptr;
    }
    if (m_cache_rec) {
        cairo_surface_destroy(m_cache_rec);
        m_cache_rec = nullptr;
    }
    if (m_cache_rec_full_no_pb) {
        cairo_surface_destroy(m_cache_rec_full_no_pb);
        m_cache_rec_full_no_pb = nullptr;
    }
    m_wmf_size = 0;
    m_fallback_image_places.clear();
    compiled = false;
}

void CDrawingChartData::SetDesign (const char *design)
{
	if (!design) return;
	if (GetDesign() == design) return;
	Invalidate();
	m_ForcedDesign = design;
}

void CDrawingChartData::SetLayout(const char * layout)
{
    if (!layout) return;
    if (GetDesign() == layout) return;
    Invalidate();
    m_graphLayout = layout;
}

 void CDrawingChartData::SetPageSize(const XY &s)
{
    XY use = (s.y<0 || s.x<0) ? XY(0,0) : s;
	if (GetPageSize() == s) return;
	Invalidate();
	m_page_size = s;
}

void CDrawingChartData::SetAddHeading(bool b)
{
	if (GetAddHeading() == b) return;
	Invalidate();
	m_addHeading = b;
}

void CDrawingChartData::SetFitWidth(bool b)
{
	if (GetFitWidth() == b) return;
	Invalidate();
	m_fitWidth = b;
}

void CDrawingChartData::SetPedantic(bool b)
{
    if (m_pedantic == b) return;
	Invalidate();
    m_pedantic = b;
}

std::string CDrawingChartData::ControlClicked(const ElementRef& element, EGUIControlType t) const
{
    Chart* chart_to_use = element.inline_data ? element.inline_data->chart : GetChart();
    if (chart_to_use->ControlClicked(element.element, t)) {
        chart_to_use->GUIStateChanged(); //this recalculates the GUI changes in all child inlined charts
        //OK, something changed. Take new GUI state...
        return GetChart()->SerializeGUIState();
    }
    return {};
}

void CDrawingChartData::SetCopyRightText(const char *c)
{
	if (m_copyright== c) return;
	Invalidate();
	m_copyright = c;
}


void CDrawingChartData::InvalidatePage() const
{
    if (m_cacheType==CACHE_EMF) {
        ClearCache();
    } else {
        if (m_cache_rec) {
            cairo_surface_destroy(m_cache_rec);
            m_cache_rec = nullptr;
        }
    }
    //If we have a compiled chart, regenerate the page
    if (compiled) CompileIfNeeded();
}

void CDrawingChartData::SetPage(unsigned page)
{
    const bool was_compiled = compiled;
    if (compiled) {
        if (GetPages()==1) page = 0;
        else page = std::min(page, GetPages());
    }
    if (page!=m_page) {
        //if we show page breaks and switch between individual page view and
        //the view of the entire chart clear all the cache, as we need to redraw
        //the whole chart
        const bool clear_cache = ((page==0) != (m_page==0)) && m_pageBreaks;
        //below instructions need the new page value set in m_page
        m_page = page;
        if (clear_cache)
            ClearCache(); //this clears the 'compiled' flag
        compiled = was_compiled;
        InvalidatePage();
    }
}

void CDrawingChartData::SetPageBreaks(bool pageBreaks)
{
    if (pageBreaks == m_pageBreaks) return;
    m_pageBreaks = pageBreaks;
    if (m_page==0) InvalidatePage();
}

void CDrawingChartData::SetFallbackResolution(double d)
{
    if (m_fallback_resolution == d) return ;
    m_fallback_resolution = d;
    if (m_cacheType == CACHE_EMF) InvalidatePage();
}



void CDrawingChartData::Invalidate() const
{
    if (m_chart)
        m_chart = nullptr; //free memory of old chart now
    compiled = false;
    ClearCache();
}

bool CDrawingChartData::CompileIfNeeded() const
{
    const bool did_compilation = m_chart==nullptr;
    auto pConf = GetConf();
    if (!pConf) return false;
    const std::chrono::steady_clock::time_point c = std::chrono::steady_clock::now();
    if (!m_chart) {
        m_chart = pConf->m_lang->create_chart(CMscGenConf::StaticReadIncludeFile, (void*)pConf);
        m_chart->prepare_for_tracking = true;
        m_chart->prepare_element_controls = pConf->m_bShowControls;
        if (m_callback) {
            m_chart->GetProgress()->callback = m_callback;
            m_chart->GetProgress()->data = m_callback_data;
            if (m_load_data.GetLength())
                m_chart->GetProgress()->ReadLoadData(std::string_view(m_load_data, m_load_data.GetLength()));
        }
        if (HasVersion() && CompareVersion(LIBMSCGEN_MAJOR, LIBMSCGEN_MINOR, LIBMSCGEN_SUPERMINOR)<0) {
            string msg = "This chart was created with Msc-Generator v";
            msg << ver_a << "." << ver_b;
            if (ver_c) msg << "." << ver_c;
            msg << ", which is newer than the current version. Please update Msc-generator, see Help|About...";
            m_chart->Error.Warning(FileLineCol(0, 0, 0), msg);
        }
        //add shapes
        m_chart->Shapes = pConf->m_lang->shapes;
        //add errors
        m_chart->Error = pConf->m_lang->designlib_errors;
        //add designs
        m_chart->ImportDesigns(pConf->m_lang->designs);
        if (!m_ForcedDesign.IsEmpty())
            m_chart->ApplyForcedDesign((const char*)m_ForcedDesign);
        //set copyright text
        m_chart->copyrightText = m_copyright;

        //MscChart specific settings
        if (MscChart* msc = dynamic_cast<MscChart*>(m_chart.get())) {
            //copy pedantic flag from app settings
            msc->mscgen_compat = pConf->m_mscgen_compat;
            msc->Error.warn_mscgen = pConf->m_bWarnMscgenCompat;
        }
        //Graph specific settings
        if (GraphChart* graph = dynamic_cast<GraphChart*>(m_chart.get())) {
            graph->do_orig_lang = pConf->m_bGraphUsesOriginalEngine;
            //set layout after setting the default style - this overrides that
            graph->ApplyForcedLayout((const char*)m_graphLayout);
        }
        m_chart->SetPedantic(m_pedantic);
        //copy forced collapse/expand entities/boxes/graphs, etc.
        m_chart->DeserializeGUIState(m_GUIState.c_str());
        //parse chart text
        m_chart->GetProgress()->StartParse();
#ifndef DEBUG
        //Protect the compiletion agains some cases of segfaults
        //Not in debug mode there we shall have the full wrath of King Error
        try {
#endif // DEBUG
            m_main_file_no =
                m_chart->ParseText(std::string_view(m_text, m_text.GetLength()),
                                   std::string_view(m_FileName, m_FileName.GetLength()));
#ifndef DEBUG
    }
        catch (...) {
            m_chart->SetToEmpty();
            m_chart->Error.FatalError(FileLineCol(), "Internal error: Input parser crashed.",
                "Try altering your chart. "
                "Consider submitting this as a bug at https://gitlab.com/msc-generator/msc-generator/-/issues");
        }
#endif // DEBUG
        if (!m_chart->Error.hasFatal()) {
#ifndef DEBUG
            try {
#endif // DEBUG
                //Do postparse, compile, calculate sizes and sort errors by line
                m_chart->CompleteParse(m_page_size.x>0 && m_page_size.y>0,
                                       m_addHeading, m_page_size, m_fitWidth, true);
#ifndef DEBUG
            } catch (...) { //Dont catch exceptions if we debug
                m_chart->SetToEmpty();
                m_chart->Error.FatalError(FileLineCol(), "Internal error: Layouting has crashed.",
                    "Try altering your chart. "
                    "Consider submitting this as a bug at https://gitlab.com/msc-generator/msc-generator/-/issues");
            }
#endif // DEBUG
        }
        const_cast<std::string&>(m_GUIState) = m_chart->SerializeGUIState();
        ClearCache();
        if (m_page > m_chart->GetPageNum())
            const_cast<unsigned&>(m_page) = m_chart->GetPageNum();
    }
    if (!pConf->m_FullPath)
        m_chart->Error.RemovePathFromErrorMessages();
    bool did_redraw = did_compilation;
    //Now regenerate the cache
    if (m_cacheType==CACHE_EMF) {
        if (m_cache_EMF==nullptr) {
            m_cache_EMF = m_chart->DrawToMetaFile(Canvas::EMFWMF, m_page, m_pageBreaks,
                m_fallback_resolution, &m_wmf_size, &m_fallback_image_places);
            did_redraw = true;
        }
    } else if (!m_cache_rec) {
        if (!m_cache_rec_full_no_pb)
            m_cache_rec_full_no_pb = m_chart->DrawToRecordingSurface(Canvas::WIN, m_pageBreaks && m_page==0);
        m_cache_rec = m_chart->ReDrawOnePage(m_cache_rec_full_no_pb, m_page);
        did_redraw = true;
    }
    if (did_compilation && pConf->m_bTechnicalInfo) {
        const double sec = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now()-c).count()/1e9;
        m_chart->Error.TechnicalInfo({}, StrCat("Compilation time was: ", std::to_string(sec)));
        const auto res = m_chart->GetProgress()->GetUnifiedSectionPercentage();
        //If we did not end on 100%, scale up, so that printed results sum up to 100%
        const double ratio = 100./m_chart->GetProgress()->GetPercentage();
        for (unsigned u = 0; u<res.size(); u++)
            if (int(round(res[u]*ratio)))
                m_chart->Error.TechnicalInfo({}, StrCat(m_chart->GetProgress()->GetUnifiedSectionName(u), ":",
                                                        std::to_string(int(round(res[u]*ratio))), "% ",
                                                        std::to_string(int(round(res[u]*ratio*10*sec))), "ms"));
    }
    compiled = true;
    if (did_compilation && m_callback && m_text.GetLength()>0) {
        m_chart->GetProgress()->Done();
        m_load_data = m_chart->GetProgress()->WriteLoadData().c_str();
    }
    return did_redraw;
}

unsigned CDrawingChartData::GetErrorNum(bool oWarnings, bool oTechnicalInfo) const {
	return GetChart()->Error.GetErrorNum(oWarnings, oTechnicalInfo);
}

bool CDrawingChartData::IsErrorInFile(unsigned num, bool oWarnings, bool oTechnicalInfo) const
{
    return GetErrorNum(oWarnings, oTechnicalInfo)>num && num>=0 &&
        IsErrorInFile(GetChart()->Error.GetErrorLoc(num, oWarnings, oTechnicalInfo));
}

bool CDrawingChartData::IsErrorInFile(const FileLineCol &l) const
{
    return compiled && l.file == m_main_file_no;
}

FileLineCol CDrawingChartData::GetErrorFileLineCol(unsigned num, bool oWarnings, bool oTechnicalInfo) const
{
    if (GetErrorNum(oWarnings, oTechnicalInfo)>num && num>=0 )
        return GetChart()->Error.GetErrorLoc(num, oWarnings, oTechnicalInfo);
    else
        return {};
}

CStringA CDrawingChartData::GetErrorText(unsigned num, bool oWarnings, bool oTechnicalInfo) const
{
    if (GetErrorNum(oWarnings, oTechnicalInfo)<=num)
        return "";
    auto v = GetChart()->Error.GetErrorText(num, oWarnings, oTechnicalInfo);
    return CStringA(v.data(), int(v.length()));
}

CStringA CDrawingChartData::GetErrorMessage(unsigned num, bool oWarnings, bool oTechnicalInfo) const
{
    if (GetErrorNum(oWarnings, oTechnicalInfo)<=num)
        return "";
    auto v = GetChart()->Error.GetErrorMessage(num, oWarnings, oTechnicalInfo);
    return CStringA(v.data(), int(v.length()));
}

CStringA CDrawingChartData::GetDesignNames() const
{
	return CStringA(GetChart()->GetDesignNamesAsString(true).c_str());
}


unsigned CDrawingChartData::GetPages() const
{
    return GetChart()->GetPageNum();
}

//if force_page==false, return the size of m_page (or the entire chart if m_page==0)
//else that of forced_page

//In CDrawingChartData we hide that the Msc chart does not originate in (0,0) by
//shifting all coordinates.
//The returned page here contains autoHeaders and copyRightText
XY CDrawingChartData::GetCanvasSize(unsigned page) const
{
    return GetChart()->GetCanvasSize(page);
}

const Block &CDrawingChartData::GetTotal() const
{
    return GetChart()->GetTotal();
}

double CDrawingChartData::GetMscWidthAttr() const
{
    const MscChart *msc = dynamic_cast<MscChart*>(GetChart());
    if (msc==nullptr) return -1;
    return msc->GetWidthAttr();
}

bool CDrawingChartData::GetMscgenCompat() const
{
    const MscChart *msc = dynamic_cast<MscChart*>(GetChart());
    if (msc==nullptr) return false;
    return msc->mscgen_compat == EMscgenCompat::FORCE_MSCGEN;
}


XY CDrawingChartData::GetPageOrigin(unsigned page) const
{
    if (page<=1)
        return GetChart()->GetTotal().UpperLeft();
    return GetChart()->GetPageData(page-1)->xy - GetChart()->GetPageData(page-1)->headingLeftingSize;
}

Block CDrawingChartData::GetNetPageBlock() const
{
    if (m_page > GetChart()->GetPageNum())
        return Block(false);
    if (m_page==0)
        return m_chart->GetTotal();
    auto *pd = m_chart->GetPageData(m_page-1);
    return Block(pd->xy, pd->xy+pd->wh);
}

double CDrawingChartData::GetHeadingSize() const
{
    //This is MscChart specific
    MscChart *msc = dynamic_cast<MscChart*>(GetChart());
    if (msc==nullptr) return 0;
    if (m_page<=1 || m_page > msc->GetPageNum())
        return msc->GetHeadingSize() - msc->GetTotal().x.from;
    return msc->GetPageData(m_page-1)->headingLeftingSize.y;
}

CStringA
CDrawingChartData::DrawToFile(const CString &fileName, bool bPageBreaks, bool ignore_pagebreaks,
                              bool include_chart_text, unsigned only_page,
                              XY scale, const XY &pageSize,
                              const double margins[4], int ha, int va) const
{
    std::string fn;
    if (fileName.GetLength())
        //here we hope that Windows fopen accepts UTF-8, which it probably does not
        fn = AsUTF8(fileName);
    else
        fn = "Untitled";
    size_t pos = fn.find_last_of('.');
    if (pos==string::npos) {
        pos = fn.length();
        fn += ".png";
    }
    CStringA ext = fn.substr(pos+1).c_str();
    Canvas::EOutputType ot;
    if (ext.CompareNoCase("png")==0) ot = Canvas::PNG;
    else if (ext.CompareNoCase("png")==0) ot = Canvas::PNG;
    else if (ext.CompareNoCase("emf")==0) ot = Canvas::EMF;
    else if (ext.CompareNoCase("svg")==0) ot = Canvas::SVG;
    else if (ext.CompareNoCase("pdf")==0) ot = Canvas::PDF;
    else if (ext.CompareNoCase("eps")==0) ot = Canvas::EPS;
    else if (ext.CompareNoCase("wmf")==0) ot = Canvas::WMF;
    else if (ext.CompareNoCase("pptx")==0) ot = Canvas::PPT;
    else {
        ot = Canvas::PNG;
        fn += ".png";
    }
    const unsigned err = GetErrorNum(false, false);
    if (GetChart()->DrawToFile(ot, scale, fn, bPageBreaks, ignore_pagebreaks,
                               include_chart_text ? (const char*)m_text : nullptr,
                               only_page, pageSize, margins, ha, va, true))
        return {};
    if (err==GetErrorNum(false, false))
        return "Error during exporting.";
    auto ret = GetErrorMessage(err, false, false);
    while (err < GetErrorNum(false, false))
        GetChart()->Error.DeleteError(err, false, false);
    return ret;
}

//This one expects point in chart coordinates
ElementRef CDrawingChartData::GetArc(CPoint point) const
{
	_ASSERT(m_chart);
    //Check if point is on currently showing page)
    if (!GetNetPageBlock().IsWithinBool(XY(point.x, point.y))) return {};
    return m_chart->FindElement(XY(point.x, point.y));
}

ElementRef CDrawingChartData::GetArc(unsigned line, unsigned col) const
{
	_ASSERT(m_chart);
	FileLineCol linenum(int(m_chart->Error.Files.size()-1), line, col);
    return m_chart->FindElement(linenum);
}

const ISMapElement *CDrawingChartData::GetLinkByCoordinate(CPoint point) const
{
    _ASSERT(m_chart);
    //Check if point is on currently showing page)
    const XY p(point.x, point.y);
    if (!GetNetPageBlock().IsWithinBool(p))
        return nullptr;
    for (const auto &e : m_chart->ismapData)
        if (e.rect.IsWithinBool(p))
            return &e;
    return nullptr;
}


//Here the "memDC" contains a bitmap as large as the visible part of the screen
//"scale" contains the magnification factor we shall display the chart with
//"clip" contains the coordinates of the visible part of the screen in scaled chart coordinates.
//E.g., if the chart is 100x100, scale is 2, and we can see 80x80 pixels in the middle of the
//chart (a small window), then the scaled chart is 200x200 and clip will be (60x60)->(140x140)
void CDrawingChartData::DrawToMemDC(CDC &memDC, double x_scale, double y_scale, const CRect &clip)
{
    _ASSERT(m_chart);
    if (!m_chart) return;
    switch (m_cacheType) {
    case CACHE_EMF: {
        if (!m_cache_EMF) break;
        const CSize size = GetSize();
        const CRect full(0,0, int(size.cx*x_scale), int(size.cy*y_scale));
        memDC.SaveDC();
        memDC.SetWindowOrg(clip.left, clip.top);
        memDC.PlayMetaFile(m_cache_EMF, &full);
        memDC.RestoreDC(1);
        break;
        }
    case CACHE_RECORDING: {
        if (!m_cache_rec) break;
        cairo_surface_t *surf = cairo_win32_surface_create(memDC.m_hDC);
        cairo_t *cr = cairo_create(surf);
        cairo_scale(cr, x_scale, y_scale);
        cairo_set_source_surface(cr, m_cache_rec, -clip.left/x_scale, -clip.top/y_scale);
        cairo_paint (cr);
        cairo_destroy (cr);
        cairo_surface_destroy(surf);
        break;
        }
    }
}

