/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/


/** @defgroup Msc-generator The Windows GUI of Msc-generator.
This part of the code compiles only on Windows and implements the GUI.
It is an OLE server object, providing an "Msc-generator siganlling chart" object
to other applications.
It is written using the Microsoft Foundation Classes (MFC) library using the
Document/View architecture. */

/** @defgroup Msc_generator_files The files comprising of the Msc-generator GUI.
@ingroup Msc-generator */

/** @file Msc-generator.cpp The implementation of CAboutDialog and MscGenApp classes.
 * @ingroup Msc_generator_files */

// Msc-generator.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Msc-generator.h"
#include "MainFrm.h"

#include "MscGenDoc.h"
#include "MscGenView.h"
#include "CExampleDialog.h"
#include "utf8utils.h"
#include "commandline.h" //for VersionText()

#include <sstream>
#include <vector>
#include <list>
#include <memory>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CMscGenConf *GetConf()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    return &pApp->m_config;
};

CEditorBar *GetInternalEditor()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    if (!pApp || !pApp->m_pWndEditor || !IsWindow(pApp->m_pWndEditor->m_hWnd)) return nullptr;
    return pApp->m_pWndEditor;
}

bool IsInternalEditorVisible()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    if (!pApp || !pApp->m_pWndEditor || !IsWindow(pApp->m_pWndEditor->m_hWnd)) return false;
    return pApp->m_pWndEditor->IsVisible();
}

int GetRegistryInt(LPCWSTR key, int def)
{
    return theApp.GetProfileInt(REG_SECTION_SETTINGS, key, def);
}

CStringW GetRegistryString(LPCWSTR key, LPCWSTR def)
{
    return theApp.GetProfileStringW(REG_SECTION_SETTINGS, key, def);
}



/** The About.. dialog box. */
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };
	CMFCLinkCtrl m_btnLink;        ///< The button holding the link to the GitLab page
	CMFCLinkCtrl m_btnLink_Latest; ///< The button holding the link to the latest version.

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    ///< DDX/DDV support
	virtual BOOL OnInitDialog( );

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_URL, m_btnLink);
    DDX_Control(pDX, IDC_BUTTON_LATEST_URL, m_btnLink_Latest);
}

BOOL CAboutDlg::OnInitDialog( )
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    BOOL a = CDialog::OnInitDialog();
	m_btnLink.SetURL(_T("https://gitlab.com/msc-generator/msc-generator/"));
	m_btnLink.SetTooltip(_T("Visit the Msc-generator site"));
	m_btnLink.SizeToContent();
	CString text = _T("Msc-generator Version ");
	GetDlgItem(IDC_STATIC_VERSION)->SetWindowText(text + (VersionText().substr(1).c_str()));
    text = _T("(using ");
    text += pApp->m_config.m_languages.GetLibraryNames().c_str();
    text += _T(")");
	GetDlgItem(IDC_STATIC_CAIRO_VERSION)->SetWindowText(text);

	int no_a = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_MAJOR, -1);
	int no_b = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_MINOR, -1);
	int no_c = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_SUPER_MINOR, -1);

    m_btnLink_Latest.SetWindowText(_T(""));
    m_btnLink_Latest.SetTooltip(_T("Msc-generator is up-to-date"));
    m_btnLink_Latest.SizeToContent();
    m_btnLink_Latest.EnableWindow(FALSE);

    const CString latest_url = pApp->GetProfileString(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_URL);
    if (no_a<=0 || no_b<0 || no_c<0 || latest_url.GetLength()==0) return a;

    if (no_a<=LIBMSCGEN_MAJOR) return a;
    if (no_a==LIBMSCGEN_MAJOR && no_b<LIBMSCGEN_MINOR) return a;
    if (no_a==LIBMSCGEN_MAJOR && no_b==LIBMSCGEN_MINOR && no_c<=LIBMSCGEN_SUPERMINOR) return a;

    m_btnLink_Latest.SetURL(latest_url);
    m_btnLink_Latest.SetWindowText(CString("Download latest version (") + VersionText(no_a, no_b, no_c).c_str() + CString(")"));
    m_btnLink_Latest.SetTooltip(latest_url);
    m_btnLink_Latest.SizeToContent();
    m_btnLink_Latest.EnableWindow();
    return a;
}


// CExternalEditorOptionDlg dialog

/** The dialog class for specifying External Text editor params.*/
class CExternalEditorOptionDlg : public CDialog
{
	DECLARE_DYNCREATE(CExternalEditorOptionDlg)

public:
	CExternalEditorOptionDlg(CWnd* pParent = nullptr);   ///< standard constructor
	virtual ~CExternalEditorOptionDlg();
// Overrides

// Dialog Data
	enum { IDD = IDD_DIALOG_OPTIONS};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_TextEditStartCommand;        ///<The command to execute to start the external editor
	CString m_TextEditorJumpToLineCommand; ///<The command to execute to make the external editor jump to a line.
};


IMPLEMENT_DYNCREATE(CExternalEditorOptionDlg, CDialog)

CExternalEditorOptionDlg::CExternalEditorOptionDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(CExternalEditorOptionDlg::IDD, pParent)
{
}

CExternalEditorOptionDlg::~CExternalEditorOptionDlg()
{
}

void CExternalEditorOptionDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_EDIT1, m_TextEditStartCommand);
    DDX_Text(pDX, IDC_EDIT2, m_TextEditorJumpToLineCommand);
}



BEGIN_MESSAGE_MAP(CExternalEditorOptionDlg, CDialog)
END_MESSAGE_MAP()

/** The language selector dialog box. */
class CLanguageSelectorDlg : public CDialog
{
public:
    CLanguageSelectorDlg() : CDialog(CLanguageSelectorDlg::IDD) {}

    // Dialog Data
    enum { IDD = IDD_DIALOG_LANGUAGE };

protected:
    //virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();
    afx_msg void OnSelChanged(); ///<Called when the selected language changes
    afx_msg void OnDblClick();   ///<Called when the user double clicks on one language
    afx_msg void OnAskClicked(); ///<Called if the 'Ask...' checkbox is clicked.

    // Implementation
protected:
    DECLARE_MESSAGE_MAP()
public:
    CString lang; ///<The language description selected.
};

BEGIN_MESSAGE_MAP(CLanguageSelectorDlg, CDialog)
    ON_LBN_SELCHANGE(IDC_LIST_LANGUAGE, &CLanguageSelectorDlg::OnSelChanged)
    ON_LBN_DBLCLK(IDC_LIST_LANGUAGE, &CLanguageSelectorDlg::OnDblClick)
    ON_BN_CLICKED(IDC_CHECK_ALWAYS_ASK_LANGUAGE, &CLanguageSelectorDlg::OnAskClicked)
END_MESSAGE_MAP()


BOOL CLanguageSelectorDlg::OnInitDialog()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    auto ls = pApp->m_config.m_languages.GetLanguages();
    auto *lb = ((CListBox*)GetDlgItem(IDC_LIST_LANGUAGE));
    for (auto &s: ls)
        lb->AddString(AsUnicode(pApp->m_config.m_languages.GetLanguage(s)->description));
    BOOL a = CDialog::OnInitDialog();
    CString wants = pApp->GetProfileString(REG_SECTION_SETTINGS, REG_KEY_TYPICAL_NEW_CHART, _T(""));
    lb->SetSel(0);
    for (int u = lb->GetCount()-1; u>=0; u--) {
        lb->GetText(u, lang);
        if (lang != wants) continue;
        lb->SetCurSel(u);
        break;
    }
    lb->SetFocus();
    auto *cb = ((CButton*)GetDlgItem(IDC_CHECK_ALWAYS_ASK_LANGUAGE));
    cb->SetCheck(pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, TRUE));
    //make the window appear on top of all other windows
    //https://stackoverflow.com/questions/593403/always-in-front-dialogs
    SetWindowPos(&this->wndTop, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
    return FALSE;
}

void CLanguageSelectorDlg::OnSelChanged()
{
    ((CListBox*)GetDlgItem(IDC_LIST_LANGUAGE))->GetText(
        ((CListBox*)GetDlgItem(IDC_LIST_LANGUAGE))->GetCurSel(),
        lang);
}

void CLanguageSelectorDlg::OnDblClick()
{
    OnSelChanged();
    OnOK();
}

void CLanguageSelectorDlg::OnAskClicked()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    auto *cb = ((CButton*)GetDlgItem(IDC_CHECK_ALWAYS_ASK_LANGUAGE));
    pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, cb->GetCheck());
}


BOOL CMscGenDataRecoveryHandler::Initialize()
{
    BOOL ret = CDataRecoveryHandler::Initialize();
    if (ret) {
        m_strRestartIdentifier = _T("RestartData");
    }
    return ret;
}

/** Returns the document name. Needed to be overridden to append default extension.
 * The CDataRecoveryHandler uses the document name as the key in
 * m_mapDocNameToAutosaveName, m_mapDocNameToDocumentPtr, and
 * m_mapDocNameToRestoreBool. These parameter enable the CDataRecoveryHandler
 * to monitor CDocument objects, the autosave file name, and the autosave
 * settings.*/
CString CMscGenDataRecoveryHandler::GetDocumentListName(_In_ CDocument *pDocument) const
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    CString strDocumentPathName = pDocument->GetPathName();
    if (strDocumentPathName.IsEmpty())
        strDocumentPathName = pDocument->GetTitle();
    //if we end in an extension supported by us, but not the correct one, replace.
    PTSTR extension = PathFindExtension(strDocumentPathName);
    if (*extension) {
        //if we actually have an extension...
        const std::string lang4ext = pApp->m_config.m_languages.LanguageForExtension((std::string)(const char*)AsUTF8(extension));
        //if the extension currently on the file does match any of the valid extensions for the current language
        //we keep the name as is
        if (lang4ext == pApp->m_config.m_lang->GetName())
            return strDocumentPathName;
        //else remove the extension (and the dot, too)
        strDocumentPathName = strDocumentPathName.Left(extension-strDocumentPathName);
    }
    // append the current extension to use.
    strDocumentPathName += _T(".");
    strDocumentPathName += AsUnicode(pApp->m_config.m_lang->GetName());
    return strDocumentPathName;
}


// CMscGenApp

BEGIN_MESSAGE_MAP(CMscGenApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CMscGenApp::OnAppAbout)
    ON_COMMAND(ID_HELP, &CMscGenApp::OnHelp)
    ON_COMMAND(ID_HELP_EXAMPLES, &CMscGenApp::OnExamples)
    // Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CMscGenApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CMscGenApp::OnFileOpen)
    // Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CMscGenApp::OnFilePrintSetup)
    ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_SETUP, &CMscGenApp::OnUpdatePrintSetup)
	ON_COMMAND(ID_EDIT_PREFERENCES, OnEditPreferences)
    ON_COMMAND(ID_BUTTON_DEFAULT_TEXT, &CMscGenApp::OnButtonDefaultText)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_DEFAULT_TEXT, &CMscGenApp::OnUpdateButtonDefaultText)
    ON_COMMAND(IDC_CHECK_PEDANTIC, &CMscGenApp::OnCheckPedantic)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_PEDANTIC, &CMscGenApp::OnUpdateCheckPedantic)
    ON_COMMAND(IDC_CHECK_WARNINGS, &CMscGenApp::OnCheckWarnings)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_WARNINGS, &CMscGenApp::OnUpdateCheckWarnings)
    ON_COMMAND(ID_CHECK_FULL_PATH, &CMscGenApp::OnCheckFullPath)
    ON_UPDATE_COMMAND_UI(ID_CHECK_FULL_PATH, &CMscGenApp::OnUpdateCheckFullPath)
    ON_COMMAND(IDC_CHECK_PAGEBREAKS, &CMscGenApp::OnCheckPageBreaks)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_PAGEBREAKS, &CMscGenApp::OnUpdateCheckPageBreaks)
    ON_COMMAND(IDC_COMBO_CSH, &CMscGenApp::OnComboCsh)
    ON_UPDATE_COMMAND_UI(IDC_COMBO_CSH, &CMscGenApp::OnUpdateComboCsh)
    ON_COMMAND(IDC_CHECK_SMART_IDENT, &CMscGenApp::OnCheckSmartIndent)
    ON_COMMAND(ID_CHECK_CSH_TAB, &CMscGenApp::OnCheckTABIndents)
    ON_COMMAND(IDC_CHECK_CSH_ERROR, &CMscGenApp::OnCheckCshError)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_IDENT, &CMscGenApp::OnUpdateCheckSmartIndent)
    ON_UPDATE_COMMAND_UI(ID_CHECK_CSH_TAB, &CMscGenApp::OnUpdateCheckTABIndents)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_CSH_ERROR, &CMscGenApp::OnUpdateCheckCshError)
    ON_COMMAND(IDC_CHECK_CSH_ERROR_IN_WINDOW, &CMscGenApp::OnCheckCshErrorInWindow)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_CSH_ERROR_IN_WINDOW, &CMscGenApp::OnUpdateCheckCshErrorInWindow)
    ON_UPDATE_COMMAND_UI(ID_EDIT_PREFERENCES, &CMscGenApp::OnUpdateEditPreferences)
    ON_COMMAND(ID_CHECK_EE_OTHER, &CMscGenApp::OnCheckEeOther)
    ON_COMMAND(ID_CHECK_EE_NOTEPAD, &CMscGenApp::OnCheckEeNotepad)
    ON_COMMAND(ID_CHECK_EE_NOTEPADPP, &CMscGenApp::OnCheckEeNotepadpp)
    ON_UPDATE_COMMAND_UI(ID_CHECK_EE_OTHER, &CMscGenApp::OnUpdateCheckEeOther)
    ON_UPDATE_COMMAND_UI(ID_CHECK_EE_NOTEPAD, &CMscGenApp::OnUpdateCheckEeNotepad)
    ON_UPDATE_COMMAND_UI(ID_CHECK_EE_NOTEPADPP, &CMscGenApp::OnUpdateCheckEeNotepadpp)
    ON_COMMAND(IDC_CHECK_HINTS, &CMscGenApp::OnCheckHints)
    ON_COMMAND(IDC_CHECK_SMART_HINT_COMPACT, &CMscGenApp::OnCheckSmartHintCompact)
    ON_COMMAND(IDC_COMBO_HINT_FILTER, &CMscGenApp::OnComboSmartHintFilter)
    ON_COMMAND(IDC_CHECK_SMART_HINT_LINE_START, &CMscGenApp::OnCheckSmartHintLineStart)
    ON_COMMAND(IDC_CHECK_SMART_HINT_ENTITY, &CMscGenApp::OnCheckSmartHintEntity)
    ON_COMMAND(IDC_CHECK_SMART_HINT_ESCAPE, &CMscGenApp::OnCheckSmartHintEscape)
    ON_COMMAND(IDC_CHECK_SMART_HINT_ATTR_NAME, &CMscGenApp::OnCheckSmartHintAttrName)
    ON_COMMAND(IDC_CHECK_SMART_HINT_ATTR_VALUE, &CMscGenApp::OnCheckSmartHintAttrValue)
    ON_COMMAND(IDC_CHECK_SMART_HINT_KEYWORD_MARKER, &CMscGenApp::OnCheckSmartHintKeywordMarker)
    ON_UPDATE_COMMAND_UI(IDC_COMBO_HINT_FILTER, &CMscGenApp::OnUpdateComboSmartHintFilter)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_COMPACT, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_LINE_START, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_ENTITY, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_ESCAPE, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_ATTR_NAME, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_ATTR_VALUE, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_SMART_HINT_KEYWORD_MARKER, &CMscGenApp::OnUpdateCheckSmartHintBoxes)
    ON_UPDATE_COMMAND_UI(IDC_CHECK_HINTS, &CMscGenApp::OnUpdateCheckHints)
    ON_COMMAND(ID_BUTTON_TRACK_COLOR, &CMscGenApp::OnButtonTrackColor)
    ON_COMMAND(ID_EMBEDDEDOPTIONS_FALLBACK_RES, &CMscGenApp::OnEmbeddedoptionsFallbackRes)
    ON_COMMAND(ID_AUTO_PAGINATE, &CMscGenApp::OnAutoPaginate)
    ON_UPDATE_COMMAND_UI(ID_AUTO_PAGINATE, &CMscGenApp::OnUpdateAutoPaginate)
    ON_COMMAND(ID_BUTTON_ORIGINAL_ENGINE, &CMscGenApp::OnOriginalEngine)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_ORIGINAL_ENGINE, &CMscGenApp::OnUpdateOriginalEngine)
    ON_COMMAND(ID_AUTO_HEADERS, &CMscGenApp::OnAutoHeaders)
    ON_UPDATE_COMMAND_UI(ID_AUTO_HEADERS, &CMscGenApp::OnUpdateAutoHeaders)
    ON_COMMAND(ID_COMBO_SCALE, &CMscGenApp::OnComboScale)
    ON_COMMAND(ID_BUTTON_PAGES, OnButtonPages)
    ON_COMMAND(ID_COMBO_ALIGNMENT, &CMscGenApp::OnComboAlignment)
    ON_COMMAND(ID_EDIT_MARGIN_L, &CMscGenApp::OnEditMarginL)
    ON_COMMAND(ID_EDIT_MARGIN_R, &CMscGenApp::OnEditMarginR)
    ON_COMMAND(ID_EDIT_MARGIN_T, &CMscGenApp::OnEditMarginT)
    ON_COMMAND(ID_EDIT_MARGIN_B, &CMscGenApp::OnEditMarginB)
    ON_UPDATE_COMMAND_UI(ID_COMBO_SCALE, &CMscGenApp::OnUpdatePrintPreviewEdits)
    ON_UPDATE_COMMAND_UI(ID_EDIT_MARGIN_L, &CMscGenApp::OnUpdatePrintPreviewEdits)
    ON_UPDATE_COMMAND_UI(ID_EDIT_MARGIN_R, &CMscGenApp::OnUpdatePrintPreviewEdits)
    ON_UPDATE_COMMAND_UI(ID_EDIT_MARGIN_T, &CMscGenApp::OnUpdatePrintPreviewEdits)
    ON_UPDATE_COMMAND_UI(ID_EDIT_MARGIN_B, &CMscGenApp::OnUpdatePrintPreviewEdits)
    ON_COMMAND(ID_COMBO_MSCGEN_COMPAT, &CMscGenApp::OnComboMscgenCompat)
    ON_UPDATE_COMMAND_UI(ID_COMBO_MSCGEN_COMPAT, &CMscGenApp::OnUpdateComboMscgenCompat)
    ON_COMMAND(ID_CHECK_MSCGEN_COMPAT_WARN, &CMscGenApp::OnCheckMscgenCompatWarn)
    ON_UPDATE_COMMAND_UI(ID_CHECK_MSCGEN_COMPAT_WARN, &CMscGenApp::OnUpdateCheckMscgenCompatWarn)
    ON_COMMAND(ID_CHECK_ASK_LANGUAGE, &CMscGenApp::OnCheckAskLanguage)
    ON_UPDATE_COMMAND_UI(ID_CHECK_ASK_LANGUAGE, &CMscGenApp::OnUpdateCheckAskLanguage)
    ON_COMMAND(ID_CHECK_AUTOSAVE, &CMscGenApp::OnCheckAutosave)
    ON_UPDATE_COMMAND_UI(ID_CHECK_AUTOSAVE, &CMscGenApp::OnUpdateCheckAutosave)
    ON_COMMAND(ID_EDIT_AUTOSAVE_INTERVAL, &CMscGenApp::OnEditAutosaveInterval)
    ON_UPDATE_COMMAND_UI(ID_EDIT_AUTOSAVE_INTERVAL, &CMscGenApp::OnUpdateEditAutosaveInterval)
    ON_COMMAND(ID_CHECK_OPEN_RECOVERY_AT_STARTUP, &CMscGenApp::OnCheckOpenRecoveryAtStartup)
    ON_UPDATE_COMMAND_UI(ID_CHECK_OPEN_RECOVERY_AT_STARTUP, &CMscGenApp::OnUpdateCheckOpenRecoveryAtStartup)
    ON_COMMAND(ID_CHECK_FILTER_AUTOSAVE, &CMscGenApp::OnCheckFilterAutosave)
    ON_UPDATE_COMMAND_UI(ID_CHECK_FILTER_AUTOSAVE, &CMscGenApp::OnUpdateCheckFilterAutosave)
    ON_COMMAND(ID_BUTTON_DELETE_AUTOSAVED_FILES, &CMscGenApp::OnButtonDeleteAutosavedFiles)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_DELETE_AUTOSAVED_FILES, &CMscGenApp::OnUpdateButtonDeleteAutosavedFiles)
    ON_COMMAND(ID_CHECK_RECOVERY_OVERWRITE, &CMscGenApp::OnCheckRecoveryOverwrite)
    ON_UPDATE_COMMAND_UI(ID_CHECK_RECOVERY_OVERWRITE, &CMscGenApp::OnUpdateCheckRecoveryOverwrite)
    ON_COMMAND(ID_CHECK_RECOVERY_INSERT_AT_CURSOR, &CMscGenApp::OnCheckRecoveryInsertAtCursor)
    ON_UPDATE_COMMAND_UI(ID_CHECK_RECOVERY_INSERT_AT_CURSOR, &CMscGenApp::OnUpdateCheckRecoveryInsertAtCursor)
    ON_COMMAND(ID_CHECK_RECOVERY_DELETE, &CMscGenApp::OnCheckRecoveryDelete)
    ON_UPDATE_COMMAND_UI(ID_CHECK_RECOVERY_DELETE, &CMscGenApp::OnUpdateCheckRecoveryDelete)
    ON_COMMAND(ID_CHECK_WARN_COMPILATION_TIME, &CMscGenApp::OnCheckWarnCompilationTime)
    ON_UPDATE_COMMAND_UI(ID_CHECK_WARN_COMPILATION_TIME, &CMscGenApp::OnUpdateCheckWarnCompilationTime)
    ON_COMMAND(ID_CHECK_CURRENT_LINE_HIGHLIGHT, &CMscGenApp::OnCheckCurrentLineHighlight)
    ON_UPDATE_COMMAND_UI(ID_CHECK_CURRENT_LINE_HIGHLIGHT, &CMscGenApp::OnUpdateCheckCurrentLineHighlight)
    ON_COMMAND(ID_CHECK_SHOW_LINE_NUMS, &CMscGenApp::OnCheckShowLineNums)
    ON_UPDATE_COMMAND_UI(ID_CHECK_SHOW_LINE_NUMS, &CMscGenApp::OnUpdateCheckShowLineNums)
    ON_COMMAND(ID_CHECK_SKIP_COPYRIGHT, &CMscGenApp::OnCheckSkipCopyright)
    ON_UPDATE_COMMAND_UI(ID_CHECK_SKIP_COPYRIGHT, &CMscGenApp::OnUpdateCheckSkipCopyright)
    ON_COMMAND(ID_CHECK_AUTO_PASTE, &CMscGenApp::OnCheckAutoPaste)
    ON_UPDATE_COMMAND_UI(ID_CHECK_AUTO_PASTE, &CMscGenApp::OnUpdateCheckAutoPaste)
    ON_COMMAND(ID_CHECK_INSTANT_COMPILATION, &CMscGenApp::OnCheckInstantCompilation)
    ON_UPDATE_COMMAND_UI(ID_CHECK_INSTANT_COMPILATION, &CMscGenApp::OnUpdateCheckInstantCompilation)
    ON_COMMAND(ID_CHECK_AUTO_SAVE, &CMscGenApp::OnCheckAutoSave)
    ON_UPDATE_COMMAND_UI(ID_CHECK_AUTO_SAVE, &CMscGenApp::OnUpdateCheckAutoSave)
END_MESSAGE_MAP()


// CMscGenApp construction

CMscGenDoc *CMscGenApp::GetDoc(void)
{
	//Get pDoc. I have no clue how to do it for in-place active state: then we have no main window.
	//So in that case we skip external editor restart and recompilation
    CMscGenDoc *pDoc = nullptr;
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (pMainWnd!=nullptr && pMainWnd->GetActiveView() != nullptr)
		pDoc = dynamic_cast<CMscGenDoc *>(pMainWnd->GetActiveView()->GetDocument());
    _ASSERT(pDoc);
    return pDoc;
}


/** Default constructor.
 * As per MFC customs, this is not doing real initialization, just
 * sets the main fields to meaningful defaults.*/
CMscGenApp::CMscGenApp()
{
	// replace application ID string below with unique ID string; recommended
	// format for string is CompanyName.ProductName.SubProduct.VersionInformation
    std::wstring version = ConvertFromUTF8_to_UTF16(VersionText());
    CString buff;
    buff.Format(_T("Msc-generator-%s"), version.c_str());
	SetAppID(buff);

	// Place all significant initialization in InitInstance
    m_pWndOutputView = 0;
    m_pWndEditor = 0;
    m_bFullScreenViewMode = false;
    //autosave at intervals and on restart; reload saved docs on restart.
    m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
    m_nAutosaveInterval = 5000; //[ms] = five seconds autosave
#ifdef _MANAGED
    // If the application is built using Common Language Runtime support (/clr):
    //     1) This additional setting is needed for Restart Manager support to work properly.
    //     2) In your project, you must add a reference to System.Windows.Forms in order to build.
    System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

    SetAppID(_T("Zoltan Turanyi.Msc-generator.v5.0"));
}


/** The one and only CMscGenApp object */
CMscGenApp theApp;
/** This identifier was generated to be statistically unique for your app
 * You may change it if you prefer to choose a specific identifier
 * {453AC3C8-F260-4D88-832A-EC957E92FDEC} */
static constexpr CLSID clsid =
{ 0x453AC3C8, 0xF260, 0x4D88, { 0x83, 0x2A, 0xEC, 0x95, 0x7E, 0x92, 0xFD, 0xEC } };


UINT CheckVersionFreshness(LPVOID);

/** A callback to enumerate fonts */
BOOL CALLBACK EnumFamCallBack(LPLOGFONT lplf, LPTEXTMETRIC lpntm, DWORD FontType, LPVOID fontset) {
    if (TRUETYPE_FONTTYPE&FontType) {
        std::set<string> *set = static_cast<std::set<string>*>(fontset);
        if (set)
            set->insert(ConvertFromUTF16_to_UTF8(lplf->lfFaceName));
    }
    return 1;
}

/** CMscGenApp initialization
 * This is doing all the important initialization.
 * - Init the OLE libs, Taskbar, RichEdit, context menu mgr, tooltips, keyboard
 * - Register our OLE object class and document template
 * - Parse command line, act on it
 * - Obtain current printer data (margins, paper size)
 * - Show our window, enable drag and drop and focus the internal editor.
 */
BOOL CMscGenApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

    CWinAppEx::InitInstance();

    //Prevent some MFC bug
    //See http://www.mombu.com/microsoft/mmc/t-activation-context-error-in-mmc-sxs-when-using-windows-vista-610122.html
    //and http://www.prof-uis.com/prof-uis/tech-support/support-forum/activateactctx-and-dactivateactctx-65396.aspx
    //afxAmbientActCtx = FALSE;


	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

    AfxEnableControlContainer();

    EnableTaskbarInteraction(TRUE);

	// AfxInitRichEdit2() is required to use RichEdit control
	AfxInitRichEdit2();

    // Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	SetRegistryKey(_T("Zoltan Turanyi"));
	LoadStdProfileSettings(9);  // Load standard INI file options (including MRU)

	InitContextMenuManager();

	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CMscGenDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CMscGenView));
	if (!pDocTemplate)
		return FALSE;
	pDocTemplate->SetServerInfo(IDR_MAINFRAME); //No other parameters, we do not support in-place editing. Parameter is the resource set for
	AddDocTemplate(pDocTemplate);
	// Connect the COleTemplateServer to the document template
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template
	m_server.ConnectTemplate(clsid, pDocTemplate, TRUE);
		// Note: SDI applications register server objects only if /Embedding
		//   or /Automation is present on the command line

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

    //Check if we are a new install and reset a few values then

    if (AsUTF8(GetRegistryString(REG_KEY_LAST_VERSION_STARTED, _T("v0.0.0")))!=VersionText().c_str()) {
        WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_STARTED, AsUnicode(VersionText()));
        WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SKIP_COPYRIGHT, 0);
        WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, 1);
    }

    //Init our config
    auto msg = m_config.Init();
    if (msg.GetLength())
        MessageBox(0, msg, _T("Msc-generator errors"), MB_OK);


	//Start process that checks if we are the latest version
	/*CWinThread* check_thread =*/ AfxBeginThread(&CheckVersionFreshness, nullptr);

	// App was launched with /Embedding or /Automation switch.
	// Run app as automation server.
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
#ifdef _DEBUG
        MessageBox(0, _T("In Msc-generator::Appinit"), _T("aa"), 0);
#endif
        // Register all OLE server factories as running.  This enables the
		//  OLE libraries to create objects from other applications
		COleTemplateServer::RegisterAll();

		// Don't show the main window
		return TRUE;
	}
	// App was launched with /Unregserver or /Unregister switch.  Unregister
	// typelibrary.  Other unregistration occurs in ProcessShellCommand().
	else if (cmdInfo.m_nShellCommand == CCommandLineInfo::AppUnregister)
	{
		UnregisterShellFileTypes();
		m_server.UpdateRegistry(OAT_DOC_OBJECT_SERVER, nullptr, nullptr, FALSE);
	}
	// App was launched standalone or with other switches (e.g. /Register
	// or /Regserver).  Update registry entries, including typelibrary.
	else if (cmdInfo.m_nShellCommand == CCommandLineInfo::AppRegister)
	{
		m_server.UpdateRegistry(OAT_DOC_OBJECT_SERVER);
	}
	// App started with other switches or standalone, see if there is -T to do a command line run
	//else if (!CommandLineMain(m_lpCmdLine))
	//	return FALSE;

    //Get Printer info (may be needed to print from command line)
    UpdatePrinterData();

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

    //Show autorecovery pane if needed
    CMainFrame * pMainWnd = dynamic_cast<CMainFrame*>(m_pMainWnd);
    if (pMainWnd) {
        //If the user wants to open the recovery pane and we do have saved recovery files....
        //...but only if we started the tool without opening a file...
        if ((cmdInfo.m_nShellCommand==CCommandLineInfo::FileNew ||
            cmdInfo.m_nShellCommand==CCommandLineInfo::RestartByRestartManager) &&
            GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_OPEN_AT_STARTUP, TRUE) &&
                pMainWnd->m_wndAutoSaveFiles.CollectAutoSavedFiles(
                    GetDataRecoveryHandler()->GetAutosavePath(),
                    GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE) ? m_config.m_lang : nullptr))
            //...then open the recovery category (will trigger the opening of the recovery pane too)
            pMainWnd->m_wndRibbonBar.SetActiveCategory(pMainWnd->m_wndRibbonBar.GetCategory(4));
        else
            //..else close the recovery pane (may have been opened because it was open last time we closed the app)
            pMainWnd->m_wndAutoSaveFiles.ShowPane(false, false, false); //hide even if latest state was showing (and got reloaded)
    }

    if (GetInternalEditor()) {
        //enumerate fonts for hints
        HDC hdc = GetDC(nullptr);
        EnumFontFamilies(hdc, nullptr, (FONTENUMPROC)&EnumFamCallBack, LPARAM(&m_config.m_languages.cshParams->fontnames));
        ReleaseDC(nullptr, hdc);
        //Set focus to Internal editor, if any
        if (GetInternalEditor()->IsVisible())
            m_pWndEditor->m_ctrlEditor.SetFocus();
    }

	return TRUE;
}

/** Terminate us.
 * Just add AfxOleTerm() to the default method*/
int CMscGenApp::ExitInstance()
{
	AfxOleTerm(FALSE);

	return CWinAppEx::ExitInstance();
}


/** App command to run the about dialog */
void CMscGenApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/** App command to run the help */
void CMscGenApp::OnHelp()
{
    TCHAR buff[2048];
    if (GetModuleFileName(nullptr, buff, 2048)) {
        PathRemoveFileSpec(buff);
        CString help = buff;
        help += _T("\\msc-gen.chm");
        ShellExecute(nullptr, nullptr, help, nullptr, nullptr, SW_SHOWNORMAL);
    }
}

/** App command to run the help */
void CMscGenApp::OnExamples() {
    CExampleDialog dlg;
    dlg.DoModal();
}

void CMscGenApp::OnFileNew()
{
    if (AskForLanguage())
        CWinAppEx::OnFileNew();
}

void CMscGenApp::OnFileOpen()
{
    CFileDialog dlgFile(TRUE, nullptr, nullptr, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_FILEMUSTEXIST, nullptr, nullptr, 0);

    CString title;
    ENSURE(title.LoadString(AFX_IDS_OPENFILE));

    CString strFilter;
    CString strDefault = _T("signalling");
    auto langs = m_config.m_languages.GetLanguages();
    CString def = GetProfileString(REG_SECTION_SETTINGS, REG_KEY_TYPICAL_NEW_CHART, _T(""));
    dlgFile.m_ofn.nMaxCustFilter = 0;
    dlgFile.m_ofn.nFilterIndex = 0;
    strFilter = L"Supported chart files";
    strFilter += (TCHAR)'\0';
    for (auto &s : langs) {
        auto pLang = m_config.m_languages.GetLanguage(s);
        for (auto &e : pLang->extensions) {
            if (strFilter[strFilter.GetLength()-1]) //not ending in nullptr
                strFilter += _T(";");
            strFilter += _T("*.");
            strFilter += AsUnicode(e);
        }
    }
    // append the "*.png" filter
    strFilter += _T(";*.png");
    strFilter += (TCHAR)'\0';   // last string
    dlgFile.m_ofn.nMaxCustFilter++;
    // append the "*.*" all files filter
    CString allFilter;
    VERIFY(allFilter.LoadString(AFX_IDS_ALLFILTER));
    strFilter += allFilter;
    strFilter += (TCHAR)'\0';   // next string please
    strFilter += _T("*.*");
    strFilter += (TCHAR)'\0';   // last string
    dlgFile.m_ofn.nMaxCustFilter++;
    dlgFile.m_ofn.nFilterIndex = 1;

    CString fileName;
    dlgFile.m_ofn.lpstrFilter = strFilter;
    dlgFile.m_ofn.lpstrTitle = title;
    dlgFile.m_ofn.lpstrFile = fileName.GetBuffer(_MAX_PATH);

    INT_PTR nResult = dlgFile.DoModal();
    fileName.ReleaseBuffer();

    int i = 1;
    for (auto &s : langs) {
        if (i++!=dlgFile.m_ofn.nFilterIndex)
            continue;
        WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_TYPICAL_NEW_CHART,
                           AsUnicode(m_config.m_languages.GetLanguage(s)->description));
        GetDoc()->m_suggested_lang = s;
        break;
    }
    if (nResult == IDOK)
        OpenDocumentFile(fileName);
}


/** CMscGenApp customization load/save methods*/
void CMscGenApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
    GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
    bNameValid = strName.LoadString(IDS_EDIT_MENU_INTERNAL);
    GetContextMenuManager()->AddMenu(strName, IDR_POPUP_INTERNAL_EDITOR);
}

/** Do nothing*/
void CMscGenApp::LoadCustomState()
{
}

/** Do nothing*/
void CMscGenApp::SaveCustomState()
{
}

bool CMscGenApp::AskForLanguage()
{
    if (!GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, TRUE)) {
        //if the user dont want us to ask the language, see if the last selection
        //is supported. If yes, select that language, else, offer the selection to the user.
        const CStringA lang_name = AsUTF8(GetProfileString(REG_SECTION_SETTINGS, REG_KEY_TYPICAL_NEW_CHART, _T("signalling")));
        return m_config.SetLanguage(m_config.m_languages.GetLanguageByDescr(std::string_view(lang_name, lang_name.GetLength())));
    }
    CLanguageSelectorDlg dlg;
    if (IDCANCEL == dlg.DoModal())
        return false;
    auto lang = m_config.m_languages.GetLanguageByDescr(std::string_view(AsUTF8(dlg.lang)));
    _ASSERT(lang);
    //OK, we will succeed - change language
    if (lang) {
        m_config.SetLanguage(lang);
        WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_TYPICAL_NEW_CHART, dlg.lang);
        return true;
    }
    return false;
}

/** Reads page geometry for the default printer and stores it in MscGenApp.*/
void CMscGenApp::UpdatePrinterData()
{
    CDC dc;
    UpdatePrinterSelection(m_hDevNames == nullptr); //force default if no current
    if (CreatePrinterDC(dc)) {
        //Get printer resolution (printer pixels per inch = DPI)
        const XY res(dc.GetDeviceCaps(LOGPIXELSX), dc.GetDeviceCaps(LOGPIXELSY));
        //convert to point/printer pixel scaling (1 point = 1/72 inch = msc-generator pixel)
        const XY sc = res/72;
        //Get page size in points
        const XY phy = XY(dc.GetDeviceCaps(PHYSICALWIDTH)/sc.x, dc.GetDeviceCaps(PHYSICALHEIGHT)/sc.y);
        //Get offset of printable area in point
        const XY off(dc.GetDeviceCaps(PHYSICALOFFSETX)/sc.x, dc.GetDeviceCaps(PHYSICALOFFSETY)/sc.y);
        //Get printable area in point
        const XY printable(dc.GetDeviceCaps(HORZRES)/sc.x, dc.GetDeviceCaps(VERTRES)/sc.y);

        m_config.m_PrinterScale = sc;
        m_config.m_PhyPrinterPageSize = phy;
        m_config.m_pageSize = EPageSize::NO_PAGE;
        for (unsigned u = unsigned(EPageSize::NO_PAGE)+1; u < unsigned(EPageSize::MAX_PAGE); u++)
            if ((GetPhysicalPageSize(EPageSize(u)) - phy).length_sqr()<10) {
                m_config.m_pageSize = EPageSize(u);
                break;
            }

        m_config.m_printer_phy_margins[0] = off.x;
        m_config.m_printer_phy_margins[1] = phy.x -printable.x - off.x;
        m_config.m_printer_phy_margins[2] = off.y;
        m_config.m_printer_phy_margins[3] = phy.y -printable.y - off.y;
        _ASSERT(m_config.m_printer_phy_margins[1]>0);
        _ASSERT(m_config.m_printer_phy_margins[3]>0);
    }
    m_config.NormalizeUserMargins();
}


/** Provide our own recovery handler of type CMscGenDataRecoveryHandler. */
CDataRecoveryHandler * CMscGenApp::GetDataRecoveryHandler()
{
    static BOOL bTriedOnce = FALSE;

    // Since the application restart and application recovery are supported only on Windows
    // Vista and above, we don't need a recovery handler on Windows versions less than Vista.
    if (SupportsRestartManager() || SupportsApplicationRecovery()) {
        if (!bTriedOnce && m_pDataRecoveryHandler == nullptr) {
            m_pDataRecoveryHandler = new CMscGenDataRecoveryHandler(m_dwRestartManagerSupportFlags, m_nAutosaveInterval);
            if (!m_pDataRecoveryHandler->Initialize()) {
                delete m_pDataRecoveryHandler;
                m_pDataRecoveryHandler = nullptr;
            } else {
                m_pDataRecoveryHandler->SaveOpenDocumentList();
            }
        }
    }

    bTriedOnce = TRUE;
    return m_pDataRecoveryHandler;
}



/** Version reminder dialog */
class CVersionDlg : public CDialog
{
public:
	CVersionDlg() : CDialog(CVersionDlg::IDD), a(-1), b(-1), c(-1)
	{}

// Dialog Data
	enum { IDD = IDD_DIALOG_VERSION };
	CMFCLinkCtrl m_btnLink; ///<The link of the msc-generator home page

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    ///< DDX/DDV support
	virtual BOOL OnInitDialog( );                       ///< Main initialization
public:
	int a; ///<Major version
    int b; ///<Minor version
    int c; ///<Superminor (or micro) version
};

void CVersionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_URL2, m_btnLink);
}

BOOL CVersionDlg::OnInitDialog( )
{
	BOOL ret = CDialog::OnInitDialog();
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    const CString latest_url = pApp->GetProfileString(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_URL);
	m_btnLink.SetURL(latest_url);
	m_btnLink.SetTooltip(latest_url);
	m_btnLink.SizeToContent();
 	CString text = _T("Currently installed version: ");
    GetDlgItem(IDC_STATIC_CURRENT_VERSION)->SetWindowText(text + AsUnicode(VersionText()));
 	text = _T("Latest version available: ");
    GetDlgItem(IDC_STATIC_LATEST_VERSION)->SetWindowText(text + AsUnicode(VersionText(a, b, c)));
	return ret;
}


/** The main function of a background thread that checks
 * if our version is current. If yes, it silently returns.
 * If no, and the user has not blocked reminders for the
 * latest version available, we display a reminder message box.
 * We handle user wishes on that box. (Ignore this version, remind later)*/
UINT CheckVersionFreshness(LPVOID)
{
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    CString latest_version_download_url;
    int latest_version_a=0, latest_version_b=0, latest_version_c=0;
	TRY {
		CInternetSession session(_T("Msc-generator"), 1, 0, nullptr, nullptr, INTERNET_FLAG_DONT_CACHE);
		CHttpConnection *httpconn = session.GetHttpConnection(_T("gitlab.com"),
			INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE, INTERNET_PORT(80));
		CHttpFile *file = httpconn->OpenRequest(_T("GET"), _T("api/v4/projects/31167732/packages/generic/msc-generator/1.0/version.txt"),
                                                nullptr, 1, nullptr, nullptr, INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);
        file->AddRequestHeaders(L"User-Agent: Msc-generator/" + AsUnicode(std::string_view(VersionText()).substr(1)) + L" (Windows)",
                                HTTP_ADDREQ_FLAG_ADD_IF_NEW);
		if (!file->SendRequest()) return false;
        char buff[10000];
		if (nullptr==file->ReadString((LPTSTR)buff, sizeof(buff)/2)) return false;
        if (0==sscanf_s(buff, "v%d.%d.%d", &latest_version_a, &latest_version_b, &latest_version_c)) return false;
        pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_MAJOR, latest_version_a);
        pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_MINOR, latest_version_b);
        pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_SUPER_MINOR, latest_version_c);
        if (file->ReadString((LPTSTR)buff, sizeof(buff)/2) && buff[0]=='h' && buff[1]=='t')
            pApp->WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_LAST_VERSION_SEEN_URL, AsUnicode(std::string_view(buff)));
	} CATCH(CInternetException, pEx) {
        return false;
	}
	END_CATCH

	//If we are the latest version (or erroneous string read from web), exit
	if (latest_version_a<LIBMSCGEN_MAJOR) return false;
	if (latest_version_a==LIBMSCGEN_MAJOR && latest_version_b<LIBMSCGEN_MINOR) return false;
	if (latest_version_a==LIBMSCGEN_MAJOR && latest_version_b==LIBMSCGEN_MINOR && latest_version_c<=LIBMSCGEN_SUPERMINOR) return false;

	//If user do not want to get reminded for the new version, exit
	int no_a = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_NOREMIND_VERSION_MAJOR, -1);
	int no_b = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_NOREMIND_VERSION_MINOR, -1);
	int no_c = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_NOREMIND_VERSION_SUPER_MINOR, -1);
	if (no_a == latest_version_a && no_b == latest_version_b && no_c == latest_version_c) return false;

    CVersionDlg dlg;
    dlg.a = latest_version_a;
    dlg.b = latest_version_b;
    dlg.c = latest_version_c;

    if (dlg.DoModal() == IDOK) {
		pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_NOREMIND_VERSION_MAJOR, dlg.a);
		pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_NOREMIND_VERSION_MINOR, dlg.b);
		pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_NOREMIND_VERSION_SUPER_MINOR, dlg.c);
	}
	return true;
}

/** Store the current chart text as default chart text in the registry.
 * Updates the registry.*/
void CMscGenApp::OnButtonDefaultText()
{
    //Get a modifiable copy of the language
    LanguageData *lang = m_config.m_languages.GetLanguageToModify(m_config.m_lang->GetName());
    lang->default_text = GetDoc()->m_itrEditing->GetText();
    WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_DEFAULTTEXT + AsUnicode(m_config.m_lang->GetName()), AsUnicode(m_config.m_lang->default_text));
}

/** Enable/Disable the button if the curren text is different/same as the default.*/
void CMscGenApp::OnUpdateButtonDefaultText(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(m_config.m_lang->default_text.c_str() != GetDoc()->m_itrEditing->GetText());
}

/** Toggles the pedantic compilation flag.
 * Updates the registry.
 * Recompile if the current compiled version is up-tp-date.*/
void CMscGenApp::OnCheckPedantic()
{
    m_config.m_Pedantic = !m_config.m_Pedantic;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_PEDANTIC, m_config.m_Pedantic);
    //recompile if it was a recently compiled
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);
}

/** Copy the m_Pedantic flag to the button*/
void CMscGenApp::OnUpdateCheckPedantic(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_Pedantic);
}

/** Toggles the "include warnings" flag.
 * Updates the registry.
 * Recompile if the current compiled version is up-tp-date.*/
void CMscGenApp::OnCheckWarnings()
{
    m_config.m_Warnings = !m_config.m_Warnings;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_WARNINGS, m_config.m_Warnings);
    //recompile if it was a recently compiled
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);
}


/** Copy the m_Warnings flag to the button*/
void CMscGenApp::OnUpdateCheckWarnings(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_Warnings);
}

/** Toggles the "full path in error messages" flag.
 * Updates the registry.
 * Recompile if the current compiled version is up-tp-date.*/
void CMscGenApp::OnCheckFullPath()
{
    m_config.m_FullPath = !m_config.m_FullPath;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_FULL_PATH_IN_ERRORS, m_config.m_FullPath);
    //recompile if it was a recently compiled
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);

}


/** Copy the m_FullPath flag to the button*/
void CMscGenApp::OnUpdateCheckFullPath(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_FullPath);
}


/** Toggles the "include page breaks" flag.
 * Updates the registry.
* Recompile if the current compiled version is up-to-date and has
* multiple pages.*/
void CMscGenApp::OnCheckPageBreaks()
{
    m_config.m_bPageBreaks = !m_config.m_bPageBreaks;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_PB_EDITING, m_config.m_bPageBreaks);
    //recompile
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc && pDoc->m_ChartShown.GetPages()>1)
        pDoc->CompileEditingChart(false, false, true);
}

/** Enable the button always*/
void CMscGenApp::OnUpdateCheckPageBreaks(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bPageBreaks);
    //pCmdUI->Enable(m_lang->has_pages);
}

/** Reads the current value of the Csh scheme combo box,
 * updates the registry
 * Updates csh info in the internal editor, if any.*/
void CMscGenApp::OnComboCsh()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(IDC_COMBO_CSH, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    int sel = c->GetCurSel();
    if (m_config.m_languages.cshParams->color_scheme  == sel)
        return;
    if (sel==0) {
        m_config.m_bShowCsh = false;
    } else {
        m_config.m_bShowCsh = true;
        m_config.m_languages.cshParams->color_scheme = sel-1; //'minimal' is of value zero
    }
	WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_CSHSCHEME, m_config.m_languages.cshParams->color_scheme);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_CSHENABLED, m_config.m_bShowCsh);
    if (IsInternalEditorVisible())
        m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
    UpdateMscgenCompatPane();
}


/** Enables the Csh combo box if csh is enabled and we have
 * an internal editor*/
void CMscGenApp::OnUpdateComboCsh(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_lang->pCsh);
}


/** Toggles the smart indentation button.
 * Updates the registry.
 * Updates the csh info if the internal editor is running.*/
void CMscGenApp::OnCheckSmartIndent()
{
    m_config.m_bSmartIndent = !m_config.m_bSmartIndent;
	WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SMARTINDENT, m_config.m_bSmartIndent);
    if (IsInternalEditorVisible() && m_config.m_bSmartIndent)
        m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::HINTS_AND_LABELS);
    UpdateMscgenCompatPane();
}

/** Toggles the smart identation button.
* Updates the registry.
* Updates the csh info if the internal editor is running.*/
void CMscGenApp::OnCheckTABIndents()
{
    m_config.m_bTABIndents = !m_config.m_bTABIndents;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TABINDENTS, m_config.m_bTABIndents);
}

/** Enables the button only if we show csh and the internal editor is running.
 * Checks it if m_bTABIndents is true.*/
void CMscGenApp::OnUpdateCheckTABIndents(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_lang->pCsh);
    pCmdUI->SetCheck(m_config.m_bTABIndents);
}

/** Enables the button only if we show csh and the internal editor is running.
* Checks it if m_bSmartIndent is true.*/
void CMscGenApp::OnUpdateCheckSmartIndent(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_lang->pCsh);
    pCmdUI->SetCheck(m_config.m_bSmartIndent);
}

/** Toggels the colorize errors checkbox.
 * Updates the registry.
 * Updates the csh info if the internal editor is running.*/
void CMscGenApp::OnCheckCshError()
{
    m_config.m_bShowCshErrors = !m_config.m_bShowCshErrors;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_CSHERRORS, m_config.m_bShowCshErrors);
    if (IsInternalEditorVisible())
        m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
    UpdateMscgenCompatPane();
}

/** Enables the button only if we show csh and the internal editor is running.
 * Checks it if m_bShowCshErrors is true.*/
void CMscGenApp::OnUpdateCheckCshError(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_lang->pCsh);
    pCmdUI->SetCheck(m_config.m_bShowCshErrors);
}

/** Toggles whether the csh errors are listed in the error window.
 * Updates the registry.
 * Updates the csh info if the internal editor is running.*/
void CMscGenApp::OnCheckCshErrorInWindow()
{
    m_config.m_bShowCshErrorsInWindow = !m_config.m_bShowCshErrorsInWindow;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_CSHERRORSINWINDOW, m_config.m_bShowCshErrorsInWindow);
    if (IsInternalEditorVisible())
        m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
    UpdateMscgenCompatPane();
}

/** Enables the button only if we show csh and the internal editor is running.
 * Checks it if m_bShowCshErrorsInWindow is true.*/
void CMscGenApp::OnUpdateCheckCshErrorInWindow(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_lang->pCsh);
    pCmdUI->SetCheck(m_config.m_bShowCshErrorsInWindow);
}

/** Opens the external text editor preferences dialog*/
void CMscGenApp::OnEditPreferences()
{
	CMscGenDoc *pDoc = GetDoc();

	CExternalEditorOptionDlg optionDlg;
	optionDlg.m_TextEditStartCommand = m_config.m_sStartTextEditor;
	optionDlg.m_TextEditorJumpToLineCommand = m_config.m_sJumpToLine;

    if (optionDlg.DoModal() == IDOK) {
        if (m_config.m_sStartTextEditor != optionDlg.m_TextEditStartCommand) {
            m_config.m_sStartTextEditor = optionDlg.m_TextEditStartCommand;
			WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_STARTTEXTEDITOR, m_config.m_sStartTextEditor);
			if (pDoc && pDoc->m_ExternalEditor.IsRunning())
                pDoc->m_ExternalEditor.Restart(STOPEDITOR_WAIT);
		}
		if (m_config.m_sJumpToLine != optionDlg.m_TextEditorJumpToLineCommand) {
            m_config.m_sJumpToLine = optionDlg.m_TextEditorJumpToLineCommand;
			WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_JUMPTOLINE, m_config.m_sJumpToLine);
		}
	}
}

/** Enables the button if the other external text editor is selected.*/
void CMscGenApp::OnUpdateEditPreferences(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(m_config.m_iTextEditorType==CMscGenConf::OTHER);
}

//C:\Windows\Notepad.exe %n;"C:\Program Files\Notepad++\notepad++.exe" -multiInst -nosession %n;
//;-n%l -nosession %n;


/** Selects the "other" external editor type. Restarts the external editor if needed*/
void CMscGenApp::OnCheckEeOther()
{
    if (m_config.m_iTextEditorType == CMscGenConf::OTHER) return;
    m_config.m_iTextEditorType = CMscGenConf::OTHER;
    m_config.m_sStartTextEditor = GetProfileString(REG_SECTION_SETTINGS, REG_KEY_STARTTEXTEDITOR, _T("Notepad.exe %n"));
    m_config.m_sJumpToLine = GetProfileString(REG_SECTION_SETTINGS, REG_KEY_JUMPTOLINE, _T(""));
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TEXTEDITORTYPE, m_config.m_iTextEditorType);
	CMscGenDoc *pDoc = GetDoc();
    if (pDoc && pDoc->m_ExternalEditor.IsRunning())
        pDoc->m_ExternalEditor.Restart(STOPEDITOR_WAIT);
}

/** Selects the notepad as external editor type.
 * Restarts the external editor if needed*/
void CMscGenApp::OnCheckEeNotepad()
{
    if (m_config.m_iTextEditorType == CMscGenConf::NOTEPAD) return;
    m_config.m_iTextEditorType = CMscGenConf::NOTEPAD;
    m_config.m_sStartTextEditor = "C:\\Windows\\Notepad.exe %n";
    m_config.m_sJumpToLine = "";
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TEXTEDITORTYPE, m_config.m_iTextEditorType);
	CMscGenDoc *pDoc = GetDoc();
    if (pDoc && pDoc->m_ExternalEditor.IsRunning())
        pDoc->m_ExternalEditor.Restart(STOPEDITOR_WAIT);
}

/** Selects the notepad++ as external editor type.
 * Restarts the external editor if needed*/
void CMscGenApp::OnCheckEeNotepadpp()
{
    if (m_config.m_iTextEditorType == CMscGenConf::NPP) return;
    m_config.m_iTextEditorType = CMscGenConf::NPP;
    m_config.m_sStartTextEditor = _T("\"") +m_config.m_NppPath+ _T("\" -multiInst -nosession %n");
    m_config.m_sJumpToLine = _T("\"") +m_config.m_NppPath+ _T("\" -n%l -nosession %n");
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TEXTEDITORTYPE, m_config.m_iTextEditorType);
	CMscGenDoc *pDoc = GetDoc();
    if (pDoc && pDoc->m_ExternalEditor.IsRunning())
        pDoc->m_ExternalEditor.Restart(STOPEDITOR_WAIT);
}


/** Always enable the button.
 * Check it if the type of external editor selected is other */
void CMscGenApp::OnUpdateCheckEeOther(CCmdUI *pCmdUI)
{
    pCmdUI->Enable();
    pCmdUI->SetCheck(m_config.m_iTextEditorType==CMscGenConf::OTHER);
}

/** Always enable the button.
 * Check it if the type of external editor selected is notepad*/
void CMscGenApp::OnUpdateCheckEeNotepad(CCmdUI *pCmdUI)
{
    pCmdUI->Enable();
    pCmdUI->SetCheck(m_config.m_iTextEditorType==CMscGenConf::NOTEPAD);
}


/** Always enable the button.
 * Check it if the type of external editor selected is notepad++*/
void CMscGenApp::OnUpdateCheckEeNotepadpp(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(m_config.m_NppPath.GetLength()>0);
    pCmdUI->SetCheck(m_config.m_iTextEditorType==CMscGenConf::NPP);
}

/** Toggle whether hints are provided or not.
 * Updates the registry*/
void CMscGenApp::OnCheckHints()
{
    m_config.m_bHints = !m_config.m_bHints;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT, m_config.m_bHints);
    UpdateMscgenCompatPane();
}

/** Check the button if hints are provided*/
void CMscGenApp::OnUpdateCheckHints(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bHints);
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_lang->pCsh);
}

/** Toggle whether hints are provided or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintCompact()
{
    m_config.m_bHintCompact = !m_config.m_bHintCompact;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SHOW_CONTROLS, m_config.m_bHintCompact);
}


/** Handle changes to the filtering Combo
 * Updates the registry*/
void CMscGenApp::OnComboSmartHintFilter()
{
    CMainFrame* const pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(IDC_COMBO_HINT_FILTER, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    const CMFCRibbonComboBox* const c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    const int sel = c->GetCurSel();
    if (m_config.m_iHintFilter == sel)
        return;
    m_config.m_iHintFilter = sel;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_FILTER, m_config.m_iHintFilter);
}

/** Enables the Filter combo box if hinting is enabled and we have
 * an internal editor*/
void CMscGenApp::OnUpdateComboSmartHintFilter(CCmdUI* pCmdUI) {
    pCmdUI->Enable(IsInternalEditorVisible() && m_config.m_bHints);
}


/** Toggle whether hints are provided at the beginning of a line or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintLineStart()
{
    m_config.m_bHintLineStart = !m_config.m_bHintLineStart;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_LINESTART, m_config.m_bHintLineStart);
}


/** Toggle whether hints are provided for entities or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintEntity()
{
    m_config.m_bHintEntity = !m_config.m_bHintEntity;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_ENTITY, m_config.m_bHintEntity);
}

/** Toggle whether hints are provided for text formatting escapes or or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintEscape()
{
    m_config.m_bHintEscape = !m_config.m_bHintEscape;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_ESCAPE, m_config.m_bHintEscape);
}


/** Toggle whether hints are provided for attribute names or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintAttrName()
{
    m_config.m_bHintAttrName = !m_config.m_bHintAttrName;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_ATTRNAME, m_config.m_bHintAttrName);
}


/** Toggle whether hints are provided for mid-command keywords and markers or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintKeywordMarker()
{
    m_config.m_bHintKeywordMarker = !m_config.m_bHintKeywordMarker;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_ATTRNAME, m_config.m_bHintKeywordMarker);
}


/** Toggle whether hints are provided for attribute values or not.
 * Updates the registry*/
void CMscGenApp::OnCheckSmartHintAttrValue()
{
    m_config.m_bHintAttrValue = !m_config.m_bHintAttrValue;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_HINT_ATTRVALUE, m_config.m_bHintAttrValue);
}


/** Enables the button if we provide hints.
 * Sets it according to its current value.*/
void CMscGenApp::OnUpdateCheckSmartHintBoxes(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(m_config.m_bHints && m_config.m_lang->pCsh && IsInternalEditorVisible());
    bool set = false;
    switch (pCmdUI->m_nID) {
    case IDC_CHECK_SMART_HINT_COMPACT: set = m_config.m_bHintCompact; break;
    case IDC_CHECK_SMART_HINT_LINE_START: set = m_config.m_bHintLineStart; break;
    case IDC_CHECK_SMART_HINT_ENTITY: set = m_config.m_bHintEntity; break;
    case IDC_CHECK_SMART_HINT_ESCAPE: set = m_config.m_bHintEscape; break;
    case IDC_CHECK_SMART_HINT_ATTR_NAME: set = m_config.m_bHintAttrName; break;
    case IDC_CHECK_SMART_HINT_ATTR_VALUE: set = m_config.m_bHintAttrValue; break;
    case IDC_CHECK_SMART_HINT_KEYWORD_MARKER: set = m_config.m_bHintKeywordMarker; break;
    default: _ASSERT(0);
    }
    pCmdUI->SetCheck(set);
}



/** Updates the tracking color after a change of the respective button on the ribbon.
 * Updates the registry*/
void CMscGenApp::OnButtonTrackColor()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(ID_BUTTON_TRACK_COLOR, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonColorButton *cb = dynamic_cast<CMFCRibbonColorButton*>(arButtons[0]);
    COLORREF c = cb->GetColor();
    const unsigned char r = GetRValue(c);
    const unsigned char g = GetGValue(c);
    const unsigned char b = GetBValue(c);
    m_config.m_trackFillColor = RGBA(r, g, b, 128); //50% transparent
    m_config.m_trackLineColor = RGBA(r/2, g/2, b/2, 255); //50% darker, fully opaque
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TRACKLINERGBA, m_config.m_trackLineColor);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TRACKFILLRGBA, m_config.m_trackFillColor);
}

/** Updates the fallback image resolution after a change on the slider.
 * Updates the registry. We also update the container document and
 * do the fallback image animation in ReDrawEMF().*/
void CMscGenApp::OnEmbeddedoptionsFallbackRes()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(ID_EMBEDDEDOPTIONS_FALLBACK_RES, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonSlider *s = dynamic_cast<CMFCRibbonSlider *>(arButtons[0]);
    if (!s) return;
    if (m_config.m_uFallbackResolution == s->GetPos()) return; //nothing to do
    m_config.m_uFallbackResolution = s->GetPos();

    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_FALLBACK_RESOLUTION, m_config.m_uFallbackResolution);
    //recompile if it is embdedded
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc && pDoc->IsEmbedded()) {
        pDoc->m_ChartShown.SetFallbackResolution(m_config.m_uFallbackResolution);
        pDoc->ReDrawEMF();
        pDoc->CheckIfChanged();
    }
}


/** Toggles autopaginate
 * Updates registry. Recompiles if no changes since last compilation.*/
void CMscGenApp::OnAutoPaginate()
{
    m_config.m_bAutoPaginate = !m_config.m_bAutoPaginate;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTO_PAGINATE, m_config.m_bAutoPaginate);
	CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);
}

/** Checks the button if autopaginate is enabled.*/
void CMscGenApp::OnUpdateAutoPaginate(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bAutoPaginate);
    //pCmdUI->Enable(m_lang->has_pages)
}


/** Toggles autoheaders.
 * Updates registry. Recompiles if no changes since last compilation.*/
void CMscGenApp::OnOriginalEngine()
{
    m_config.m_bGraphUsesOriginalEngine = !m_config.m_bGraphUsesOriginalEngine;
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true); //track mode turned off by this, too
}

/** Enables the button if automatic pagination is on, checks it if auto headings
 * are generated.*/
void CMscGenApp::OnUpdateOriginalEngine(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bGraphUsesOriginalEngine);
    pCmdUI->Enable(m_config.m_lang->GetName()=="graph");
}

/** Toggles autoheaders.
* Updates registry. Recompiles if no changes since last compilation.*/
void CMscGenApp::OnAutoHeaders()
{
    m_config.m_bAutoHeading = !m_config.m_bAutoHeading;
    if (!m_config.m_bAutoPaginate) return;
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc && m_config.m_bAutoPaginate)
        pDoc->CompileEditingChart(false, false, true);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTO_HEADING, m_config.m_bAutoHeading);
}

/** Enables the button if automatic pagination is on, checks it if auto headings
* are generated.*/
void CMscGenApp::OnUpdateAutoHeaders(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bAutoHeading);
    pCmdUI->Enable(m_config.m_bAutoPaginate && m_config.m_lang->has_autoheading);
}

/** Updates the scaling value after a change of it on the GUI.
 * Updates the registry and recompiles if automatic pagination is on.*/
void CMscGenApp::OnComboScale()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(ID_COMBO_SCALE, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
	CMscGenDoc *pDoc = GetDoc();
    if (!pDoc) return;
    const std::string val = AsUTF8str(c->GetEditText());
    int new_val = m_config.m_iScale4Pagination;
    if (from_chars(val, new_val)) {
        if (CaseInsensitiveEqual(val, "Fit Width")) new_val = -1;
        else if (CaseInsensitiveEqual(val, "Fit Page")) new_val = -2;
    }
    if (m_config.m_iScale4Pagination==new_val)
        return;
    m_config.m_iScale4Pagination = new_val;
    //m_iScale4Pagination may have been left unchanged, but we normalize text
    pMainWnd->FillScale4Pagination();
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SCALE4PAGINATION, m_config.m_iScale4Pagination);

    if (m_config.m_bAutoPaginate)
        pDoc->CompileEditingChart(false, false, true);
    pDoc->UpdateAllViews(nullptr);
}


void CMscGenApp::OnButtonPages()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CPreviewView *pView = dynamic_cast<CPreviewView *>(pMainWnd->GetActiveView());
    if (!pView) return;
    //TODO: Change page size in print prview
    OnFilePrintSetup();
    pMainWnd->FillPageSize();
}

void CMscGenApp::OnComboAlignment()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(ID_COMBO_ALIGNMENT, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    m_config.m_iPageAlignment = c->GetCurSel() - 4;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_PAGE_ALIGNMENT, m_config.m_iPageAlignment);
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(pMainWnd->GetActiveDocument());
    if (pDoc)
        pDoc->UpdateAllViews(nullptr);
}




/** Update our user specified margins after a change of them on the GUI.
 * Update the registry and recompile if needed.*/
void CMscGenApp::DoEditMargin(UINT id)
{
    _ASSERT(id == ID_EDIT_MARGIN_L || id == ID_EDIT_MARGIN_R ||
            id == ID_EDIT_MARGIN_T || id == ID_EDIT_MARGIN_B);
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (!pMainWnd) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(id, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
	CMscGenDoc *pDoc = GetDoc();
    if (!pDoc || !c) return;
    const std::string val = AsUTF8str(c->GetEditText());
    bool change = false;
    double cm;
    if (!from_chars(val, cm)) {
        switch (id) {
        case ID_EDIT_MARGIN_L: m_config.m_printer_usr_margins[0] = cm * PT_PER_CM; break;
        case ID_EDIT_MARGIN_R: m_config.m_printer_usr_margins[1] = cm * PT_PER_CM; break;
        case ID_EDIT_MARGIN_T: m_config.m_printer_usr_margins[2] = cm * PT_PER_CM; break;
        case ID_EDIT_MARGIN_B: m_config.m_printer_usr_margins[3] = cm * PT_PER_CM; break;
        }
        m_config.NormalizeUserMargins();
        change = true;
    }
    //m_printer_usr_margins may have been left unchanged, but we normalize text
    pMainWnd->FillMargins();
    if (!change) return;

    CString val2;
    val2.Format(_T("%lf"), m_config.m_printer_usr_margins[0]);
    WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_PAGE_MARGIN_L, val2);
    val2.Format(_T("%lf"), m_config.m_printer_usr_margins[1]);
    WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_PAGE_MARGIN_R, val2);
    val2.Format(_T("%lf"), m_config.m_printer_usr_margins[2]);
    WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_PAGE_MARGIN_T, val2);
    val2.Format(_T("%lf"), m_config.m_printer_usr_margins[3]);
    WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_PAGE_MARGIN_B, val2);

    if (m_config.m_bAutoPaginate)
        pDoc->CompileEditingChart(false, false, true);
    pDoc->UpdateAllViews(nullptr);
}

/** Enable these edit boxes if we are in print preview*/
void CMscGenApp::OnUpdatePrintPreviewEdits(CCmdUI *pCmdUI)
{
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(GetMainWnd());
    pCmdUI->Enable(pWnd ? pWnd->IsPrintPreview() : false);
}

/** Black out the mscgen compatibility pane on the status bar if no CSH*/
void CMscGenApp::UpdateMscgenCompatPane()
{
    if (m_config.m_bShowCsh || m_config.m_bShowCshErrors ||
        m_config.m_bShowCshErrorsInWindow ||
        m_config.m_bSmartIndent || m_config.m_bHints)
        return; //do nothing if we will cshparse the text
}

/** Open the print setup dialog, read the page size after and
 * update our internal values. Recompile if needed.*/
void CMscGenApp::OnFilePrintSetup()
{
    CMainFrame *pMainWnd= dynamic_cast<CMainFrame *>(GetMainWnd());
    if (!pMainWnd) return;
    XY ps = m_config.m_PhyPrinterPageSize;
    pMainWnd->m_bAutoSplit = 0;
    CWinAppEx::OnFilePrintSetup();
    UpdatePrinterData();
    if (m_config.m_PhyPrinterPageSize == ps) return;
    pMainWnd->FillPageSize();
    pMainWnd->FillMargins();

	CMscGenDoc *pDoc = GetDoc();
    if (!pDoc) return;
    if (m_config.m_bAutoPaginate)
        pDoc->CompileEditingChart(false, false, true);
    pDoc->UpdateAllViews(nullptr);
}

/** Enable these edit boxes if we are in print preview*/
void CMscGenApp::OnUpdatePrintSetup(CCmdUI *pCmdUI)
{
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(GetMainWnd());
    pCmdUI->Enable(pWnd ? !pWnd->IsPrintPreview() : false);
}



void CMscGenApp::OnComboMscgenCompat()
{
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(GetMainWnd());
    if (!pWnd) return;
    pWnd->m_wndRibbonBar.GetElementsByID(ID_COMBO_MSCGEN_COMPAT, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    int sel = c->GetCurSel();
    _ASSERT(sel>=0 && sel<=2);
    if (m_config.m_mscgen_compat == EMscgenCompat(sel)) return;
    m_config.m_mscgen_compat = EMscgenCompat(sel);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_MSCGEN_COMPAT, int(m_config.m_mscgen_compat));
    msc::MscCsh *p = dynamic_cast<msc::MscCsh*>(m_config.m_languages.GetLanguage("signalling")->pCsh.get());
    if (p)
        p->mscgen_compat = m_config.m_mscgen_compat;
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);
    if (IsInternalEditorVisible())
        m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
}


void CMscGenApp::OnUpdateComboMscgenCompat(CCmdUI *pCmdUI)
{
    return;
}


/** When the 'warn on mscgen compatibility' checkbox on the ribbon is clicked.*/
void CMscGenApp::OnCheckMscgenCompatWarn()
{
    m_config.m_bWarnMscgenCompat = !m_config.m_bWarnMscgenCompat;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_MSCGEN_COMPAT_WARN, m_config.m_bWarnMscgenCompat);
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);
    if (IsInternalEditorVisible())
        m_pWndEditor->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
}


/** Called by the framework to see if the 'warn on mscgen compatibility' checkbox is enabled and/or checked.*/
void CMscGenApp::OnUpdateCheckMscgenCompatWarn(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bWarnMscgenCompat);
    pCmdUI->Enable(m_config.m_mscgen_compat!=EMscgenCompat::FORCE_MSCGEN);
}


/** When the 'ask language on new' checkbox on the ribbon is clicked.*/
void CMscGenApp::OnCheckAskLanguage()
{
    int i = !GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, TRUE);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, i);
}


/** Called by the framework to see if the 'ask language on new' checkbox is enabled and/or checked.*/
void CMscGenApp::OnUpdateCheckAskLanguage(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_ASK_FOR_NEW_CHART, TRUE));
}


void CMscGenApp::OnCheckAutosave()
{
    int i = !GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE, TRUE);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE, i);
}


void CMscGenApp::OnUpdateCheckAutosave(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE, TRUE));
}


void CMscGenApp::OnEditAutosaveInterval()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    pMainWnd->m_wndRibbonBar.GetElementsByID(ID_EDIT_AUTOSAVE_INTERVAL, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    CMscGenDoc *pDoc = GetDoc();
    if (!pDoc || !c) return;
    const std::string val = AsUTF8str(c->GetEditText());
    double cm;
    if (!from_chars(val, cm) && cm>=1 && cm<=10000) {
        unsigned value = unsigned(cm);
        WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_INTERVAL, value);
        GetDataRecoveryHandler()->SetAutosaveInterval(value*1000);
    }
    pMainWnd->SetEditBox(ID_EDIT_AUTOSAVE_INTERVAL, GetDataRecoveryHandler()->GetAutosaveInterval()/1000);
}


void CMscGenApp::OnUpdateEditAutosaveInterval(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE, TRUE));
}


void CMscGenApp::OnCheckOpenRecoveryAtStartup()
{
    int i = !GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_OPEN_AT_STARTUP, TRUE);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_OPEN_AT_STARTUP, i);
}


void CMscGenApp::OnUpdateCheckOpenRecoveryAtStartup(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_OPEN_AT_STARTUP, TRUE));
}


void CMscGenApp::OnCheckFilterAutosave()
{
    int i = !GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE);
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, i);
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    if (pMainWnd->m_wndAutoSaveFiles.IsVisible()) {
        pMainWnd->m_wndAutoSaveFiles.CollectAutoSavedFiles(
            GetDataRecoveryHandler()->GetAutosavePath(),
            GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE) ? m_config.m_lang : nullptr);
        pMainWnd->m_wndAutoSaveFiles.ShowIfHasEntries(true);
    }
}


void CMscGenApp::OnUpdateCheckFilterAutosave(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE));
}

void CMscGenApp::OnButtonDeleteAutosavedFiles()
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    pMainWnd->m_wndAutoSaveFiles.DeleteFilesAndHide();
}


void CMscGenApp::OnUpdateButtonDeleteAutosavedFiles(CCmdUI *pCmdUI)
{
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(GetMainWnd());
    pCmdUI->Enable(pMainWnd->m_wndAutoSaveFiles.IsVisible() &&
                   pMainWnd->m_wndAutoSaveFiles.m_wndFiles.m_files.size());
    pMainWnd->TriggerIfRibbonCategoryChange();
}


void CMscGenApp::OnCheckRecoveryOverwrite()
{
    m_config.m_bRecoveryOverwrites = true;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_RECOVERY_OVERWRITE, m_config.m_bRecoveryOverwrites);
}


void CMscGenApp::OnUpdateCheckRecoveryOverwrite(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bRecoveryOverwrites);
}


void CMscGenApp::OnCheckRecoveryInsertAtCursor()
{
    m_config.m_bRecoveryOverwrites = false;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_RECOVERY_OVERWRITE, m_config.m_bRecoveryOverwrites);
}


void CMscGenApp::OnUpdateCheckRecoveryInsertAtCursor(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(!m_config.m_bRecoveryOverwrites);
}


void CMscGenApp::OnCheckRecoveryDelete()
{
    m_config.m_bRecoveryDeletes = !m_config.m_bRecoveryDeletes;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_RECOVERY_DELETE, m_config.m_bRecoveryDeletes);
}


void CMscGenApp::OnUpdateCheckRecoveryDelete(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bRecoveryDeletes);
}

void CMscGenApp::OnCheckWarnCompilationTime()
{
    m_config.m_bTechnicalInfo = !m_config.m_bTechnicalInfo;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_TECHNICAL_INFO, m_config.m_bTechnicalInfo);
}


void CMscGenApp::OnUpdateCheckWarnCompilationTime(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bTechnicalInfo);
}

void CMscGenApp::OnCheckCurrentLineHighlight()
{
    m_config.m_bCurrentLineHighLight = !m_config.m_bCurrentLineHighLight;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_CURRENT_LINE_HIGHLIGHT, m_config.m_bCurrentLineHighLight);
    if (m_pWndEditor && m_pWndEditor->m_ctrlEditor) {
        m_pWndEditor->m_ctrlEditor.Invalidate();
        m_pWndEditor->m_ctrlEditor.UpdateWindow();
    }
}


void CMscGenApp::OnUpdateCheckCurrentLineHighlight(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bCurrentLineHighLight);
}

void CMscGenApp::OnCheckShowLineNums()
{
    m_config.m_bShowLineNums = !m_config.m_bShowLineNums;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SHOW_LINE_NUMS, m_config.m_bShowLineNums);
    if (m_pWndEditor && m_pWndEditor->m_ctrlEditor)
        m_pWndEditor->UpdateNumbers(true, true);
}


void CMscGenApp::OnUpdateCheckShowLineNums(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bShowLineNums);
}



void CMscGenApp::OnCheckSkipCopyright()
{
    m_config.m_bSkipCopyright = !m_config.m_bSkipCopyright;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SKIP_COPYRIGHT, m_config.m_bSkipCopyright);
    CMscGenDoc *pDoc = GetDoc();
    if (pDoc)
        pDoc->CompileEditingChart(false, false, true);
}

void CMscGenApp::OnUpdateCheckSkipCopyright(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_bSkipCopyright);
}


void CMscGenApp::OnCheckAutoPaste()
{
    m_config.m_autopaste= !m_config.m_autopaste;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTO_PASTE, m_config.m_autopaste);
}

void CMscGenApp::OnUpdateCheckAutoPaste(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_autopaste);
}


void CMscGenApp::OnCheckInstantCompilation()
{
    m_config.m_instant_compilation = !m_config.m_instant_compilation;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_INSTANT_COMPILATION, m_config.m_instant_compilation);
}

void CMscGenApp::OnUpdateCheckInstantCompilation(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_instant_compilation);
}

void CMscGenApp::OnCheckAutoSave()
{
    m_config.m_auto_save= !m_config.m_auto_save;
    WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTO_SAVE, m_config.m_auto_save);
}

void CMscGenApp::OnUpdateCheckAutoSave(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_config.m_auto_save);
}
