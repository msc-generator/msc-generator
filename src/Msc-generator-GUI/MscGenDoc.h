/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file MscGenDoc.h The main document class interface.
* @ingroup Msc_generator_files */


#pragma once
#include "ChartData.h"
#include "ExternalEditor.h"
#include "afxwin.h"
#include "ppt.h"

class CMscGenSrvrItem;
class CMscGenView;

/** Structure describing one piece of animated content.
 * It can be a tracking rectangle/shape, an element control (for
 * collapse/expand) or a contour of a fallback image on the embedded
 * object view or the compilation progress bar (covering the whole view).*/
struct AnimationElement {
    static constexpr unsigned TRACK_MODE_HOOVER_FADE_PERCENT = 20;
    ElementRef element; ///<If we are a tracking rect or control, this is the corresponding arc
    /** Types of animation elements */
    enum ElementType {
        CONTROL = 0,    ///<A collapse/expand control
        TRACKRECT,      ///<A tracking rectangle (or rather shape)
        FALLBACK_IMAGE, ///<A contour of fallback images
        COMPILING_GREY, ///<The compiling progress bar
        MAX_TYPE        ///<Maximum value for AnimationElement::ElementType
    } what;             ///<What sort of animation is this.
    static const unsigned default_appear_time[MAX_TYPE]; ///<Length of time for these animations to appear
    static const unsigned default_disapp_time[MAX_TYPE]; ///<Length of time for these animations to disappear
    /** States an animation can be in. */
	int    fade_delay;  ///<After appear this much long has to pass before we automatically start disappearing. If <0 we do not automatically start fading.
    int    appe_time;   ///<This much time left from appearing.
    int    disa_time;   ///<This much time left from disappearing.
	double fade_value;  ///<How much are we showing. 0 is not shown, 1 is fully shown.
    double target_fade; ///<The value we are fading towards
    bool   keep;        ///<When true, we keep this animation from fading
    /** Initialize as a tracking rectangle/shape or control*/
    AnimationElement(const ElementRef &a, ElementType et, int delay = -1,
                     int appear=-1, int disappear=-1);
    /** Initialize as a fallback image or compiling progress bar.*/
	AnimationElement(ElementType et, int delay = -1,
                     int appear=-1, int disappear=-1);
    /** Invert keep and set target fade accordingly. Return the new keep value.*/
    bool InvertKeep();
};

/** The main document object.
 * It actually handles rendering to a cache (in CChartDrawingData), as well.
 * This is because in case of split view, we have two CMscGenView objects, each one
 * having the same zoom, cached rendered content, tracking animations. Those common
 * data is then stored/processed in this document object.*/
class CMscGenDoc : public COleServerDocEx
{
public:
    static constexpr int FILE_NO_FOR_CSH_ERRORS = 1654892; ///<A magic number for a file to indicate a CSH error
protected:
	CMscGenDoc(); ///<Protected constructor: create from serialization only
	DECLARE_DYNCREATE(CMscGenDoc)
	DECLARE_MESSAGE_MAP()
	virtual COleServerItem* OnGetEmbeddedItem();
    virtual COleServerItem* OnGetLinkedItem(LPCTSTR lpszItemName);
	virtual CDocObjectServer* GetDocObjectServer(LPOLEDOCUMENTSITE pDocSite);
    AnimationElement* _AddAnimationElement(AnimationElement::ElementType type, ElementRef element = {},
        double fade_target = 1., double starting_fade = 0., int delay = -1); //Add a tracking element to the list. Returns the added (or refreshed element)

    void UpdateInternalEditor();
    bool IsInFullScreenMode() const;

public:
    virtual ~CMscGenDoc();

    // Attributes
public:
    /** @name Actual Signalling Chart Data and various pointers
     * The undo buffer is essentially a list of CChartData objects (each encapsulating full chart text and attributes)
     * We also store two compiled/rendered charts (CDrawingChartData, encapsulating an Msc object), one that is
     * currently shown and another that is being compiled.
    * @{ */
	std::list<CChartData> m_charts;     ///<The undo buffer containing chart texts
	IChartData m_itrSaved;              ///<The chart that is last saved. This iterator may be invalid (==m_charts.end()), use only for comparison
	IChartData m_itrEditing;            ///<The chart that is the current one in the internal or external editor
	IChartData m_itrShown;              ///<The chart that is compiled and shown in view. Iterator may be invalid (==m_charts.end()) if user undone the shown chart.
	CDrawingChartData m_ChartShown;     ///<The chart that is currently showing, including a cached rendering of it.
    CDrawingChartData m_ChartCompiling; ///<The chart that is currently compiling/rendering.
    //@}
    /** @name Misc data for embedded objects
    * @{ */
    int      m_page_serialized_in;       ///<The page read by last Serialize() - we store it since we have no other way to get it out of Serialize()
    unsigned m_uSavedFallbackResolution; ///<The fallback resolution saved for embedded charts.  Used to check if it changed and we need to save.
    bool     m_bSavedPageBreaks;         ///<The page break indicator flag saved for embedded charts.  Used to check if it changed and we need to save.
    unsigned m_uSavedPage;               ///<The page we have embedded. Used to check if it changed and we need to save.
    size_t   serialize_doc_overhead;     ///<For embedded objects this stores the calculated serialization overhead (so we can display Object Size correctly)
    //@}
    /** @name Compilation related
     * We may do compilation in a separate thread. The separate thread works on m_ChartCompiling.
     * We have a lock here to protect that, a handle to a process and ways to communicate with it.
    * @{ */
    CCriticalSection m_SectionCompiling;   ///<A critical section to protect m_ChartCompiling, during compilation
    std::atomic_long m_compilation_instance = 0;   ///<Each compilation event is numbered, to match successful compilations with messages sent.
    CWinThread *m_pCompilingThread;        ///<The handle of the compilation thread
    const ProgressBase *m_Progress;        ///<The progress object of the compilation. Null if not compiling or no progress support.
    bool m_killCompilation;                ///<The main thread may set this causing the compilation thread to abort
    HWND m_hMainWndToSignalCompilationEnd; ///<If the compilation thread succeeds, it sends a WM_APP+293 message to this window (should be our main window)
    bool m_hadArrangeViews;                ///<If we shall arrange views after compilation
    bool m_highlight_fallback_images;      ///<if we shall highlight fallback image loc after compilation
    //@}
    /** @name Zoom related stuff
     * Since we do actual rendering in this object, we must have zoom related functions here.
     * All of the zoom values here are in pixels, so 100 means an exact match to the screen
     * resolution pixel to pixel.
     * To the user we dont show these values, but ones that are adjusted by the screen DPI resolution.
     * This is so that on high DPI screens we are not showing enormous zoom. Also,
     * We do not write these to the registry, but the ones we show to the user.
     * @{ */
    unsigned m_zoom;     ///<Current zoom in percentage.
    unsigned m_maxZoom;  ///<Maximum Zoom possible
    unsigned m_maxAutoZoom; ///<Maximum zoom for the "Fit to Width" and "Overview" modes
    unsigned m_minZoom;  ///<Minimum Zoom possible
    /** What zoom modes we may have.
     * A zoom mode is an automatic zoom setting applied after compilation.*/
	enum class EZoomMode {
        NONE=0,         ///<No automatic zoom setting after compilation - keep what zoom we had before.
        OVERVIEW,       ///<Zoom such that the whole chart (or its current page) fits the view.
        ORIGSIZE,       ///<Zoom back to 100%
        ZOOM_FITTOWIDTH ///<Zoom such that the whole chart (or its current page) fits the *width* view.
    } m_ZoomMode; ///<What is the current zoom mode selected by the user.
    //@}
    /** @name Animations and Tracking mode related members
    * @{ */
    bool m_bTrackMode;                           ///<True if tracking mode is on - if the mouse is tracked over arcs
    Contour m_fallback_image_location;           ///<The location of the fallback images extracted during rendering to an EMF cache
	std::vector<AnimationElement> m_animations;  ///<The currently visible list of animations
    std::map<ElementRef, Block, std::less<>> m_controlsShowing;///<Controls currently appearing. The Block value is meant in the coordinate space of the chart of the element (which may be inlined)
	CHARRANGE m_saved_charrange;                 ///<The character range we had before the tracking mode started - this is re-applied in those moments when we do not point to any element during tracking mode
	Element *m_last_arc;                         ///<During tracking the arc highlighted in the editor
	CMscGenView *m_pViewFadingTimer;             ///<Our view in which track rectangles show
	//@}
	static CLIPFORMAT m_cfPrivate;    ///<Clipboard format for chart objects
	CExternalEditor m_ExternalEditor; ///<A hidden window to do external editor functions
	bool m_bAttemptingToClose;        ///<This flag indicates to compilation functions if we are closing, set during CanCloseFrame()
    bool m_unicode;                   ///<True if the chart was loaded from a unicode file
    std::string m_suggested_lang;     ///<The language selected by the user in the File Open dialog. (Used if the extension is not recognized.)
    CTime m_ModTimeOnDisk;            ///<The last modification time of the file on disk (valid only if GetPathName() returns a nonempty filename.)
    bool  m_ModTimeOnDiskValid;       ///<True if we should pay attention to m_ModTimeOnDisk.
    using steady_clock = std::chrono::system_clock;
    steady_clock::time_point m_last_change;       ///<When the last change was made to the chart (text)
    steady_clock::time_point m_reset_status_bar;  ///<When the status bar message needs to be deleted

    /** What is open */
    enum class ChartSource {
        File,      ///<A regular file is open (or Unnamed or an OLE embedded object)
        Clipboard, ///<The content of the clipboard is open
    };
    ChartSource m_what_is_open = ChartSource::File; ///<What is open currently, a file or clipboard content?
    ClipboardSeen m_clipboard_seen;   ///<If charts were available on the clipboard, this holds their content. User may decide to open them or just drop them.
    PptClipboard m_clipboard_content; ///<If a chart were opened from the clipboard, this holds the original clipboard content.
    size_t m_clipboard_chart_used;    ///<If a chart were opened from the clipboard, this holds the index of that chart in clipboard content


public:
	virtual void Serialize(CArchive& ar);
            void SerializePage(CArchive& ar, unsigned &page);
            void SerializeInShapes(CArchive &ar, bool message);
            void SerializeInFormat0(CArchive &ar, CChartData &chart, const CStringA &design);
            void SerializeInFormat1(CArchive &ar, CChartData &chart, unsigned &page);
    virtual void DeleteContents(); // Destroys existing data, updates views
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	virtual BOOL OnNewDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void OnCloseDocument();
    virtual void OnNewEmbedding(LPSTORAGE lpStorage) override;
    virtual BOOL DoSave(LPCTSTR lpszPathName, BOOL bReplace = TRUE) override;
    virtual void OnIdle() override;

    afx_msg void OnUpdateFileExport(CCmdUI* pCmdUI);
    afx_msg void OnFileExport();
            void DoExport(bool pdfOnly);
    afx_msg void OnPreviewExport();
    afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditRedo(CCmdUI *pCmdUI);
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditCutCopy(CCmdUI *pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnEditFind();
	afx_msg void OnEditRepeat();
	afx_msg void OnEditReplace();
    afx_msg void OnEditCopyEntireChart();
    afx_msg void OnEditCopyEntireChartAltText();
	afx_msg void OnUpdateEditPasteEntireChart(CCmdUI *pCmdUI);
			void DoPasteData(COleDataObject &dataObject);
	afx_msg void OnEditPasteEntireChart();
	afx_msg void OnUpdateButtonEdittext(CCmdUI *pCmdUI);
	afx_msg void OnButtonEdittext();          //External editor button
	afx_msg void OnUdpateEditUpdate(CCmdUI *pCmdUI);
	afx_msg void OnEditUpdate();              //update chart from internal editor
    afx_msg void OnButtonTrack();             //Tracking mode button
    afx_msg void OnUpdateButtonTrack(CCmdUI *pCmdUI);
    afx_msg void OnTrackHere();            //User selected from the internal editor (or pressed F4) - track what is at the mouse (saved by the CEditorBar into m_ctxMenuPos);
    afx_msg void OnUpdateTrackHere(CCmdUI *pCmdUI);
    afx_msg void OnButtonCollapseAll();
    afx_msg void OnButtonExpandAll();
    void DoViewNexterror(bool next, bool small_step);  //This does the stepping
	afx_msg void OnViewNexterror();           //User advances error
	afx_msg void OnViewPreverror();           //User steps back in errors
    afx_msg void OnViewNexterrorSmall();      //User advances error in small steps
    afx_msg void OnViewPreverrorSmall();      //User steps back in errors in small steps
    afx_msg void OnEditSelectAll();

	void ChangeDesign(const char *design);
    void ChangePage(unsigned page);
    void StepPage(signed int step);

	//Zoom functions
			bool SetZoom(int zoom=0);                        //Update views with new zoom factor. ==0 means just reset toolbar, Returns true if we have actually changed zoom .
			bool ArrangeViews(EZoomMode mode);               //Automatically adjust zoom. Returns true if we have done something.
			bool ArrangeViews() {return ArrangeViews(m_ZoomMode);}  ///<Automatically adjust zoom using existng zoom mode (if any) Returns true if we have done something.
	afx_msg void OnViewZoomnormalize();                      //Automatically adjust zoom: overview mode
	afx_msg void OnView100Percent();                         //Automatically adjust zoom: set to 100%
	afx_msg void OnViewFittowidth();                         //Automatically adjust zoom: set width to window
    afx_msg void OnUpdateViewZoom(CCmdUI *pCmdUI);
            void SwitchZoomMode(EZoomMode mode);             //Set zoom mode to specified value (and adjust zoom)
	afx_msg void OnZoommodeKeepinoverview();                 //Set zoom mode
	afx_msg void OnZoommodeKeep100Percent();       //Set zoom mode
	afx_msg void OnZoommodeKeepfittingtowidth();             //Set zoom mode
	afx_msg void OnUpdateZoommodeKeepinoverview(CCmdUI *pCmdUI);
    afx_msg void OnZoommodeKeep100Percent(CCmdUI *pCmdUI);
	afx_msg void OnUpdateZoommodeKeepfittingtowidth(CCmdUI *pCmdUI);

	        bool DispatchMouseWheel(UINT nFlags, short zDelta, CPoint pt);

public:
	void InsertNewChart(CChartData &&);                  //insert a new chart into the list (destroys redo, updates iterators)
	bool CheckIfChanged();                               //Check if we have changed and updates SetModifiedFlag
    void SetModifiedFlag(BOOL bModified) override;
    void OnExternalEditorChange(CChartData &&data);      //this is called by m_ExternalEditor if the text in the external editor changes
    void OnInternalEditorChange(long start, long ins, long del, CHARRANGE sel_before, POINT scroll_pos_before);//this is called by CMiniEditor if the text in the internal editor changes
    void OnInternalEditorSelChange();                    //this is called by CMiniEditor if the selection in the internal editor changes
	void CompileEditingChart(bool resetZoom, bool force_block, bool force_compile);//Call this to show the currently edited chart, it compiles and updates the views, calls NotifyChanged(), which updates the container object for OLE embedded charts, but does not save
    void CompleteCompilingEditingChart(long seq);        //This will be called once the compilation is complete
    void KillCompilation();                              //Abort the compilation
    void WaitForCompilationToEnd();                      //Wait till compilation ends
    void RecoverAutoSavedFile(const CString &filename);  //When a user selects an autosaved file for recovery
    void SetStatusMessage(LPCTSTR msg, int timer_sec=5, COLORREF color=RGB(0,0,0));

	void StartFadingTimer();                             //Ensure that one and only one View runs a fading timer;
	bool DoFading();                                     //Do one step fading. Return true if there are still elements in the process of fading
    /** Add a tracking element to the list. Returns the added (or refreshed element)*/
    AnimationElement* AddAnimationTrackRect(ElementRef element,
        double fade_target = 1., double starting_fade = 0., int delay = -1)
        {return _AddAnimationElement(AnimationElement::TRACKRECT, element, fade_target, starting_fade, delay); }
    /** Add a control animation to the list. Returns the added (or refreshed element)*/
    AnimationElement* AddAnimationControl(ElementRef element)
        {return _AddAnimationElement(AnimationElement::CONTROL, element); }
    /** Add the compiling animation to the animation list. Returns the added (or refreshed element)*/
    AnimationElement* AddAnimationCompilingGray()
        {return _AddAnimationElement(AnimationElement::COMPILING_GREY); }
    /** Add the flash animation for fallback images to the animation list. Returns the added (or refreshed element)*/
    AnimationElement* AddAnimationFallbackImage()
        {return _AddAnimationElement(AnimationElement::FALLBACK_IMAGE, {}, false, 1, 0); }
    void StartTrackFallbackImageLocations(const Contour &);
    void StartFadingAll(AnimationElement::ElementType type=AnimationElement::TRACKRECT, const Element *except=nullptr); //Start the fading process for all rectangles (even for delay<0, except one)
    bool HasAnimationWithKeep();
    void ResetAllAnimationKeep();
    void SetTrackMode(bool on);                                      //Turns tracking mode on
    const ISMapElement *UpdateTrackRects(CPoint mouse, bool silent); //updates tracking rectangles depending on the mouse position (position is in MscDrawer coord space)
	void HighLightArc(const ElementRef &element, bool silent);       //Select in internal editor
    bool OnControlClicked(const ElementRef &element, EGUIControlType t);      //Do what is needed if a control is clicked. Ture if chart invalidated.
    void OnActivateEmbeddedRibbonCategory(bool embedded);
    void ReDrawEMF();

#ifdef SHARED_HANDLERS
    virtual void InitializeSearchContent();
    virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS
protected:
#ifdef SHARED_HANDLERS
    // Helper function that sets search content for a Search Handler
    void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS

};


#ifndef SHARED_HANDLERS
CMscGenDoc *GetMscGenDocument();
#endif
