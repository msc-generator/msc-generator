/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file MainFrm.cpp The main window classes.
* @ingroup Msc_generator_files */

#include "stdafx.h"
#include "Msc-generator.h"

#include "MainFrm.h"
#include "MscGenDoc.h"
#include "MscGenView.h"

#include "gvgraphs.h" //for GetLayoutMethods

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/** We do the same as the stock implementation, but that seem to fail sometimes
 * in mysterious ways...*/
BOOL CMscGenSplitterWnd::SplitRow(int cyBefore)
{
    ASSERT_VALID(this);
    ASSERT(GetStyle() & SPLS_DYNAMIC_SPLIT);
    ASSERT(m_pDynamicViewClass != nullptr);
    ASSERT(m_nRows < m_nMaxRows);

    cyBefore -= m_cyBorder;
    int rowNew = m_nRows;
    int cyNew = m_pRowInfo[rowNew-1].nCurSize - cyBefore - m_cySplitter;
    if (cyNew <  m_pRowInfo[rowNew-1].nMinSize) {
        TRACE(traceAppMsg, 0, "Warning: split too small to shrink old pane.\n");
        return FALSE;
    }
    if (cyBefore <  m_pRowInfo[rowNew].nMinSize) {
        TRACE(traceAppMsg, 0, "Warning: split too small to create new pane.\n");
        return FALSE;
    }

    // create the scroll bar first (so new views can see that it is there)
    if (m_bHasVScroll &&
        !CreateScrollBarCtrl(SBS_VERT, AFX_IDW_VSCROLL_FIRST + rowNew)) {
        TRACE(traceAppMsg, 0, "Warning: SplitRow failed to create scroll bar.\n");
        return FALSE;
    }

    m_nRows++;  // bump count during view creation

    // create new views to fill the new row (RecalcLayout will position)
    for (int col = 0; col < m_nCols; col++) {
        CSize size(m_pColInfo[col].nCurSize, cyNew);
        if (!CreateView(rowNew, col, m_pDynamicViewClass, size, nullptr)) {
            TRACE(traceAppMsg, 0, "Warning: SplitRow failed to create new row.\n");
            // delete anything we partially created 'col' = # columns created
            while (col > 0)
                DeleteView(rowNew, --col);
            if (m_bHasVScroll) {
                auto pScr = GetDlgItem(AFX_IDW_VSCROLL_FIRST + rowNew);
                if (pScr) pScr->DestroyWindow();
            }
            m_nRows--;      // it didn't work out
            return FALSE;
        }
    }

    // new parts created - resize and re-layout
    m_pRowInfo[rowNew-1].nIdealSize = cyBefore;
    m_pRowInfo[rowNew].nIdealSize = cyNew;
    ASSERT(m_nRows == rowNew+1);
    RecalcLayout();

    return TRUE;
}

/** If the user deletes a row, kill AutoSplit, as well.*/
void CMscGenSplitterWnd::DeleteRow(int rowDelete)
{
    CMainFrame *pWnd = dynamic_cast<CMainFrame *>(GetParent());
    if (pWnd)
        pWnd->m_bAutoSplit = false;
    CSplitterWnd::DeleteRow(rowDelete);
}

/** After we have changed the text of the status bar, recalc its pane sizes.
 * @param nIndex The index of the pane to recalc. If -1, recalc all indexes.*/
void CMscGenStatusBar::RecalcPaneSizes(int nIndex)
{
    if (nIndex>=m_nCount)
        return;
    HFONT hFont = GetCurrentFont();
    ENSURE(hFont != nullptr);        // must have a font !
    CString strText;
    CClientDC dcScreen(nullptr);
    HGDIOBJ hOldFont = dcScreen.SelectObject(hFont);

    int start = nIndex<0 ? 0 : nIndex;
    int end = nIndex<0 ? m_nCount : nIndex+1;

    for (int i = start; i < end; i++) {
        CMFCStatusBarPaneInfo* pSBP = ((CMFCStatusBarPaneInfo*)m_pData) + i;
        if (pSBP == nullptr) {
            ASSERT(0);
            break;
        }
        pSBP->cxText = dcScreen.GetTextExtent(pSBP->lpszText, _tcslen(pSBP->lpszText)).cx;
        ASSERT(pSBP->cxText >= 0);
    }
    dcScreen.SelectObject(hOldFont);
    RecalcLayout();
    Invalidate();
}

BOOL CMscGenStatusBar::SetPaneTextAndRecalc(int nIndex, LPCTSTR lpszNewText)
{
    BOOL ret = SetPaneText(nIndex, lpszNewText, FALSE);
    if (ret)
        RecalcPaneSizes(nIndex);
    return ret;
}


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

/** Maxiumum number of toolbars we use (obsolete now)*/
const int  iMaxUserToolbars = 10;
/** First toolbar we use. (obsolete now)*/
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
/** Last toolbar we use. (obsolete now)*/
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;


//#define ID_VIEW_APPLOOK_WIN_2000        210
//#define ID_VIEW_APPLOOK_OFF_XP          211
//#define ID_VIEW_APPLOOK_WIN_XP          212
//#define ID_VIEW_APPLOOK_OFF_2003        213
//#define ID_VIEW_APPLOOK_VS_2005         214
//#define ID_VIEW_APPLOOK_OFF_2007_BLUE   215
//#define ID_VIEW_APPLOOK_OFF_2007_BLACK  216
//#define ID_VIEW_APPLOOK_OFF_2007_SILVER 217
//#define ID_VIEW_APPLOOK_OFF_2007_AQUA   218
//#define ID_VIEW_APPLOOK_VS_2008         219
//#define ID_VIEW_APPLOOK_WINDOWS_7       220

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	ON_WM_CREATE()
    ON_WM_TIMER()
    ON_COMMAND(ID_VIEW_FULL_SCREEN, OnViewFullScreen)
	ON_COMMAND(ID_BUTTON_AUTO_SPLIT, OnButtonAutoSplit)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_AUTO_SPLIT, OnUpdateButtonAutoSplit)
//	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
    ON_WM_ACTIVATE()
	ON_COMMAND(ID_DESIGN_ZOOM, OnDesignZoom)
    ON_CBN_SELENDOK(ID_DESIGN_ZOOM, OnDesignZoom)
    ON_COMMAND(ID_DESIGN_PAGE, OnDesignPage)
	ON_COMMAND(ID_FULL_SCREEN_PAGE, OnFullScreenPage)
	ON_CBN_SELENDOK(ID_DESIGN_PAGE, OnDesignPage)
	ON_UPDATE_COMMAND_UI(ID_DESIGN_PAGE, OnUpdateDesignPage)
	ON_COMMAND(ID_DESIGN_PAGE_FULL_SCREEN_NEXT, OnDesignNextPage)
	ON_COMMAND(ID_DESIGN_PAGE_FULL_SCREEN_PREV, OnDesignPrevPage)
	ON_COMMAND(ID_DESIGN_DESIGN, OnDesignDesign)
	ON_CBN_SELENDOK(ID_DESIGN_DESIGN, OnDesignDesign)
	ON_UPDATE_COMMAND_UI(ID_DESIGN_DESIGN, OnUpdateDesignDesign)
    ON_COMMAND(ID_DESIGN_LAYOUT, OnDesignLayout)
    ON_CBN_SELENDOK(ID_DESIGN_LAYOUT, OnDesignLayout)
    ON_UPDATE_COMMAND_UI(ID_DESIGN_LAYOUT, OnUpdateDesignLayout)
    ON_COMMAND(ID_VIEW_ZOOMIN, OnViewZoomin)
	ON_COMMAND(ID_VIEW_ZOOMOUT, OnViewZoomout)
	ON_COMMAND(ID_BUTTON_EDITINTERNAL, OnViewInternalEditor)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_EDITINTERNAL, OnUpdateViewInternalEditor)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_DEFAULT_TEXT, OnUpdateButtonDefaultText)
    ON_UPDATE_COMMAND_UI(ID_EMBEDDEDOPTIONS_FALLBACK_RES, OnUpdateEmbeddedoptionsFallbackRes)
    ON_MESSAGE(WM_APP+293, OnCompilationDone)
    ON_COMMAND(ID_STATUS_LANGUAGE, OnLanguageStatus)  //Track button handled by CMscGenDoc
    ON_UPDATE_COMMAND_UI(ID_STATUS_LANGUAGE, OnUpdateLanguageStatus)
    ON_COMMAND(ID_STATUS_UNICODE, OnUnicodeStatus)  //Track button handled by CMscGenDoc
    ON_UPDATE_COMMAND_UI(ID_STATUS_UNICODE, OnUpdateUnicodeStatus)
    ON_UPDATE_COMMAND_UI(ID_STATUS_LINE_COL, OnUpdateLineCol)
    ON_COMMAND(ID_EDIT_RE_CSH, &CMainFrame::OnEditReCsh)
    ON_UPDATE_COMMAND_UI(ID_EDIT_RE_CSH, &CMainFrame::OnUpdateEditReCsh)
    ON_COMMAND(ID_BUTTON_RE_INDENT, &CMainFrame::OnButtonReIndent)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_RE_INDENT, &CMainFrame::OnUpdateEditIndentTabsize)
    ON_COMMAND(ID_EDIT_INDENT_TABSIZE, &CMainFrame::OnEditIndentTabsize)
    ON_COMMAND(ID_EDIT_INDENT_TABSIZE_II, &CMainFrame::OnEditIndentTabsize_II)
    ON_COMMAND(ID_EDIT_INDENT_TABSIZE_IM, &CMainFrame::OnEditIndentTabsize_IM)
    ON_COMMAND(ID_EDIT_INDENT_TABSIZE_IA, &CMainFrame::OnEditIndentTabsize_IA)
    ON_COMMAND(ID_EDIT_INDENT_TABSIZE_IB, &CMainFrame::OnEditIndentTabsize_IB)
    ON_UPDATE_COMMAND_UI(ID_EDIT_INDENT_TABSIZE, &CMainFrame::OnUpdateEditIndentTabsize)
    ON_UPDATE_COMMAND_UI(ID_EDIT_INDENT_TABSIZE_II, &CMainFrame::OnUpdateEditIndentTabsize)
    ON_UPDATE_COMMAND_UI(ID_EDIT_INDENT_TABSIZE_IM, &CMainFrame::OnUpdateEditIndentTabsize)
    ON_UPDATE_COMMAND_UI(ID_EDIT_INDENT_TABSIZE_IA, &CMainFrame::OnUpdateEditIndentTabsize)
    ON_UPDATE_COMMAND_UI(ID_EDIT_INDENT_TABSIZE_IB, &CMainFrame::OnUpdateEditIndentTabsize)
    ON_COMMAND(ID_CHECK_INDENT_SPEC_LABEL, &CMainFrame::OnCheckIndentSpecLabel)
    ON_COMMAND(ID_CHECK_INDENT_SPEC_ATTR, &CMainFrame::OnCheckIndentSpecAttr)
    ON_UPDATE_COMMAND_UI(ID_CHECK_INDENT_SPEC_LABEL, &CMainFrame::OnUpdateCheckIndentSpecLabel)
    ON_UPDATE_COMMAND_UI(ID_CHECK_INDENT_SPEC_ATTR, &CMainFrame::OnUpdateCheckIndentSpecAttr)
    ON_COMMAND(ID_BUTTON_SELECT_FONT, &OnButtonSelectFont)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_SELECT_FONT, &OnUpdateButtonSelectFont)
    ON_UPDATE_COMMAND_UI(ID_BUTTON_EDITORS, &CMainFrame::OnUpdateButtonEditors)
//    ON_COMMAND(ID_ESCAPE, &CMainFrame::OnEscape)
END_MESSAGE_MAP()

/** The list of statusbar indicators */
static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
    ID_STATUS_LANGUAGE,
    ID_STATUS_LINE_COL,
    ID_STATUS_UNICODE,
	ID_INDICATOR_TRK,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2005);
    m_bAutoSplit = false;
    m_at_embedded_object_category = false;
    m_at_recovery_category = false;
    m_has_fallback_image = false;
    m_mscgen_mode_ind = -1;
}

/** Helper to set the text of a ribbon edit box to a value.*/
void CMainFrame::SetEditBox(UINT ctrlID, int value)
{
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ctrlID, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonEdit *e = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    _ASSERT(e);
    CString num;
    num.Format(_T("%d"), value);
    e->SetEditText(num);
}

/** Create the associated window object, ribbons, status bars, panels, etc.*/
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);

	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

    // set the visual manager and style based on persisted value
	OnApplicationLook(theApp.m_nAppLook);

    //Create ballon window
    m_wndBalloon.Create(this);
    CMFCToolTipInfo* params = new CMFCToolTipInfo();
    params->m_bBoldLabel = TRUE;
    params->m_bDrawDescription = TRUE;
    params->m_bDrawIcon = FALSE;
    params->m_bRoundedCorners = TRUE;
    params->m_bDrawSeparator = TRUE;
    params->m_clrFill = RGB(255, 255, 255);
    params->m_clrFillGradient = RGB(228, 228, 240);
    params->m_clrText = RGB(255, 0, 0); //RGB(61, 83, 80);
    params->m_clrBorder = RGB(144, 149, 168);
    m_wndBalloon.SetParams(params);
    //XXX dont we leak params here?


	m_wndRibbonBar.Create(this);
	m_wndRibbonBar.LoadFromResource(IDR_RIBBON);
    m_wndRibbonBar.ShowCategory(3); //for some reason we need to explicitly show this (this is the internal editor)
    m_wndRibbonBar.ShowCategory(4); //for some reason we need to explicitly show this (this is the autosave)
    //bool bb= m_wndRibbonBar.GetCategory(3)->GetPanel(2)->IsCollapsed();

    SetEditBox(ID_EDIT_AUTOSAVE_INTERVAL, pApp->GetDataRecoveryHandler()->GetAutosaveInterval()/1000);
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(0, arButtons);
    for (int i = 0; i<arButtons.GetSize(); i++)
        if (const LPCTSTR c = arButtons[i]->GetText(); !_tcscmp(c, _T("ObjectSize")))
            m_labelObjectSize = dynamic_cast<CMFCRibbonLabel*>(arButtons[i]);
        else if (!_tcscmp(c, _T("FallbackImage")))
            m_labelFallbackImage = dynamic_cast<CMFCRibbonLabel*>(arButtons[i]);
    _ASSERT(m_labelFallbackImage);
    _ASSERT(m_labelObjectSize);

    m_wndRibbonBar.ForceRecalcLayout();

	// prevent the menu bar from taking the focus on activation
	CMFCPopupMenu::SetForceMenuFocus(FALSE);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
    m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));
	m_wndStatusBar.EnablePaneDoubleClick();
    m_wndStatusBar.SetTipText(NUM_STATUS_BAR_TACKING, _T("Toggle Tracking Mode")); //index 2 is ID_INDICATOR_TRK
    m_wndStatusBar.SetTipText(NUM_STATUS_BAR_LANGUAGE, _T("Shows the language/type of the current chart."));
    m_wndStatusBar.SetTipText(NUM_STATUS_BAR_UNICODE, _T("Shows if the chart in the internal editor is loaded from a Unicode or UTF-8/ASCII file. Double click to change."));
    m_wndStatusBar.SetTipText(NUM_STATUS_BAR_LINE_COL, _T("Current cursor position (line:column)"));
    m_wndStatusBar.SetPaneTextAndRecalc(NUM_STATUS_BAR_LANGUAGE, _T("Chart Language"));
    m_wndStatusBar.SetPaneTextAndRecalc(NUM_STATUS_BAR_LINE_COL, _T("9999:999"));

	if (!m_wndOutputView.Create(_T("Errors and Warnings"), this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_OUTPUT,
		WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create output bar\n");
		return FALSE;      // fail to create
	}
	pApp->m_pWndOutputView = &m_wndOutputView;

	if (!m_ctrlEditor.Create(_T("Internal Editor"), this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_EDITOR,
		WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create output bar\n");
		return FALSE;      // fail to create
	}
	pApp->m_pWndEditor = &m_ctrlEditor;

    if (!m_wndAutoSaveFiles.Create(_T("Recovery"), this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_AUTOSAVEFILES,
        WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI)) {
        TRACE0("Failed to create files bar\n");
        return FALSE;      // fail to create
    }

	m_wndOutputView.EnableDocking(CBRS_ALIGN_ANY);
    m_ctrlEditor.EnableDocking(CBRS_ALIGN_ANY);
    m_wndAutoSaveFiles.EnableDocking(CBRS_ALIGN_ANY);
    EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndOutputView);
	DockPane(&m_ctrlEditor);
    DockPane(&m_wndAutoSaveFiles);

	// enable Visual Studio 2005 style docking window behavior
	CDockingManager::SetDockingMode(DT_SMART);
	// enable Visual Studio 2005 style docking window auto-hide behavior
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	if (m_ctrlEditor.IsVisible())
        m_ctrlEditor.SetFocus();

	EnableFullScreenMode(ID_VIEW_FULL_SCREEN);
	EnableFullScreenMainMenu(FALSE);


    //Set inital value of CSH scheme and hint filtering combo boxes
    arButtons.RemoveAll();
    m_wndRibbonBar.GetElementsByID(IDC_COMBO_CSH, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    _ASSERT(c);
    c->SelectItem(pApp->m_config.m_bShowCsh ? int(pApp->m_config.m_languages.cshParams->color_scheme)+1 : 0);

    arButtons.RemoveAll();
    m_wndRibbonBar.GetElementsByID(IDC_COMBO_HINT_FILTER, arButtons);
    //TODO: for some strange reason this returns 2 elements...
    for (unsigned i = 0; i<arButtons.GetSize(); i++)
        if (auto c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[i]))
            c->SelectItem(pApp->m_config.m_iHintFilter);

    //Set initial values for tab sizes
    SetEditBox(ID_EDIT_INDENT_TABSIZE, m_ctrlEditor.m_ctrlEditor.m_tabsize);
    SetEditBox(ID_EDIT_INDENT_TABSIZE_II, pApp->m_config.m_languages.cshParams->m_II);
    SetEditBox(ID_EDIT_INDENT_TABSIZE_IM, pApp->m_config.m_languages.cshParams->m_IM);
    SetEditBox(ID_EDIT_INDENT_TABSIZE_IA, pApp->m_config.m_languages.cshParams->m_IA);
    SetEditBox(ID_EDIT_INDENT_TABSIZE_IB, pApp->m_config.m_languages.cshParams->m_IB);

    //Set initial value for tracking color button
    m_wndRibbonBar.GetElementsByID(ID_BUTTON_TRACK_COLOR, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonColorButton *cb = dynamic_cast<CMFCRibbonColorButton*>(arButtons[0]);
    //Make it 50% lighter - as if 50% transparent on white background
    unsigned char r = GetRValue(pApp->m_config.m_trackFillColor);
    unsigned char g = GetGValue(pApp->m_config.m_trackFillColor);
    unsigned char b = GetBValue(pApp->m_config.m_trackFillColor);
    cb->SetColor(RGB(r, g, b));

    //Set initial values for thes embedded options
    m_wndRibbonBar.GetElementsByID(ID_EMBEDDEDOPTIONS_FALLBACK_RES, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonSlider *s = dynamic_cast<CMFCRibbonSlider *>(arButtons[0]);
    if (s) s->SetPos(pApp->m_config.m_uFallbackResolution);

    //Set mscgen compatibility stuff
    arButtons.RemoveAll();
    m_wndRibbonBar.GetElementsByID(ID_COMBO_MSCGEN_COMPAT, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    if (c)c->SelectItem(int(pApp->m_config.m_mscgen_compat));

    SetProgressBarState(TBPF_NOPROGRESS);
    SetProgressBarRange(0, 1000);

    SetLanguageOnRibbon();
    return 0;
}

/**Called by the framework at window creation.*/
BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	return m_wndSplitter.Create(this,
		2, 1,
		CSize(10, 10),
		pContext);
}

/** Called by the framework when the user changes application look.*/
void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

	switch (theApp.m_nAppLook)
	{
	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2008:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_WINDOWS_7:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(TRUE);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
	}

	RedrawWindow(nullptr, nullptr, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

/** Called by the framework to update the status of the application look selector.*/
void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}

/**Called by the framework to load the resources.
 * We use this to initially fill content of the design selector, zoom and page combo boxes.*/
BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext)
{
	// base class does the real work

	if (!CFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}
	// enable customization button for all user toolbars
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	for (int i = 0; i < iMaxUserToolbars; i ++)
	{
		CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
		if (pUserToolbar != nullptr)
		{
			pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
		}
	}

	//Actualize combo box values - THIS never runs Document is not initialized here
    //Instead do all this after compilation
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
	if (pDoc) {
        FillDesignComboBox(pDoc->m_itrEditing->GetDesign(), true, true);
        FillLayoutComboBox(pDoc->m_itrEditing->GetLayout(), true);
        FillPageComboBox(pDoc->m_ChartShown.GetPages(), pDoc->m_ChartShown.GetPage());
		FillZoomComboBox(pDoc->m_zoom); //merely fill in zoom combo with list of zoom values
	}

	return TRUE;
}

/**Called by the framework when the main window receives focus.
 * We use it to pass focus to the internal editor immediately.*/
void CMainFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    if (IsInternalEditorVisible())
		pApp->m_pWndEditor->m_ctrlEditor.SetFocus();
}

/**Called by the framework before processing any message.
 * We need to override it to capture ESCAPE key.
 * CFrameWndEx handles it in this message by shutting down full screen.
 * We do the same, but if we show an embedded object in fullscreen mode,
 * we have to exit the application and not just full screen mode.*/
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    //In case we get the escape keys and not the internal editor
    //(e.g., in fullscreen mode), we handle escaping tracking mode
    if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE) {
        DoEscapePressed();
		return TRUE;
	}
	return CFrameWndEx::PreTranslateMessage(pMsg);
}

/** Called by the framework when user switches to or from full screen mode.
 * If we show an OLE object via the "Show Full Screen" OLE verb and we are
 * already in full screen mode, we exit the application.
 * Else we turn full screen view on or off. In the former case we also
 * add buttons to the full screen toolbar, in the latter case we update the
 * Zoom combo box with the zoom value the user set during full screen view.*/
void CMainFrame::OnViewFullScreen()
{
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
	//if we show an embedded object in fullscreen mode, we have to exit now.
	if (pApp->m_bFullScreenViewMode && IsFullScreen()) {
        //Here we end a session opened by the Show in Full Screen OLE action
		//Hide, so user does not see as fullscreen is first restored
		ShowWindow(SW_HIDE);
		//Exit app.
		//Instead of CFrameWndEx::OnClose we call CFrameWnd::OnClose.
		//The former does the following before calling CFrameWnd::OnClose:
		//1. It shuts down print preview (ethere is none in full screen view mode)
		//2. it kills OLE container (we are not one, there is nothing to kill);
		//3. it restores Fullscreen (we do not want this: we just want the full screen to disapear)
		//4. saves window state by calling CWinAppEx::OnClosingMainFrame
		//5. saves window placement by calling CFrameImpl::StoreWindowPlacement.
		//The last two we dont want since a full screen windows has all the wrong state and placement
		CFrameWnd::OnClose();
	} else {
        CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
        if (!IsFullScreen()) {
            ShowFullScreen();
            AddToFullScreenToolbar();
            //Enter track mode
            if (pDoc)
                pDoc->SetTrackMode(true);
        } else {
            //If we are cancelling full screen update the zoom combo box with current value
			if (pDoc)
				pDoc->SetZoom();
            ShowFullScreen();
        }
    }
}

/** Message Handler to handle WM_APP+293 message, which signals that the compilation thread
 * has successfully completed. We just call CMscGenDoc::CompleteCompilingEditingChart() from here.*/
LRESULT CMainFrame::OnCompilationDone(WPARAM /*wParam*/, LPARAM seq)
{
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        pDoc->CompleteCompilingEditingChart(seq);
    return 0; //OK, we processed it
}

/**Adds extra buttons to the full screen toolbar.
 * Specifically, page switching, autosplit, fit to width and overview.*/
bool CMainFrame::AddToFullScreenToolbar() //finds the toolbar and adds our buttons to it
{
    CPaneFrameWnd *f = nullptr;
    CString strCaption;
    strCaption.LoadString(IDS_AFXBARRES_FULLSCREEN);
    for (CWnd *i = GetWindow(GW_HWNDFIRST); i; i = i->GetWindow(GW_HWNDNEXT)) {
        CString s;
        i->GetWindowText(s);
        if (s != strCaption) continue;
        if (!i->IsKindOf(RUNTIME_CLASS(CPaneFrameWnd))) continue;
        //OK, we found the right paneframewnd
        f = dynamic_cast<CPaneFrameWnd *>(i);
        break;
    }
    if (f==nullptr) return false;
    CMFCToolBar *p = nullptr;
    strCaption.LoadString(IDS_AFXBARRES_FULLSCREEN);
    for (CWnd *j = f->GetWindow(GW_CHILD); j; j = j->GetWindow(GW_HWNDNEXT)) {
        CString s;
        j->GetWindowText(s);
        if (s != strCaption) continue;
        if (!j->IsKindOf(RUNTIME_CLASS(CMFCToolBar))) continue;
        //OK, we found the toolbar in it
        p = dynamic_cast<CMFCToolBar *>(j);
        break;
    }
    if (!p) return false;
    p->InsertSeparator();
    CSize size = p->GetButtonSize();
    p->LoadBitmap(IDB_WRITESMALL);
    //Add autoSplit Button
    //In order to set the image of the button, we would need to copy that image from the
    //ribbon image collection to the toolbar image collection. I dont know how to do that yet.
    CMFCToolBarButton button(ID_BUTTON_TRACK, 11, _T("Tracking Mode"), false, false);
    button.m_bText = true;
    p->InsertButton(button);
    p->InsertSeparator();
    CMFCToolBarButton button2(ID_BUTTON_AUTO_SPLIT, 10, _T("AutoSplit"), true, true);
    button2.m_bText = true;
    p->InsertButton(button2);
    //Add page combo, if more than one page
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    if (pDoc && pApp && pDoc->m_ChartShown.GetPages()>1) {
        CMFCToolBarButton nextButton (ID_DESIGN_PAGE_FULL_SCREEN_NEXT, -1, _T("Next"));
        CMFCToolBarButton prevButton (ID_DESIGN_PAGE_FULL_SCREEN_PREV, -1, _T("Prev"));
        CMFCToolBarEditBoxButton edit (ID_FULL_SCREEN_PAGE, -1,
            ES_NUMBER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL | ES_RIGHT, 50);
        p->InsertSeparator();
        p->InsertButton(prevButton);
        p->InsertButton(edit);
        p->InsertButton(nextButton);
        FillPageComboBox(pDoc->m_ChartShown.GetPages(), pDoc->m_ChartShown.GetPage());
    }
    p->InsertSeparator();
    //buttonBitMap = CMFCToolBar::GetDefaultImage(ID_VIEW_ZOOMNORMALIZE);
    CMFCToolBarButton button3(ID_VIEW_ZOOMNORMALIZE, -1, _T("Overview"));
    p->InsertButton(button3);
    p->InsertSeparator();
    //buttonBitMap = CMFCToolBar::GetDefaultImage(ID_VIEW_FITTOWIDTH);
    CMFCToolBarButton button4(ID_VIEW_FITTOWIDTH, -1, _T("Fit to Width"));
    p->InsertButton(button4);

    p->AdjustLayout();

    //Arrange size
    p->StretchPane(32000, false);
    f->SizeToContent();

    //Arrange position
    CPoint distance(10, 10);

    CRect rectToolbarFrame, rcScreen;
    f->GetWindowRect(&rectToolbarFrame);
    GetActiveView()->GetWindowRect(&rcScreen);
    //Use this to position to bottom right corner
    distance = rcScreen.BottomRight() - distance - rectToolbarFrame.Size();

    f->SetWindowPos(nullptr, distance.x, distance.y, 0, 0,
        SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
    return true;
}

/** Helper to find a certain category in a ribbon.*/
CMFCRibbonCategory* GetRibbonCategory(CMFCRibbonBar *pR, const TCHAR *name)
{
    const int count = pR->GetCategoryCount();
    const CString n = name;
    for (int i = 0; i<count; i++) {
        CMFCRibbonCategory* p = pR->GetCategory(i);
        if (!n.CompareNoCase(p->GetName()))
            return p;
    }
    return nullptr;
}

/** Helper to find a certain panel in a ribbon category.*/
CMFCRibbonPanel *GetRibbonPanel(CMFCRibbonCategory*pRC, const TCHAR *name)
{
    if (pRC==nullptr)
        return nullptr;
    const int panelcount = pRC->GetPanelCount();
    const CString panelname = name ? name : _T("");
    for (int i = 0; i<panelcount; i++) {
        CMFCRibbonPanel *p = pRC->GetPanel(i);
        if (panelname.CompareNoCase(p->GetName())) continue;
        return p;
    }
    return nullptr;
}

/** Adds extra panes, edits and buttons to the print preview ribbon category.*/
void CMainFrame::AddToPrintPreviewCategory()
{
    bool changed = false;
    //find print preview category
    CMFCRibbonCategory* const pRC = GetRibbonCategory(&m_wndRibbonBar, _T("Print Preview"));
    if (!pRC || 3 < pRC->GetPanelCount()) {
        _ASSERT(0);
        return;
    }

    //Find the "Print" Panel and add PDF export
    CMFCRibbonPanel *pRP = GetRibbonPanel(pRC, _T("Print"));
    if (pRP)
        //Add button if it is not yet already added
        if (pRP->GetCount()== 0 || pRP->GetElement(pRP->GetCount()-1)==nullptr ||
            pRP->GetElement(pRP->GetCount()-1)->GetID() != ID_BUTTON_PREVIEW_EXPORT) {
            //Get Images for the file category
            CMFCRibbonCategory* pRCMain = m_wndRibbonBar.GetMainCategory();
            CMFCRibbonButton *pRB;
            if (pRCMain) {
                CMFCToolBarImages *pMainImageList = &pRCMain->GetLargeImages();
                //pImageList->AddIcon(theApp.LoadIcon(IDI_SOME_ICON), true);
                pRB = new CMFCRibbonButton(ID_BUTTON_PREVIEW_EXPORT, _T("Export to PDF"),
                    pMainImageList->ExtractIcon(9));
            } else
                pRB = new CMFCRibbonButton(ID_BUTTON_PREVIEW_EXPORT, _T("Export to PDF"));
            pRP->Add(pRB);
            changed = true;
        }

	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);

    //Find the "Pagination" to see if we need to add it
    pRP = GetRibbonPanel(pRC, _T("Pagination"));
    if (pRP == nullptr) {
        //Add new panel
        pRP = pRC->AddPanel(_T("Pagination"));
        pRP->SetCenterColumnVert();
        pRP->SetJustifyColumns();

        //Add panel content
        CMFCRibbonCheckBox *pRButt = new CMFCRibbonCheckBox(ID_AUTO_PAGINATE, _T("Auto Paginate"));
        pRP->Add(pRButt);
        pRButt = new CMFCRibbonCheckBox(ID_AUTO_HEADERS, _T("Auto Heading"));
        pRP->Add(pRButt);
        CMFCRibbonComboBox *pRCB = new CMFCRibbonComboBox(ID_COMBO_SCALE, true, 60, _T("Scale"));
        pRCB->EnableDropDownListResize();
        pRCB->AddItem(_T("25%"));
        pRCB->AddItem(_T("50%"));
        pRCB->AddItem(_T("75%"));
        pRCB->AddItem(_T("100%"));
        pRCB->AddItem(_T("125%"));
        pRCB->AddItem(_T("150%"));
        pRCB->AddItem(_T("Fit width"));
        pRCB->AddItem(_T("Fit page"));
        if (pApp->m_config.m_iScale4Pagination<=-2)
            pRCB->SelectItem(8);
        else if (pApp->m_config.m_iScale4Pagination==-1)
            pRCB->SelectItem(7);
        else {
            CString val;
            val.Format(_T("%d%%"), pApp->m_config.m_iScale4Pagination);
            pRCB->SetEditText(val);
        }
        pRP->Add(pRCB);

        pRP->AddSeparator();

        //pRCB = new CMFCRibbonComboBox(ID_COMBO_PAGES, false, 70, "Page Size");
        //pRCB->AddItem("none");
        //pRCB->AddItem("A0 portrait");
        //pRCB->AddItem("A0 landscape");
        //pRCB->AddItem("A1 portrait");
        //pRCB->AddItem("A1 landscape");
        //pRCB->AddItem("A2 portrait");
        //pRCB->AddItem("A2 landscape");
        //pRCB->AddItem("A3 portrait");
        //pRCB->AddItem("A3 landscape");
        //pRCB->AddItem("A4 portrait");
        //pRCB->AddItem("A4 landscape");
        //pRCB->AddItem("A5 portrait");
        //pRCB->AddItem("A5 landscape");
        //pRCB->AddItem("A6 portrait");
        //pRCB->AddItem("A6 landscape");
        //pRCB->AddItem("Letter portrait");
        //pRCB->AddItem("Letter landscape");
        //pRCB->AddItem("Legal portrait");
        //pRCB->AddItem("Legal landscape");
        //pRCB->AddItem("Ledger");
        //pRCB->AddItem("Tabloid");
        //pRCB->SelectItem(EPageSize(pApp->m_pageSize));
        //pRP->Add(pRCB);

        //CMFCRibbonEdit* pRE = new CMFCRibbonEdit(ID_EDIT_PAGES, 90, "Page size:");
        //pRE->SetEditText(ConvertPageSizeVerbose(pApp->m_pageSize));
        //pRP->Add(pRE);
        CMFCRibbonLabel *pRL = new CMFCRibbonLabel(_T("Page size:"));
        pRP->Add(pRL);

        pRL = new CMFCRibbonLabel(_T("(Use Print Setup to change.)"));
        pRP->Add(pRL);
        //CMFCRibbonButton *pRB = new CMFCRibbonButton(ID_BUTTON_PAGES, "Page setup");
        //pRP->Add(pRB);

        pRCB = new CMFCRibbonComboBox(ID_COMBO_ALIGNMENT, false, 90, _T("Alignment:"));
        pRCB->EnableDropDownListResize();
        pRCB->AddItem(_T("Top - Left"));
        pRCB->AddItem(_T("Top - Center"));
        pRCB->AddItem(_T("Top - Right"));
        pRCB->AddItem(_T("Middle - Left"));
        pRCB->AddItem(_T("Middle - Center"));
        pRCB->AddItem(_T("Middle - Right"));
        pRCB->AddItem(_T("Bottom - Left"));
        pRCB->AddItem(_T("Bottom - Center"));
        pRCB->AddItem(_T("Bottom - Right"));
        pRCB->SelectItem(pApp->m_config.m_iPageAlignment + 4);
        pRP->Add(pRCB);

        pRP->AddSeparator();
        pRP->Add(new CMFCRibbonLabel(_T("Margins (cm)")));

        CMFCRibbonEdit* pRE = new CMFCRibbonEdit(ID_EDIT_MARGIN_L, 35, _T("Left"));
        CString val;
        val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[0]/PT_PER_CM);
        pRE->SetEditText(val);
        pRP->Add(pRE);

        pRE = new CMFCRibbonEdit(ID_EDIT_MARGIN_R, 35, _T("Right"));
        val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[1]/PT_PER_CM); //cm to points, see http://www.asknumbers.com/CentimetersToPointsConversion.aspx
        pRE->SetEditText(val);
        pRP->Add(pRE);

        pRP->Add(new CMFCRibbonLabel(_T(":")));

        pRE = new CMFCRibbonEdit(ID_EDIT_MARGIN_T, 35, _T("Top"));
        val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[2]/PT_PER_CM); //cm to points, see http://www.asknumbers.com/CentimetersToPointsConversion.aspx
        pRE->SetEditText(val);
        pRP->Add(pRE);

        pRE = new CMFCRibbonEdit(ID_EDIT_MARGIN_B, 35, _T("Bottom"));
        val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[3]/PT_PER_CM); //cm to points, see http://www.asknumbers.com/CentimetersToPointsConversion.aspx
        pRE->SetEditText(val);
        pRP->Add(pRE);
        changed = true;
    }
    if (changed)
        m_wndRibbonBar.RecalcLayout();
}


/**Deletes our extra panes, edits and buttons from the preview category.
 * This is needed, or else the content of the edits show over the other categories due to
 * some mistery even if it is not the preview category that is selected.*/
void CMainFrame::DeleteFromPrintPreviewCategory()
{
    bool changed = false;
    CMFCRibbonCategory* const pRC = GetRibbonCategory(&m_wndRibbonBar, _T("Print Preview"));
    if (!pRC) return;

    //Find the "Print" Panel and add PDF export
    CMFCRibbonPanel *pRP = GetRibbonPanel(pRC, _T("Print"));
    if (pRP) {
            CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arElements;
            pRP->GetElementsByID(ID_BUTTON_PREVIEW_EXPORT, arElements);
            for (int u = 0; u<arElements.GetSize(); u++)
                if (pRP->GetIndex(arElements[u])>=0) {
                    pRP->Remove(pRP->GetIndex(arElements[u]));
                    changed = true;
                }
    }
    pRP = GetRibbonPanel(pRC, _T("Pagination"));
    if (pRP) {
        pRC->RemovePanel(pRC->GetPanelIndex(pRP));
        changed = true;
    }
    if (changed)
        m_wndRibbonBar.RecalcLayout();
}


/** A new ribbon panel class that can change its name. */
class CMyMFCRibbonPanel : public CMFCRibbonPanel
{
public:
    /** Use this to expose CleanUpSizes().*/
    void AA() { CleanUpSizes(); }
    /** Set the name of the panel. */
    void SetName(const CString &name) { m_strName = name; }
};


/** Adds extra panes, edits and buttons to the print preview ribbon category.*/
void CMainFrame::SetLanguageOnRibbon()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    CMFCRibbonCategory* const pRC = GetRibbonCategory(&m_wndRibbonBar, _T("Home"));
    if (!pRC) {
        _ASSERT(0);
        return;
    }

    CMFCRibbonPanel *pRP = pRC->GetPanel(0);
    if (!pRP) return;
    pRP->RemoveAll();
    pRP->SetJustifyColumns();
    pRP->Add(new CMFCRibbonButton(ID_EDIT_UPDATE, _T("Update Chart"), -1, 5));
    if (pApp->m_config.m_examples->HasExamplesFor(pApp->m_config.m_lang))
        pRP->Add(new CMFCRibbonButton(ID_HELP_EXAMPLES, _T("Show Examples"), -1, 1));
    pRP->Add(new CMFCRibbonButton(ID_BUTTON_TRACK, _T("Tracking Mode"), 11, -1));
    pRP->Add(new CMFCRibbonButton(ID_BUTTON_AUTO_SPLIT, _T("Auto Split"), 10, -1));
    pRP->Add(new CMFCRibbonButton(ID_VIEW_SHOWELEMENTCONTROLS, _T("Element Controls"), 9, -1));
    bool onlyFullDesigns;
    if (pApp->m_config.m_lang->GetName()== "signalling") {
        pRP->AddSeparator();
        pRP->Add(new CMFCRibbonLabel(_T("Design")));
        pRP->Add(new CMFCRibbonComboBox(ID_DESIGN_DESIGN, FALSE));
        auto pRB = new CMFCRibbonEdit(ID_DESIGN_PAGE, 60, _T("Page"));
        pRB->EnableSpinButtons(0, 100);
        pRP->Add(pRB);
        onlyFullDesigns = true;
    } else if (pApp->m_config.m_lang->GetName() == "gv") {
        pRP->AddSeparator();
        pRP->Add(new CMFCRibbonComboBox(ID_DESIGN_LAYOUT, FALSE, 70, _T("Layout")));
        pRP->Add(new CMFCRibbonComboBox(ID_DESIGN_DESIGN, FALSE, 70, _T("Design")));
        auto pRB = new CMFCRibbonEdit(ID_DESIGN_PAGE, 70, _T("Page"));
        pRB->EnableSpinButtons(0, 100);
        pRP->Add(pRB);
        pRP->AddSeparator();
        pRP->Add(new CMFCRibbonButton(ID_BUTTON_COLLAPSE_ALL, _T("Collapse all")));
        pRP->Add(new CMFCRibbonButton(ID_BUTTON_EXPAND_ALL, _T("Expand all")));
#ifdef _DEBUG
        pRP->Add(new CMFCRibbonCheckBox(ID_BUTTON_ORIGINAL_ENGINE, _T("Original engine\n(debug only)")));
#endif
        onlyFullDesigns = false;
    } else if (pApp->m_config.m_lang->GetName() == "xxx") {
        onlyFullDesigns = false;
    } else if (pApp->m_config.m_lang->GetName() == "flow") {
        onlyFullDesigns = false;
    } else if (pApp->m_config.m_lang->GetName() == "block") {
        onlyFullDesigns = false;
    } else {
        _ASSERT(0); //unknown language
        onlyFullDesigns = false;
    }
    FillDesignComboBox(nullptr, true, onlyFullDesigns);
    CClientDC dc(this);
    reinterpret_cast<CMyMFCRibbonPanel*>(pRP)->AA();
    reinterpret_cast<CMyMFCRibbonPanel*>(pRP)->SetName(AsUnicode(pApp->m_config.m_lang->description));
    m_wndRibbonBar.RecalcLayout();
}

/** Activates the Home category on the Ribbon*/
void CMainFrame::ActivateHomeCategory()
{
    m_wndRibbonBar.SetActiveCategory(m_wndRibbonBar.GetCategory(1));
}

/** Handles an escape key being pressed.
 * When in tracking mode, we deselect all highlighted elements,
 * if no elements selected, we exit tracking mode.
 * If not in tracking mode, but in Full Screen, we exit that.*/
void CMainFrame::DoEscapePressed()
{
    CMscGenDoc *pDoc = GetMscGenDocument();
    //If we are in tracking mode...
    if (pDoc->m_bTrackMode) {
        //...and has animations..
        if (pDoc->HasAnimationWithKeep()) {
            //..we remove them
            pDoc->ResetAllAnimationKeep();
            pDoc->StartFadingAll();
        } else {
            //If no animations, we exit tracking mode
            pDoc->SetTrackMode(false);
        }
    } else {
        //if not in track mode...
        if (IsFullScreen())
            //..but in fullscreen, we kill it...
            //CFrameWndEx::PreTranslateMessage first checks it against a menu
            //But we have no menus in full screen mode
            CMainFrame::OnViewFullScreen();
        else
            //..else just switch to the home category
            ActivateHomeCategory();
    }
}

/** Called by the frameowrk at activation.
 * We take the opportunity to shift the focus to the internal editor.*/
void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
    CFrameWndEx::OnActivate(nState, pWndOther, bMinimized);

    //Set focus to internal editor
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    if (nState != WA_INACTIVE)
        if (IsInternalEditorVisible())
            pApp->m_pWndEditor->SetFocus();
}

/**Called by the framework when user presses the AutoSplit button.*/
void CMainFrame::OnButtonAutoSplit()
{
    if (!m_bAutoSplit) {
        CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
        m_bAutoSplit = true;
        if (pDoc)
            SetSplitSize(unsigned(pDoc->m_ChartShown.GetHeadingSize()*pDoc->m_zoom/100.));
    } else {
        //turn it off
        switch (m_wndSplitter.GetRowCount()) {
        default: _ASSERT(0); break;
        case 1: break;
        case 2:
            int Cur, Min;
            m_wndSplitter.GetRowInfo(0, Cur, Min);
            m_wndSplitter.DeleteRow(0);
            CMscGenView *pView = dynamic_cast<CMscGenView *>(m_wndSplitter.GetPane(0,0));
            if (pView) {
		        CPoint pos = pView->GetScrollPosition();
                pos.y -= Cur;
                if (pos.y<0) pos.y=0;
		        pView->ScrollToPosition(pos);
            }
            break;
        }
        m_bAutoSplit = false;
    }
}

/**Called by the framework to get the status of the AutoSplit button.*/
void CMainFrame::OnUpdateButtonAutoSplit(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->SetCheck(m_bAutoSplit && pApp->m_config.m_lang->has_autoheading);
    pCmdUI->Enable(pApp->m_config.m_lang->has_autoheading);
}




/** Switches to split view mode and sets the height of the top row to 'coord'.
 * Also adjust scroll positions accordingly. We do nothing if not in autosplit mode.*/
void CMainFrame::SetSplitSize(unsigned coord)
{
    if (!m_bAutoSplit) return;
    //CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    //if (pDoc) coord = double(coord)*pDoc->m_zoom/100.;
    int prev=0, dummy;
    CSize orig_pos(0,0);
    switch (m_wndSplitter.GetRowCount()) {
    case 1:
        {
        CMscGenView *pView = dynamic_cast<CMscGenView *>(m_wndSplitter.GetPane(0,0));
        if (pView) orig_pos = pView->GetScrollPosition();
        if (!m_wndSplitter.SplitRow(coord)) return; //failed
        }
        break;
    case 2:
        {
        CMscGenView *pView = dynamic_cast<CMscGenView *>(m_wndSplitter.GetPane(1,0));
        if (pView) orig_pos = pView->GetScrollPosition();
        }
        m_wndSplitter.GetRowInfo(0, prev, dummy);
        m_wndSplitter.SetRowInfo(0, coord, dummy);
        m_wndSplitter.RecalcLayout();
        break;
    default: _ASSERT(0); return;
    }
    int now;
    m_wndSplitter.GetRowInfo(0, now, dummy);
    //Now prev contains what was the size of pane(0) before and now is after
    CMscGenView *pView = dynamic_cast<CMscGenView *>(m_wndSplitter.GetPane(0,0));
    if (pView) {
		CPoint pos = pView->GetScrollPosition();
        pos.x = orig_pos.cx;
        pos.y = 0;
		pView->ScrollToPosition(pos);
    }
    pView = dynamic_cast<CMscGenView *>(m_wndSplitter.GetPane(1,0));
    if (pView) {
		CPoint pos = pView->GetScrollPosition();
        pos.x = orig_pos.cx;
        pos.y = orig_pos.cy + now-prev;
        if (pos.y < coord) pos.y = coord;
		pView->ScrollToPosition(pos);
    }
}

/** Fills the status bar with the cursor position of the internal editor.
 * @param [in] line The line of the cursor. If zero, we do not display any value - no internal editor.
 * @param [in] col The column of the cursor. If zero, we display it as is.
*/
void CMainFrame::SetStatusBarLineCol(unsigned line, unsigned col)
{
    CString s;
    if (line)
        s.Format(_T("%04u:%03u"), line, col);
    m_wndStatusBar.SetPaneText(NUM_STATUS_BAR_LINE_COL, s);
}


/** Fills the design selector combo box with the list of designs and highlights
 * the currently selected.
 * @param [in] current The name of the currently forced design. Can be nullptr.
 * @param [in] updateComboContent If true, we refresh the entries of the combo box,
 *                                the user can select from. (We do this at any time
 *                                the combo box is empty. Setting this flag forces an update.)
 * @param [in] onlyFull If true, add only the full designs. If updateComboContent is
 *                      not set, it is ignored.*/
bool CMainFrame::FillDesignComboBox(const CStringA &current, bool updateComboContent, bool onlyFull)
{
    bool ret = false;
	//Add the list of designs to the combo boxes
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_DESIGN, arButtons);
    for (unsigned u = 0; u < arButtons.GetSize(); u++) {
        CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[u]);

        if (updateComboContent || c->GetCount()==0) {
            c->RemoveAllItems();
            CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
            ASSERT(pApp != nullptr);
            if (pApp->m_config.m_lang->designs.size()) {
                c->AddItem(_T("(chart-defined)"));
                c->AddItem(_T("plain")); //place 'plain' as first
                for (auto pDesign : pApp->m_config.m_lang->designs)
                    if (pDesign.first != "plain" && (!onlyFull || pDesign.second->IsFull()))
                    c->AddItem(AsUnicode(pDesign.first));
            } else
                c->AddItem(_T("-(only plain is available)-"));
        }
        //restore the selection to the given style if it can be found
        int index = current.GetLength()==0 ? 0 : c->FindItem(AsUnicode(current));
        ret = (index >= 0);
        c->SelectItem(ret ? index : 0);
    }
    return ret;
}

/** Called by the framework if the user changes the selection in the design selector box.
 * We call CMscGenDoc::ChangeDesign() with the name of the selected design.*/
void CMainFrame::OnDesignDesign()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_DESIGN, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    int index = c->GetCurSel();
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc && index>=0)
        pDoc->ChangeDesign(index<=0 ? "" : (const char*)AsUTF8(c->GetItem(index)));
}

/** Called the framework when it needs to refresh the state of the design selector combo box.
 * We enable this control only if there are more than one designs. */
void CMainFrame::OnUpdateDesignDesign(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    pCmdUI->Enable(pApp->m_config.m_lang->designs.size()>1);
}


/** Fills the layout selector combo box with the list of layouts available
 * the currently selected.
 * @param [in] current The name of the currently suggested layout.
 * @param [in] updateComboContent If true, we refresh the entries of the combo box,
 *                                the user can select from. (We do this at any time
 *                                the combo box is empty. Setting this flag forces an update.)*/
bool CMainFrame::FillLayoutComboBox(const CStringA &current, bool updateComboContent)
{
    bool ret = false;
    //Add the list of layouts to the combo boxes
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_LAYOUT, arButtons);
    for (unsigned u = 0; u < arButtons.GetSize(); u++) {
        CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[u]);
        if (updateComboContent || c->GetCount()==0) {
            c->RemoveAllItems();
            c->AddItem(_T("(default)"));
            const auto layouts = GetLayoutMethods();
            for (auto l : layouts)
                c->AddItem(AsUnicode(l));
        }
        //restore the selection to the given layout if it can be found
        int index = current.GetLength()==0 ? 0 : c->FindItem(AsUnicode(current));
        ret = (index >= 0);
        c->SelectItem(ret ? index : 0);
    }
    return ret;
}

/** Called by the framework if the user changes the selection in the design selector box.
* We call CMscGenDoc::ChangeDesign() with the name of the selected design.*/
void CMainFrame::OnDesignLayout()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_LAYOUT, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    int index = c->GetCurSel();
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc) {
        if (index>0)
            pDoc->m_itrEditing->SetLayout(AsUTF8(c->GetItem(index)));
        else if (index==0)
            pDoc->m_itrEditing->SetLayout("");
        //else keep unchanged (if nothing selected and index == -1);
        pDoc->CompileEditingChart(false, false, true);
    }
}

/** Called the framework when it needs to refresh the state of the design selector combo box.
* We enable this control only if there are more than one designs. */
void CMainFrame::OnUpdateDesignLayout(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->Enable(pApp->m_config.m_lang->GetName()=="graph");
}


/** Fills the page selector combo box of the ribbon and selects a specific page in the combo box.
 * We also add sub buttons to the copy chart ribbon button, one for each page to copy.
 * @param [in] no_pages The total number of pages in the chart.
 * @param [in] page The currently active page of the chart to select in the combo box.*/
void CMainFrame::FillPageComboBox(int no_pages, int page)
{
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_PAGE, arButtons);
    for (unsigned u = 0; u<arButtons.GetSize(); u++) {
        CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[u]);

        CMFCToolBarEditBoxButton *c2 = CMFCToolBarEditBoxButton::GetByCmd(ID_FULL_SCREEN_PAGE);

        if (no_pages == 1) {
            c->EnableSpinButtons(0, 0);
            c->SetEditText(_T("1/1"));
            if (c2) c2->GetEditBox()->SetWindowText(_T("1/1"));
        } else {
            c->EnableSpinButtons(0, no_pages);
            CString str;
            if (page==0)
                str.Format(_T("(all)/%d"), no_pages);
            else
                str.Format(_T("%d/%d"), page, no_pages);
            c->SetEditText(str);
            if (c2) c2->GetEditBox()->SetWindowText(str);
        }
    }
}

/** Called by the framework if the user selects a different page.
 * We call CMscGenDoc::ChangePage() in response.*/
void CMainFrame::OnDesignPage()
{
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_PAGE, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (!pDoc) return;
    unsigned page = pDoc->m_ChartShown.GetPage();
    const std::string val = AsUTF8str(c->GetEditText());
    if (!from_chars(val, page))
        pDoc->ChangePage(page);
}

/** Called by the framework if the user selects a different page on the full screen toolbar.
* We call CMscGenDoc::ChangePage() in response.*/
void CMainFrame::OnFullScreenPage()
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (!pDoc) return;
    unsigned page = pDoc->m_ChartShown.GetPage();
    const std::string val = AsUTF8str(CMFCToolBarEditBoxButton::GetContentsAll(ID_FULL_SCREEN_PAGE));
    if (!from_chars(val, page))
        pDoc->ChangePage(page);
}

/** Called by the framework to get the status of the page selector combo box.
 * We enable it if there are more than one pages in the compiled chart.*/
void CMainFrame::OnUpdateDesignPage(CCmdUI *pCmdUI)
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        pCmdUI->Enable(pDoc->m_ChartShown.GetPages()>1);
}

/** Called by the framework if the user stepped to the next page.
 * We call CMscGenDoc::StepPage() in return.*/
void CMainFrame::OnDesignNextPage()
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        pDoc->StepPage(+1);
}

/** Called by the framework if the user stepped to the previous page.
* We call CMscGenDoc::StepPage() in return.*/
void CMainFrame::OnDesignPrevPage()
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        pDoc->StepPage(-1);
}

/** Fill in the content of the zoom combo box (if not filled) and its current selection.
 * @param [in] zoom The current zoom value, not adjusted for DPI scaling,
 *                  that is, a value of 100 means one pixel for each
 *                  chart pixel. We divide it by the DPI scaling factor.*/
void CMainFrame::FillZoomComboBox(int zoom)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
	CString text;
    text.Format(_T("%d%%"), int(zoom/pApp->m_config.GetCurrentDPIScalingFactor()));
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_ZOOM, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
    static constexpr unsigned zoom_values[] = {400, 300, 200, 150, 100, 75, 50, 40, 30, 20, 10, 0};
    if (c->GetCount()!=sizeof(zoom_values)/sizeof(unsigned)-1) {
        for (int i=0; zoom_values[i]>0; i++) {
            CString text2;
            text2.Format(_T("%u%%"), zoom_values[i]);
            c->AddItem(text2, zoom_values[i]);
        }
    }
    c->SetEditText(text);
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        SetSplitSize(unsigned(pDoc->m_ChartShown.GetHeadingSize()*double(zoom)/100.));
}

/** Zoom in 10%. Calls CMscGenDoc::SetZoom().*/
void CMainFrame::OnViewZoomin()
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        pDoc->SetZoom(int(pDoc->m_zoom*1.1));
}

/** Zoom out 10%. Calls CMscGenDoc::SetZoom().*/
void CMainFrame::OnViewZoomout()
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (pDoc)
        pDoc->SetZoom(int(pDoc->m_zoom/1.1));
}

/** Called by the framework if the user has changed the zoom combo box.
 * We digest what has the user selected and call CMscGenDoc::SetZoom(), if
 * it is a number and CMscGenDoc::ArrangeViews() if "overview", "auto" or
 * contains "width". (Even if those are not added to the zoom combo box
 * right now.*/
void CMainFrame::OnDesignZoom()
{
	CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (!pDoc) return;
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_DESIGN_ZOOM, arButtons);
    _ASSERT(arButtons.GetSize()==1);
	CMFCRibbonComboBox *c = dynamic_cast<CMFCRibbonComboBox*>(arButtons[0]);
	CString text = c->GetEditText();
	if (!text.CompareNoCase(_T("overview")) || !text.CompareNoCase(_T("auto"))) {
		pDoc->ArrangeViews(CMscGenDoc::EZoomMode::OVERVIEW);
    } else if (-1!=text.Find(_T("width"), 0) || -1!=text.Find(_T("Width"), 0)) {
        pDoc->ArrangeViews(CMscGenDoc::EZoomMode::ZOOM_FITTOWIDTH);
    } else {
        pDoc->SetZoom(int(_ttoi(text)*pApp->m_config.GetCurrentDPIScalingFactor()));
	}
}

/** Fill in the fields of the Embedded Object panel (object size and fallback image coverage)*/
void CMainFrame::FillEmbeddedPanel(size_t size, double percent)
{
    m_wndRibbonBar.ShowContextCategories(ID_CONTEXT_EMBEDDEDOPTIONS);
    if (m_labelObjectSize) {
        CString sizeText = _T("Object size: ");
        if (size>=1536*1024)
            sizeText.AppendFormat(_T("%.2f MB"), size/1024./1024.);
        else if (size>=1536)
            sizeText.AppendFormat(_T("%.2f KB"), size/1024.);
        else if (size>0)
            sizeText.AppendFormat(_T("%u bytes"), size);
        m_labelObjectSize->SetText(sizeText);
    }
    if (m_labelFallbackImage) {
        m_has_fallback_image = bool(percent);
        if (m_has_fallback_image) {
            CString fi;
            fi.AppendFormat(_T("%d%% of chart area is fallback image"), abs(int(floor(percent+0.5))));
            m_labelFallbackImage->SetText(fi);
        } else
            m_labelFallbackImage->SetText(_T("No fallback images in this chart."));
    }
    if (m_labelFallbackImage || m_labelObjectSize)
        m_wndRibbonBar.ForceRecalcLayout();
}

/** Fill in the scale combo box at the Print Preview panel.*/
void CMainFrame::FillScale4Pagination()
{
    if (!IsPrintPreview()) return;
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    if (pApp->m_config.m_iScale4Pagination<-2) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_COMBO_SCALE, arButtons);
    if (arButtons.GetSize()==0) return;
    _ASSERT(arButtons.GetSize()==1);
 	CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    if (!c) return;
    if (pApp->m_config.m_iScale4Pagination==-2)
        c->SetEditText(_T("Fit Page"));
    else if (pApp->m_config.m_iScale4Pagination==-1)
        c->SetEditText(_T("Fit Width"));
    else {
        CString val;
        val.Format(_T("%d%%"), pApp->m_config.m_iScale4Pagination);
        c->SetEditText(val);
    }
}
/** Fill the page size label on the Print Preview category with the current page size taken from
 * CMscGenApp::m_pageSize.*/
void CMainFrame::FillPageSize()
{
    if (!IsPrintPreview()) return;
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(0, arButtons);
    for (int i = 0; i<arButtons.GetSize(); i++)
        if (const LPCTSTR c = arButtons[i]->GetText(); !_tcscmp(c, _T("Page size:")))
            if (CMFCRibbonLabel* pRL = dynamic_cast<CMFCRibbonLabel*>(arButtons[i])) {
                CString s = _T("Page size: ");
                s.Append(AsUnicode(ConvertPageSizeVerbose(pApp->m_config.m_pageSize)));
                if (!_tcscmp(pRL->GetText(), s)) {
                    pRL->SetText(s);
                    m_wndRibbonBar.ForceRecalcLayout();
                }
            }
}

/** Fill the margin edits on the Print Preview category with the current page size taken from
* CMscGenApp::m_printer_usr_margins.*/
void CMainFrame::FillMargins()
{
    if (!IsPrintPreview()) return;
	CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
	ASSERT(pApp != nullptr);
    if (pApp->m_config.m_iScale4Pagination<-2) return;
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_EDIT_MARGIN_L, arButtons);
    if (arButtons.GetSize()==0) return;
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonEdit *c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    if (!c) return;
    CString val;
    val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[0]/PT_PER_CM);
    c->SetEditText(val);

    m_wndRibbonBar.GetElementsByID(ID_EDIT_MARGIN_R, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[1]/PT_PER_CM);
    c->SetEditText(val);

    m_wndRibbonBar.GetElementsByID(ID_EDIT_MARGIN_T, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[2]/PT_PER_CM);
    c->SetEditText(val);

    m_wndRibbonBar.GetElementsByID(ID_EDIT_MARGIN_B, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    c = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    val.Format(_T("%lg"), pApp->m_config.m_printer_usr_margins[3]/PT_PER_CM);
    c->SetEditText(val);
}

/** Called by the framework when the user clicks on the Internal Editor.*/
void CMainFrame::OnViewInternalEditor()
{
    if (!m_ctrlEditor.IsVisible())
        m_ctrlEditor.m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
	m_ctrlEditor.ShowPane(!m_ctrlEditor.IsVisible(), FALSE, TRUE);
}

/** Called by the framework to get the status of the Internal Editor button.
 * We report checked status, if the internal editor is currently visible.*/
void CMainFrame::OnUpdateViewInternalEditor(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_ctrlEditor.IsVisible());
    pCmdUI->Enable();
}

/** Checks if the use has switched to the embedded object category or away from it.
 * It is called from many update functions, which get called at category changes.
 * We compare what category is currently displayed to m_at_embedded_object_category
 * and if there is a change, we call CmscGenDoc::OnActivateEmbeddedRibbonCategory().
 * We also test if we have switched to or away from the Autosave and Recovery pane,
 * and we show/hide the recovered file list accordingly.*/
void CMainFrame::TriggerIfRibbonCategoryChange()
{
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    const bool category_is_embedded = CString(_T("Embedded Object")) == m_wndRibbonBar.GetActiveCategory()->GetName();
    const bool category_is_recovery = CString(_T("Autosave and Recovery")) == m_wndRibbonBar.GetActiveCategory()->GetName();
    if (category_is_embedded != m_at_embedded_object_category) {
        m_at_embedded_object_category = category_is_embedded;
        if (pDoc)
            pDoc->OnActivateEmbeddedRibbonCategory(category_is_embedded);
    }
    if (category_is_recovery!= m_at_recovery_category) {
        m_at_recovery_category = category_is_recovery;
        if (category_is_recovery) {
            CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
            ASSERT(pApp != nullptr);
            m_wndAutoSaveFiles.CollectAutoSavedFiles(
                pApp->GetDataRecoveryHandler()->GetAutosavePath(),
                pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_AUTOSAVE_FILTER_LIST, TRUE) ? pApp->m_config.m_lang : nullptr);
            m_wndAutoSaveFiles.ShowIfHasEntries();
        } else {
            m_wndAutoSaveFiles.ShowPane(false, false, false);
        }
    }
}


/** Called by the framework to see the status of the Default Chart Text button.
 * We do nothing, just call TriggerIfRibbonCategoryChange(). */
void CMainFrame::OnUpdateButtonDefaultText(CCmdUI * /*pCmdUI*/)
{
    //This is just used to test if we have switched to "Embedded Object" category
    //This is only called if this category is visible
    TriggerIfRibbonCategoryChange();
}

/** Called by the framework to see the status of the Default Chart Text button.
* We enable if the object has fallback images and call TriggerIfRibbonCategoryChange(). */
void CMainFrame::OnUpdateEmbeddedoptionsFallbackRes(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(m_has_fallback_image);
    //This is just used to test if we have switched to "Embedded Object" category
    //This is only called if this category is visible
    TriggerIfRibbonCategoryChange();
}

/** Called by the framework when the user dbl clicks the mscgen compat status bar panel.*/
void CMainFrame::OnLanguageStatus()
{
    return; //we do nothing
}


/** Called by the framework to see the status of the mscgen compat status bar panel.*/
void CMainFrame::OnUpdateLanguageStatus(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    CString s = AsUnicode(pApp->m_config.m_lang->description);
    if (pApp->m_config.m_lang->GetName()=="signalling" && m_mscgen_mode_ind==1)
        s += _T(" (mscgen compat mode)");
    m_wndStatusBar.SetPaneTextAndRecalc(NUM_STATUS_BAR_LANGUAGE, s);
}

/** Called by the framework when the user dbl clicks the mscgen compat status bar panel.*/
void CMainFrame::OnUnicodeStatus()
{
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (!pDoc || pDoc->IsEmbedded()) return;  //dont allow changes if embedded
    pDoc->m_unicode = !pDoc->m_unicode;
}


/** Called by the framework to see the status of the character encoding status bar panel.*/
void CMainFrame::OnUpdateUnicodeStatus(CCmdUI *pCmdUI)
{
    CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
    if (!pDoc) return;
    const TCHAR *new_text;
    if (pDoc->IsEmbedded()) {
        new_text = _T("UTF-8");
    } else {
        if (pDoc->m_unicode)
            new_text = _T("Unicode");
        else
            new_text = _T("UTF-8");
    }
    if (m_wndStatusBar.GetPaneText(NUM_STATUS_BAR_UNICODE) != new_text)
        m_wndStatusBar.SetPaneTextAndRecalc(NUM_STATUS_BAR_UNICODE, new_text);
    //disable if an embedded document - we dont allow changing encoding then
    pCmdUI->Enable(!pDoc->IsEmbedded());
    pCmdUI->SetCheck(!pDoc->IsEmbedded() && pDoc->m_unicode);
}

/** Called by the framework to see the status of line-col status bar panel.
 * Here we fill the cursor pos.*/
void CMainFrame::OnUpdateLineCol(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    if (!IsInternalEditorVisible()) {
        SetStatusBarLineCol(0, 0);
        return;
    }
    long start, end;
    pApp->m_pWndEditor->m_ctrlEditor.GetSel(start, end);
    int line, col;
    pApp->m_pWndEditor->m_ctrlEditor.ConvertPosToLineCol(start, line, col);
    SetStatusBarLineCol(line+1, col);
    pCmdUI->Enable();
}


/** Request full re-csh by the user. Do nothing if internal editor not visible.*/
void CMainFrame::OnEditReCsh()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    if (!IsInternalEditorVisible())
        return;
    m_ctrlEditor.m_ctrlEditor.ReCsh();
}


/** Update the re-csh button (not used now)*/
void CMainFrame::OnUpdateEditReCsh(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->Enable(IsInternalEditorVisible() && pApp->m_config.m_lang->pCsh);
}


/** When the re-indent button is pressed. */
void CMainFrame::OnButtonReIndent()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    if (IsInternalEditorVisible())
        if (m_ctrlEditor.m_ctrlEditor.ReIndent(0, -1))
            //if there were changes, re-color the doc
            //(this is done so that CMscGenDoc is notified and can save undo)
            m_ctrlEditor.m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::CSH);
}


/** Takes the value of the ctrlID ribbon edit control and places it to
 *  *member and writes to regKey in the registry. Used to set tab sizes.*/
void CMainFrame::OnEditTabValue(UINT ctrlID, const TCHAR* regKey, int *member)
{
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ctrlID, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonEdit *e = dynamic_cast<CMFCRibbonEdit*>(arButtons[0]);
    _ASSERT(e);
    int val;
    const std::string text = AsUTF8str(e->GetEditText());
    if (!from_chars(text, val) && member && 0<=val && val<10) {
        *member = val;
        CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
        ASSERT(pApp != nullptr);
        pApp->WriteProfileInt(REG_SECTION_SETTINGS, regKey, val);
    } else if (member) {
        CString num;
        num.Format(_T("%d"), *member);
        e->SetEditText(num);
        ::MessageBeep(MB_ICONEXCLAMATION);
    }
}

void CMainFrame::OnEditTabValue(UINT ctrlID, const TCHAR* regKey, void(LanguageCollection::*setter)(int))
{
    int tmp;
    OnEditTabValue(ctrlID, regKey, &tmp);
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    //now set the value in each csh class
    (pApp->m_config.m_languages.*setter)(tmp);
}


void CMainFrame::OnUpdateEditIndentTabsize(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->Enable(IsInternalEditorVisible() && pApp->m_config.m_lang->pCsh);
}


/** When the 'special indent for attributes' checkbox on the ribbon is clicked. */
void CMainFrame::OnCheckIndentSpecAttr()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pApp->m_config.m_languages.cshParams->m_bSIAttr = !pApp->m_config.m_languages.cshParams->m_bSIAttr;
    pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SI_ATTR,
        pApp->m_config.m_languages.cshParams->m_bSIAttr);
}


/** When the 'special indent for labels' checkbox on the ribbon is clicked. */
void CMainFrame::OnCheckIndentSpecLabel()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pApp->m_config.m_languages.cshParams->m_bSIText = !pApp->m_config.m_languages.cshParams->m_bSIText;
    pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_SI_TEXT,
        pApp->m_config.m_languages.cshParams->m_bSIText);
}

/** Called to see if we can enable (and what status) for the 'spec indent for labels' checkbox on the ribbon. */
void CMainFrame::OnUpdateCheckIndentSpecLabel(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->Enable(IsInternalEditorVisible() && pApp->m_config.m_lang->pCsh);
    pCmdUI->SetCheck(pApp->m_config.m_languages.cshParams->m_bSIText);
}


/** Called to see if we can enable (and what status) for the 'spec indent for attrs' checkbox on the ribbon. */
void CMainFrame::OnUpdateCheckIndentSpecAttr(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->Enable(IsInternalEditorVisible() && pApp->m_config.m_lang->pCsh);
    pCmdUI->SetCheck(pApp->m_config.m_languages.cshParams->m_bSIAttr);
}


void CMainFrame::OnButtonSelectFont()
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    const bool TrueTypeOnly = true;
    const bool FixedPitchOnly = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_FIXEDONLY, TRUE);
    CString name = pApp->GetProfileString(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_NAME, _T(""));
    int point_size10 = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_SIZE10, 160);
    int char_set = pApp->GetProfileInt(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_CHARSET, DEFAULT_CHARSET);
    CHOOSEFONT cf;
    LOGFONT lf;
    CEditorBar::FillLogFont(lf, name, point_size10, char_set, FixedPitchOnly);
    cf.lStructSize = sizeof(cf);
    cf.Flags = CF_SCREENFONTS| CF_NOVERTFONTS | CF_INITTOLOGFONTSTRUCT;
    if (FixedPitchOnly)
        cf.Flags |= CF_FIXEDPITCHONLY;
    if (TrueTypeOnly)
        cf.Flags |= CF_SCALABLEONLY;
    cf.lpLogFont = &lf;
    cf.hwndOwner = m_hWnd;
    cf.hDC = nullptr;
    cf.lpfnHook = nullptr;

    //cf.hInstance = AfxGetResourceHandle();
    //cf.lpTemplateName = MAKEINTRESOURCE(IDD_MYFONTTEMPLATE);
    //cf.Flags |= CF_ENABLETEMPLATE;

    if (IDOK == ChooseFont(&cf)) {
        name = lf.lfFaceName;
        point_size10 = cf.iPointSize;
        char_set = lf.lfCharSet;
        pApp->WriteProfileString(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_NAME, name);
        pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_SIZE10, point_size10);
        pApp->WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_INTERNAL_FONT_CHARSET, char_set);
        m_ctrlEditor.SelectNewFont(name, point_size10, char_set, FixedPitchOnly);
    }
}


void CMainFrame::OnUpdateButtonSelectFont(CCmdUI *pCmdUI)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    pCmdUI->Enable(pApp && IsInternalEditorVisible());
}


/** Show the balloon window over the Text editor button of the ribbon,
 * informing the user what to use to re-open it again.*/
void CMainFrame::ShowInternalEditorCloseBalloon()
{
    //Now adjust description tool tip
    if (!m_wndBalloon.m_hWnd) return;
    ActivateHomeCategory();
    CArray<CMFCRibbonBaseElement*, CMFCRibbonBaseElement*> arButtons;
    m_wndRibbonBar.GetElementsByID(ID_BUTTON_EDITORS, arButtons);
    _ASSERT(arButtons.GetSize()==1);
    CMFCRibbonButton *b = dynamic_cast<CMFCRibbonButton*>(arButtons[0]);
    _ASSERT(b);

    TOOLINFO ti;
    m_wndBalloon.FillInToolInfo(ti, this, 1);
    //if we have a selection and it actually has a description,
    //update the tooltip and show it.
    ti.uFlags |= TTF_TRACK | TTF_CENTERTIP;
    ti.rect = b->GetRect();
    ti.lpszText = const_cast<TCHAR*>(_T("You have closed the internal editor."));
    int ret = m_wndBalloon.SendMessage(TTM_ADDTOOL, 0, (LPARAM)&ti);
    m_wndBalloon.SetToolInfo(&ti);
    m_wndBalloon.SetDescription(_T("Use the 'Text Editors...' button or the context menu to re-enable."));
    m_wndBalloon.SetFixedWidth(250, 350);
    CPoint p;
    p.x = ti.rect.left;
    p.y = ti.rect.bottom;
    ClientToScreen(&p);
    ret = m_wndBalloon.SendMessage(TTM_TRACKPOSITION, 0, MAKELPARAM(p.x, p.y));
    ret = m_wndBalloon.SendMessage(TTM_TRACKACTIVATE, TRUE, (LPARAM)&ti);
    ret = ::SetWindowPos(m_wndBalloon.m_hWnd, HWND_TOPMOST, 0, 0, 0, 0,
        SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER);

    SetTimer(BALLOON_TIMER_NUMBER, BALLOON_TIMER_INTERVAL, nullptr);
}

/** Called by the framework when the balloon timer fires.
 * We call CMscGenDoc::DoFading() to do the animations (which
 * will Invalidate() us, if needed causing a repain after we have procesed
 * this message). If we have no remaining animations, we kill the timer.*/
void CMainFrame::OnTimer(UINT_PTR timer)
{
    switch (timer) {
    case BALLOON_TIMER_NUMBER: {
        TOOLINFO ti;
        m_wndBalloon.FillInToolInfo(ti, this, 1);
        m_wndBalloon.SendMessage(TTM_TRACKACTIVATE, FALSE, (LPARAM)&ti);
        break;
    }
    case FADE_TIMER_NUMBER: {
        CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(GetActiveDocument());
        if (pDoc && pDoc->m_pViewFadingTimer)
            pDoc->m_pViewFadingTimer->OnFadingTimer();
        break;
    }
    default:
        _ASSERT(0);
    }
}

void CMainFrame::OnUpdateButtonEditors(CCmdUI *pCmdUI)
{
    pCmdUI->Enable();
}


void CMainFrame::OnEscape()
{
    ActivateHomeCategory();
}
