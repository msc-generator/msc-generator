/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file MscGenConf.h Main header file for the Config object common between the DLL and App
 * @ingroup Msc_generator_files */

#pragma once

#ifndef __AFXWIN_H__
    #error "include 'stdafx.h' before including this file for PCH"
#endif

#include <string>
#include "resource.h"       // main symbols
#include "canvas.h"
#include "utf8utils.h"
#include "commandline.h"
#include "cgencommon.h"

 /** Gets Alpha channel value.
 * Complementing WinGDI macros (like GetRValue())
 * for trackrects color fill and line*/
#define GetAValue(rgb)      (LOBYTE((rgb)>>24))
 /** Coverts a color having alpha channel to a 32-bit COLORREF */
#define RGBA(r,g,b,a)       ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)|(((DWORD)(BYTE)(a))<<24)))
 /** The main application class.
 * This is an SDI application, so only one Document per app.*/


/** Converts a wide char string to UTF8. */
inline CStringA AsUTF8(const CStringW &text) { return ConvertFromUTF16_to_UTF8(std::wstring_view(text.GetString(), text.GetLength())).c_str(); }
/** Converts a wide char string to UTF8. */
inline std::string AsUTF8str(const CStringW& text) { return ConvertFromUTF16_to_UTF8(std::wstring_view(text.GetString(), text.GetLength())); }
/** Converts an ASCII or UTF8 string to a wide char CString */
inline CStringW AsUnicode(std::string_view text) { return CStringW(ConvertFromUTF8_to_UTF16(text).c_str()); }
/** Converts an ASCII or UTF8 string to a wide char CString */
inline CStringW AsUnicode(const CStringA &text) { return AsUnicode(std::string_view(text, text.GetLength())); }

FILE* OpenNamedFile(const char* utf8filename, bool write, bool binary);

class CMscGenConf
{
public:
    EPageSize m_pageSize;          ///<Raw printer page size, NO_PAGE means a custom size
    XY                 m_PhyPrinterPageSize;     ///<raw page size in points
    double             m_printer_phy_margins[4]; ///<left,right, up, down physical margins of the printer in points
    double             m_printer_usr_margins[4]; ///<left,right, up,down margins specified by the user in points
    XY                 m_PrinterScale;           ///<The scale to apply so that we get from points to printer pixels
protected:
    double             m_DPIScaling;             ///<The scaling factor for high-res monitors. 100% (==value of 1.0) for 96DPI screens.
public:
    /** Return printable area in points (physical page - user margins)*/
    XY GetPrintablePaperSize() const { return m_PhyPrinterPageSize - XY(m_printer_usr_margins[0] + m_printer_usr_margins[1], m_printer_usr_margins[2] + m_printer_usr_margins[3]); }
    /** Calculate the scale to apply to print a given size page.
     * The returned scaling factor will be the scale from chart pixels
     * to points (=user scaling)
     * This is based on the user preference: fit to page/fit width/a given %*/
    double CalculateScaleFor(double x, double y) const {
        const XY printable = GetPrintablePaperSize(); //this is in points (1/72 inch)
        if (m_iScale4Pagination<=-2)
            return std::min(printable.x/x, printable.y/y);
        else if (m_iScale4Pagination==-1)
            return printable.x/x;
        else
            return m_iScale4Pagination/100.0;
    }

        /** @name User selected preferences
     * These can be set on the preferences category of the ribbon. */
    /** @{ */
	bool m_Pedantic;                     ///<True if pedantic compilation should be used
	bool m_Warnings;                     ///<True if warnings are to be displayed
    bool m_FullPath;                     ///<True if the full path of files shall be shown in error messages.
	bool m_bPageBreaks;                  ///<True if page breaks are to be displayed
	unsigned m_uFallbackResolution;      ///<The resolution of fallback images (OLE objects)
    LanguageCollection m_languages;      ///<The data for all things needed for a language
    const LanguageData *m_lang;          ///<The current language
    std::optional<Examples> m_examples;  ///<The examples loaded for each language
	CStringA m_CopyrightText;            ///<The copyright text to display at the bottom of the charts. Can be UTF-8.
	bool m_bShowCsh;                     ///<True if we show CSH markings in internal editor.
    bool m_bShowCshErrors;               ///<True if we show CSH discovered parse errors
    bool m_bShowCshErrorsInWindow;       ///<True if we list the CSH discovered parse errors in the output window.
    bool m_bSmartIndent;                 ///<Whether we do smart indent or not. If true m_csh.ColonLabels are kep up-to-date at every change
    bool m_bTABIndents;                  ///<Whether TAB indents the line (true) or inserts a tab
    CHARFORMAT m_csh_cf[CSH_SCHEME_MAX][COLOR_MAX]; ///<The colors used for each color scheme
	COLORREF m_trackFillColor;           ///<The color used to fill element tracking outlines
	COLORREF m_trackLineColor;           ///<The color used to draw element tracking outlines
    bool m_bHints;                       ///<True if hinting is turned on
    bool m_bHintLineStart;               ///<true if we provide hints at the beginning of a line
    bool m_bHintEntity;                  ///<True if we provide hints for entity names
    bool m_bHintEscape;                  ///<True if we provide hints for text formatting escapes
    bool m_bHintAttrName;                ///<True if we provide hints for attribute names
    bool m_bHintAttrValue;               ///<True if we provide hints for attribute values
    bool m_bHintKeywordMarker;           ///<True if we provide hints for mid-command keywords and markers
    int m_iHintFilter;                   ///<An EHintFilter enum cast to int saying how we filter hints
    bool m_bHintCompact;                 ///<True if we compact the hint list hiding hints with same prefix
    bool m_bShowControls;                ///<True if we show element controls (collapse/expand)
    bool m_bAutoPaginate;                ///<True if we auto-paginate the chart
    bool m_bAutoHeading;                 ///<True if we add an auto heading for each automatic page break
    int m_iScale4Pagination;             ///<The scale used for auto-pagination. In percentage, -1=Fit Width -2=Fit Page
    int m_iPageAlignment;                ///<Alignment of chart in the fix-size page: -1/0/+1 left/center/right + -3/0/+3 bottom/center/top
    EMscgenCompat m_mscgen_compat;       ///<How we approach mscgen compatibility
    bool m_bWarnMscgenCompat;            ///<If we report mscgen deprecated features.
    bool m_bGraphUsesOriginalEngine;     ///<If we use the original DOT engine (debug feature)
    bool m_bRecoveryOverwrites;          ///<At recovery we overwrite the current chart text
    bool m_bRecoveryDeletes;             ///<At recovery we delete the recovered autosave
    bool m_bTechnicalInfo;               ///<True if we generate a warning to show compilation time
    bool m_bCurrentLineHighLight;        ///<True if we highlight the current line in the internal editor
    bool m_bShowLineNums;                ///<True if we show line nums in internal editor.
    bool m_bSkipCopyright;               ///<If true, we skip the copyright text
    bool m_autopaste;                    ///<If set, Msc-generator activates on a compatible office snippet being copied to the clipboard and pastes it, if there are no unsaved changes.
    bool m_instant_compilation;          ///<If set, we always compile after every change.
    bool m_auto_save;                    ///<If set, we save files and clipboard content after every change

	//Editor related
    /** Types of External editor */
	enum EEditorType {
        NOTEPAD=0, ///<Windows notepad
        NPP=1,     ///<Notepad++ (supports jump into file)
        OTHER=2    ///<Some other external editor
    } m_iTextEditorType; ///<Which external editor to use
	CString m_sStartTextEditor; ///<Command line to start the "other" external editor
	CString m_sJumpToLine;      ///<Command line to jump to a specific line in the "other" external editor
	CString m_NppPath;          ///<The path of the Notepad++ program, empty if not installed here.
    /** @} */


public:
    CMscGenConf();
    CString Init();
    /** Set the language. True on success.*/
    bool SetLanguage(const LanguageData *l);
    /** Set the language. True on success.*/
    bool SetLanguage(std::string_view lang) { return SetLanguage(m_languages.GetLanguage(lang)); }
    bool IsCurrentLanguage(std::string_view lang) const noexcept { return m_lang && m_lang->HasExtension(lang); }
    double GetCurrentDPIScalingFactor() const;
    bool NormalizeUserMargins();
    void ReadRegistryValues(bool reportProblem);
    std::vector<std::pair<std::string, std::string>> ReadDesigns(unsigned folder, const CString & fileName, std::string_view language);
    CString ReadAllDesigns();
    static std::tuple<std::string, std::string, std::string> StaticReadIncludeFile(std::string_view filename, void *config, std::string_view included_from);
    std::tuple<std::string, std::string, std::string> ReadIncludeFile(CStringW &&filename, CStringW &&included_from);
    static std::vector<std::string> FileListProc(std::string_view prefix, std::string_view included_from);
    std::vector<std::string> FileListProc(CStringW &&prefix, CStringW &&included_from);
};


//Implemented both in Msc-generator.cpp and MscGeneratorHandlers.cpp
CMscGenConf *GetConf();
class CEditorBar;
CEditorBar *GetInternalEditor();
bool IsInternalEditorVisible();
int GetRegistryInt(LPCWSTR key, int def);
CStringW GetRegistryString(LPCWSTR key, LPCWSTR def);



//Registry data keys
#define REG_SECTION_SETTINGS _T("Settings")
#define REG_KEY_PEDANTIC _T("Pedantic")
#define REG_KEY_WARNINGS _T("Warnings")
#define REG_KEY_FULL_PATH_IN_ERRORS _T("FullPathInErrors")
#define REG_KEY_PB_EDITING _T("PageBreak_Editing")
#define REG_KEY_FALLBACK_RESOLUTION _T("Default_Fallback_Resolution")
#define REG_KEY_ALWAYSOPEN _T("AlwaysOpen")
#define REG_KEY_TEXTEDITORTYPE _T("TextEditorType")
#define REG_KEY_STARTTEXTEDITOR _T("TextEditorCommand")
#define REG_KEY_JUMPTOLINE _T("JumpToLineCommand")
#define REG_KEY_DEFAULTTEXT _T("DefaultText")
#define REG_KEY_DEFAULTZOOMMODE _T("DefaultZoomMode")
#define REG_KEY_CSHENABLED _T("CshEnabled")
#define REG_KEY_CSHSCHEME _T("CshScheme")
#define REG_KEY_SMARTINDENT _T("SmartIdent")
#define REG_KEY_TABINDENTS _T("TABIdents")
#define REG_KEY_CSHERRORS _T("CshErrorsEnabled")
#define REG_KEY_CSHERRORSINWINDOW _T("CshErrorsInWindow")
#define REG_KEY_FINDWINPOS_X _T("FindReplaceWindowXPos")
#define REG_KEY_FINDWINPOS_Y _T("FindReplaceWindowYPos")
#define REG_KEY_FINDMATCHCASE _T("FindReplaceMatchCase")
#define REG_KEY_TRACKFILLRGBA _T("TrackFillRGBA")
#define REG_KEY_TRACKLINERGBA _T("TrackLineRGBA")
#define REG_KEY_NOREMIND_VERSION_MAJOR _T("NoRemindVersionMajor")
#define REG_KEY_NOREMIND_VERSION_MINOR _T("NoRemindVersionMinor")
#define REG_KEY_NOREMIND_VERSION_SUPER_MINOR _T("NoRemindVersionSuperMinor")
#define REG_KEY_LAST_VERSION_SEEN_MAJOR _T("NoLastVersionSeenMajor")
#define REG_KEY_LAST_VERSION_SEEN_MINOR _T("NoLastVersionSeenMinor")
#define REG_KEY_LAST_VERSION_SEEN_SUPER_MINOR _T("NoLastVersionSeenSuperMinor")
#define REG_KEY_LAST_VERSION_SEEN_URL _T("NoLastVersionSeenSuperURL")
#define REG_KEY_TABSIZE _T("TabSize")
#define REG_KEY_TABSIZE_II _T("TabSizeII")
#define REG_KEY_TABSIZE_IB _T("TabSizeIB")
#define REG_KEY_TABSIZE_IM _T("TabSizeIM")
#define REG_KEY_TABSIZE_IA _T("TabSizeIA")
#define REG_KEY_SI_ATTR _T("SIAttr")
#define REG_KEY_SI_TEXT _T("SIText")
#define REG_KEY_HINT _T("DoHints")
#define REG_KEY_HINT_LINESTART _T("HintLineStart")
#define REG_KEY_HINT_ENTITY _T("HintEntity")
#define REG_KEY_HINT_ESCAPE _T("HintEscape")
#define REG_KEY_HINT_ATTRNAME _T("HintAttrName")
#define REG_KEY_HINT_ATTRVALUE _T("HintAttrValue")
#define REG_KEY_HINT_KEYWORD_MARKER _T("HintKeywordMarker")
#define REG_KEY_HINT_COMPACT _T("HintCompact")
#define REG_KEY_HINT_FILTER _T("HintFilterNew")
#define REG_KEY_SHOW_CONTROLS _T("ShowElementControls")
#define REG_KEY_LOAD_DATA _T("LoadData")
#define REG_KEY_AUTO_PAGINATE _T("AutoPaginate")
#define REG_KEY_AUTO_HEADING _T("AutoHeading")
#define REG_KEY_SCALE4PAGINATION _T("Scale4Pagination")
#define REG_KEY_PAGE_ALIGNMENT _T("PageAlignment")
#define REG_KEY_PAGE_MARGIN_L _T("PageMarginL")
#define REG_KEY_PAGE_MARGIN_R _T("PageMarginR")
#define REG_KEY_PAGE_MARGIN_T _T("PageMarginT")
#define REG_KEY_PAGE_MARGIN_B _T("PageMarginB")
#define REG_KEY_MSCGEN_COMPAT _T("MscgenCompat")
#define REG_KEY_MSCGEN_COMPAT_WARN _T("MscgenCompatWarn")
#define REG_KEY_INTERNAL_EDITOR_FONT _T("InternalEditorFontName")
#define REG_KEY_TYPICAL_NEW_CHART _T("TypicalNewChartDescr")
#define REG_KEY_ASK_FOR_NEW_CHART _T("AskForNewChart")
#define REG_KEY_AUTOSAVE _T("Autosave")
#define REG_KEY_AUTOSAVE_INTERVAL _T("AutosaveInterval")
#define REG_KEY_AUTOSAVE_OPEN_AT_STARTUP _T("AutosaveOpenAtStartup")
#define REG_KEY_AUTOSAVE_FILTER_LIST _T("AutosaveFilterList")
#define REG_KEY_LAST_ZOOM _T("Last Zoom")
#define REG_KEY_INTERNAL_FONT_NAME _T("InternalEditorFontName")
#define REG_KEY_INTERNAL_FONT_SIZE10 _T("InternalEditorFontSize")
#define REG_KEY_INTERNAL_FONT_CHARSET _T("InternalEditorFontCharSet")
#define REG_KEY_INTERNAL_FONT_FIXEDONLY _T("InternalEditorFontSelectionFixedOnly")
#define REG_KEY_RECOVERY_OVERWRITE _T("RecoveryOverwrites")
#define REG_KEY_RECOVERY_DELETE _T("RecoveryDeletes")
#define REG_KEY_INSTALLDIR _T("InstallDir")
#define REG_KEY_TECHNICAL_INFO _T("TechnicalInfo")
#define REG_KEY_CURRENT_LINE_HIGHLIGHT _T("CurrentLineHighlight")
#define REG_KEY_SHOW_LINE_NUMS _T("ShowLineNums")
#define REG_KEY_SKIP_COPYRIGHT _T("SkipCopyright")
#define REG_KEY_AUTO_PASTE _T("AutoPaste")
#define REG_KEY_INSTANT_COMPILATION _T("InstantCompilation")
#define REG_KEY_AUTO_SAVE _T("AutoSave")
#define REG_KEY_LAST_VERSION_STARTED _T("LastVersionStarted")
