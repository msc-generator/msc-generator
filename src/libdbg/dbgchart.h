/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file dbgchart.h The declaration for the DbgChart class.
* @ingroup libdbg_files */

#ifndef DBG_GRAPHS_H
#define DBG_GRAPHS_H

#include "cgencommon.h"

/** This is the namespace containing dbg chart elements, except parsing.*/
namespace dbg {

class DbgElement : public Element
{
public:
    DbgElement(Chart* chart, const Edge& e, FileLineColRange&& r) {
        file_pos = std::move(r);
        area = Path{ e }.SimpleWiden(chart->trackFrameWidth);
        area.arc = this;
        chart->AllCovers += area;
        chart->AllElements[file_pos] = this;
    }
    void Scale(double zoom) { area.Scale(zoom); area_draw.Scale(zoom); }
};

struct DbgEdge : Edge
{
    LineAttr line = LineAttr(ELineType::SOLID, ColorType::black(), 1);
    XY off = XY(0, 0);
};

using DbgPath = contour::EdgeVector<DbgEdge>;

/** The chart class.*/
class DbgChart : public ChartBase<ContextBase<Style>>
{
public:
    /** @name Language attributes
    * @{ */
    std::string GetLanguageDescription() const override { return "Dbg Contours"; } ///<Returns the human-readable (UTF-8) short description
    std::vector<std::string> GetLanguageExtensions() const override { return{"dbg"}; } ///<Returns a set of extensions in order of preference
    std::string GetLanguageEntityName() const override { return "dbg entity"; }
    std::string GetLanguageDefaultText() const override { return ""; } ///<Returns the default text of the chart in UTF-8
    bool GetLanguageHasAutoheading() const override { return false; } ///<True if the chart has automatic headings
    bool GetLanguageHasElementControls() const override { return false; } ///<True if the elements of the chart support GUI controls
    std::unique_ptr<Csh> CshFactory(Csh::FileListProc proc, const LanguageCollection* languages) const override;
    std::map<std::string, std::string> RegisterLibraries() const override;
    /** @} */
public: 
    std::vector<DbgPath> paths;
    std::list<DbgElement> elements; //list to preserve pointers
    std::optional<XY> center;       //if set, we limit to a 1000x1000 (divided by zoom) rect around this point 
    double size = 1000;             //if a center is set, this is the size around it that we limit to
    double zoom = 1;                //apply this zoom
    DbgChart(FileReadProcedure *p, void *param, const LanguageCollection* languages);
    ~DbgChart();

    static std::unique_ptr<Chart> Factory(FileReadProcedure *p, void *param, const LanguageCollection* languages) { return std::make_unique<DbgChart>(p, param, languages); }
    bool AddCommandLineArg(const std::string&) override { return false; }
    void AddCommandLineOption(const Attribute&) override {}
    bool GetPedantic() const noexcept override { return forced_pedantic.value_or(true); } //Change this, if you support pedantic mode

    void AddEdge(const Edge & edge, FileLineColRange l);
    void ParseText(std::string_view input, const FileLineCol& first_char) override;
    void CompleteParse(bool autoPaginate = false,
                       bool addHeading = true, XY pageSize = XY(0, 0),
                       bool fitWidth = true, bool collectLinkInfo = false) override;
    void CollectIsMapElements(Canvas & canvas) override;
    void RegisterAllLabels() override;
    void Draw(Canvas & canvas, bool pageBreaks, unsigned page) override;
    void SetToEmpty() override;
};

};//namespace

using dbg::DbgChart;

#endif