#include <type_traits>
#include <iostream>
#include "maphoon-lexer/includes.h"
#include "xxx_lexer.h"
#include "xxx_parser_compile.h"

int main() {
    std::ostringstream out;
    lexing::printcode<char, xxx_compile_tokentype>( "char", "int", {"xxx"},
        xxx::build_classifier<xxx_compile_tokentype>(), out,
        [](std::ostream& out, char ch) {
             unsigned char c = ch;
             if (c=='\\' || c=='\'') out << "\'\\" << c << '\'';
             else if (32 <= c && c <= 127) out << '\'' << c << '\'';
             else out << (int)c; },
        [](std::ostream& out, xxx_compile_tokentype S) { out << int(S) ; }
    );
    //Remove Q00 (and the following colon) if present only once
    //to avoid "unused label" warnings
    std::string ret = std::move(out).str();
    if (const size_t pos = ret.find("Q00")
        ; pos != ret.npos && ret.substr(pos, 4) == "Q00:" && ret.find("Q00", pos + 1) == ret.npos)
        ret.erase(pos, 4);

    std::ofstream("xxx_tokenizer.h") << ret;
}