/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2024 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file xxxstyle.cpp Xxx specific style and context definitions 
* @ingroup libxxx_files */


#include <cstring>
#include "xxxstyle.h"
#include "xxxchart.h"

using namespace xxx;

///Control which components are used
XxxStyle::XxxStyle(EStyleType tt, EColorMeaning cm, EArcArrowType a,
                   bool t, bool l, bool f, bool s, bool nu, bool shp, bool x):
    SimpleStyleWithArrow<FullArrowAttr>(tt,cm, a, t, l, f, s, nu, shp),
    f_xxx(x)
{
    //Add code that marks which of your attributes can be assigned
    //empty the style.
}

//Has all the components, but is empty
XxxStyle::XxxStyle(EStyleType tt, EColorMeaning cm) :
    SimpleStyleWithArrow<FullArrowAttr>(tt, cm),
    f_xxx(true)
{
    //Add all your components
    //EMpty the style
}

void XxxStyle::Empty()
{
    //clear all your attributes
    SimpleStyle::Empty();
}

void XxxStyle::MakeCompleteButText()
{
    //set default value to all your attributes, except text
    //(so that global text options can be added)
    SimpleStyle::MakeCompleteButText();
}



Style &XxxStyle::operator += (const Style &toadd)
{
    const XxxStyle* p = dynamic_cast<const XxxStyle *>(&toadd);
    if (p) {
        //Write your own code adding an XxxStyle to another
    }
    return *this;
}


/** Add an atribute to a style. Provide errors if needed and return true
 * if the attrbute name was recognized as one that applies to us.*/
bool XxxStyle::AddAttribute(const Attribute & a, Chart *chart)
{
    if (f_xxx && a.Is("xxx")) {
        //Write your own code adding the attribute
        return true;
    }
    if (f_xxx && a.EndsWith("xxx")) {
        //Write your own code adding the attribute
        return true;
    }
    return SimpleStyle::AddAttribute(a, chart);
}

void XxxStyle::AttributeNames(Csh &csh) const
{
    //Add your own attributes
    if (f_xxx)
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME)+"xxx",
            "Explanation for attribute xxx",
            EHintType::ATTR_NAME));
    SimpleStyle::AttributeNames(csh);
}

bool XxxStyle::AttributeValues(std::string_view attr, Csh &csh) const
{
    //test for your own attributes and add their potential values
    if (CaseInsensitiveEqual(attr, "xxx") && f_xxx) {
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<number: \b0.0..1.0\b>",
            "Explain this attribute value",
            EHintType::ATTR_VALUE, false));
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "auto",
            "Explain this attibute value.",
            EHintType::ATTR_VALUE, false));
        return true;
    }
    return SimpleStyle::AttributeValues(attr, csh);
}

void XxxContext::ApplyContextContent(const XxxContext &o)
{
    ContextBase::ApplyContextContent(o);
    if (o.IsFull()) {
        //copy assign all the context members you have added                  
    } else {
        //merge all the context members you have added                  
    }
}

void XxxContext::ApplyContextContent(XxxContext &&o)
{
    if (o.IsFull()) {
        //move assign all the context members you have added                  
    } else {
        //move merge all the context members you have added                  
    }
    ContextBase::ApplyContextContent(std::move(o));
}


void XxxContext::Empty()
{
    ContextBase<XxxStyle>::Empty();
    ///Add empty versions of the default styles
}

void XxxContext::Plain()
{
    ContextBase<XxxStyle>::Plain();
    ///Add standard ('plain') values for your default styles
}


