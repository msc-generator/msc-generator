/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2024 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file xxxchart.h The declaration for the XxxChart class.
* @ingroup libxxx_files */

#ifndef XXX_GRAPHS_H
#define XXX_GRAPHS_H

#include "cgencommon.h"
#include "xxxstyle.h"

/** This is the namespace containing xxx chart elements, except parsing.*/
namespace xxx {

class XxxChart;

class XxxInstruction {
public:
    XxxInstruction(XxxChart&);
    void SetLineEnd(const FileLineColRange&) {}
};


using XxxInstrList = UPtrList<XxxInstruction>;



/** The chart class.*/
class XxxChart : public ChartBase<XxxContext>
{
public:
    /** @name Language attributes
    * @{ */
    std::string GetLanguageDescription() const override { return "Xxx Graph"; } ///<Returns the human-readable (UTF-8) short description
    std::vector<std::string> GetLanguageExtensions() const override { return{"xxx", "xx2"}; } ///<Returns a set of extensions in order of preference
    std::string GetLanguageEntityName() const override { return "xxx entity"; }
    std::string GetLanguageDefaultText() const override { return "#This is the default xxx text\n"; } ///<Returns the default text of the chart in UTF-8
    bool GetLanguageHasAutoheading() const override { return false; } ///<True if the chart has automatic headings
    bool GetLanguageHasElementControls() const override { return false; } ///<True if the elements of the chart support GUI controls
    std::unique_ptr<Csh> CshFactory(Csh::FileListProc proc, const LanguageCollection* languages) const override;
    std::map<std::string, std::string> RegisterLibraries() const override;
    /** @} */
public:
    XxxChart(FileReadProcedure *p, void *param, const LanguageCollection* languages);
    ~XxxChart();

    XxxInstrList PopContext();            ///<Closes a scope

    static std::unique_ptr<Chart> Factory(FileReadProcedure *p, void *param, const LanguageCollection* languages) { return std::make_unique<XxxChart>(p, param, languages); }

    bool AddCommandLineArg(const std::string & arg) override;
    void AddCommandLineOption(const Attribute & a) override;
    bool DeserializeGUIState(std::string_view) override;
    std::string SerializeGUIState() const override;
    bool ControlClicked(Element *, EGUIControlType) override;
    bool ApplyForcedDesign(const string & name) override;
    bool GetPedantic() const noexcept override { return forced_pedantic.value_or(true); } //Change this, if you support pedantic mode
    void SetPedantic(bool pedantic) noexcept override { Chart::SetPedantic(pedantic); }   //Change this, if you support pedantic mode

    bool AddChartOption(const Attribute &a);
    std::unique_ptr<XxxInstruction> AddAttribute(const Attribute&);
    bool AddDesignAttribute(const Attribute&);

    void ParseText(std::string_view input, const FileLineCol& first_char) override;
    void CompleteParse(bool autoPaginate = false,
                       bool addHeading = true, XY pageSize = XY(0, 0),
                       bool fitWidth = true, bool collectLinkInfo = false) override;
    void CollectIsMapElements(Canvas & canvas) override;
    void RegisterAllLabels() override;
    void Draw(Canvas & canvas, bool pageBreaks, unsigned page) override;
    void SetToEmpty() override;
};

};//namespace

using xxx::XxxChart;

#endif