#!/usr/bin/env python3
# Highlight red pixels from https://stackoverflow.com/questions/61481540/find-the-coordinates-of-red-pixel-in-an-image
# red.py FILE [MAX-TO-SHOW (3)]
import cv2
import numpy as np
from sys import argv
img = argv[1]
num = int(argv[2]) if len(argv) > 2 else 3

out = img.split('.')
out[-2] += '_red'
out = '.'.join(out)

img = cv2.imread(img)
r = max([int(min(img.shape[:2]) / 25), 50])
w = max([int(r / 10), 5])
red = (0, 0, 255) # BGR!
#red = (0, 0, 0) # look for black pixels when using png2txt (deprecated)
for (px, py), _ in zip(np.argwhere(cv2.inRange(img, red, red)), range(num)):
    cv2.circle(img, (py, px), r, red, w)
cv2.imwrite(out, img)
