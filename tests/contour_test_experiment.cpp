#include "contour_test.h"
#include "canvas.h"

using contour::HoledSimpleContour;
using contour::ContourList;

void contour_test_experiment() {
    contour_unit_tests(false);
    //Draw(1, {A});
    //Draw(2, {B});
    //Draw(3, {A, B, R});
    //Draw(4, {A, B, A+B});
}
