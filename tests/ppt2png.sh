#!/bin/bash -e
# LOFFICE=... ppt2png.sh some/path/to/orig.pptx new-filename.png
F="`basename ${1%.*}`.png"
D=tmp-lo-$RANDOM
until E=$($LOFFICE "-env:UserInstallation=file:///$PWD/$D" --convert-to png --outdir $D "$1" |& \
 awk -vx="$1" '/convert .* using filter : impress_png_Export$/ { next }
               /failed to launch javaldx/ { next }
               /terminate called after throwing an instance of .*WrappedTargetRuntimeException/ { next }
               /Unspecified Application Error/ { next }
               $0 {print x ":", $0}') && [ -s "$D/$F" -o "$E" ]; do sleep 3; done
[[ -f "$D/$F" ]] && mv "$D/$F" "$2" || { [[ "$E" ]] && echo "$E" >&2 || touch "$2"; }
rm -rf $D
