#!/usr/bin/env python3
# Print a _diff bitmap to screen, highlighting red pixels
# or reverse with `python -mfire png2txt.py decode text-repr`
from PIL import Image
from sys import argv

# Palette; reverse order for mostly white images (smaller utf-8 output)
CHR = [' ', '░', '▒', '▓', '█'][::-1] # https://en.wikipedia.org/wiki/Block_Elements
print(f'block palette: {"".join(CHR)}')
CHR = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '8', '@'][::-1]
print(f'ascii palette: {"".join(CHR)}')
r = (len(CHR) - 1) / 255. # map 0..255 to indices in CHR
RED = 'X'

def reverse(c):
    assert c in CHR, f'`{c}` is not in the palette'
    return int(CHR.index(c) / r)

def encode(file, palette='ascii'):
    img = Image.open(file)
    gr = img.convert('L')
    assert img.size == gr.size
    assert 3 <= len(img.getpixel((0, 0))) <= 4
    red = (255, 0, 0) if len(img.getpixel((0, 0))) == 3 else (255, 255, 0, 0)
    w, h = img.size
    m = [[CHR[0] for _ in range(w)] for _ in range(h)]
    for y in range(h):
        for x in range(w):
            px = gr.getpixel((x, y))
            m[y][x] = RED if img.getpixel((x, y)) == red else CHR[int(px * r)]
    for i in m:
        print(''.join(i))

def decode(file, palette='ascii'):
    # red = (255, 0, 0) Would need 1) convering to RGB and 2) rememberin places of red and then editing
    red = 0 # use black instead; no other color should be this dark
    with open(file) as f:
        buf = [[red if c == RED else reverse(c) for c in l.rstrip('\n')] for l in f]
        assert all(len(i) == len(buf[0]) for i in buf)
        assert all(min(i) >= 0 for i in buf), f'?? {i=}'
        assert all(max(i) <= 255 for i in buf)
        gr = Image.frombuffer('L', (len(buf[0]), len(buf)), bytes(x for row in buf for x in row))
        gr.save(f'{file}_decoded.png')

if __name__ == '__main__':
    encode(argv[1])
