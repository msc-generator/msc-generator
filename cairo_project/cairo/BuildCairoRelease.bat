set PATH=%PATH%;C:\MinGW\msys\1.0\bin
call "..\SetVsEnv.bat"

set ROOTDIR=%CD%\..

set INCLUDE=%INCLUDE%;%ROOTDIR%\zlib
set INCLUDE=%INCLUDE%;%ROOTDIR%\libpng
set INCLUDE=%INCLUDE%;%ROOTDIR%\pixman\pixman
set INCLUDE=%INCLUDE%;%ROOTDIR%\cairo\boilerplate
set INCLUDE=%INCLUDE%;%ROOTDIR%\cairo\cairo\src

set LIB_SAVE=%LIB%

set LIB=%LIB_SAVE%;%ROOTDIR%\libpng\projects\vstudio\Release

make -f Makefile.win32 "CFG=release" "LIBCFLAG=-MD"

pause